call venv\Scripts\activate.bat
set DEBUG_SHAREDEARTH=

pip freeze > requirements.txt

rd /q /s static
mkdir static

@echo Compressing files...
python manage.py compress
@if %errorlevel% neq 0 exit /b %errorlevel%

call heroku maintenance:on --app sharedearth-staging
call heroku ps:scale --app sharedearth-staging web=0

@echo Collecting static files a second time...
python manage.py collectstatic --noinput
@if %errorlevel% neq 0 exit /b %errorlevel%

git add --all .
@if %errorlevel% neq 0 exit /b %errorlevel%

git commit -m "Automatic commit before heroku deployment"
@if %errorlevel% neq 0 exit /b %errorlevel%

git push bitbucket master
REM @if %errorlevel% neq 0 exit /b %errorlevel%

git push heroku-staging master
@if %errorlevel% neq 0 exit /b %errorlevel%

call heroku run --app sharedearth-staging python manage.py migrate

call setenv.bat

call heroku ps:scale --app sharedearth-staging web=1

echo Waiting fifteen seconds for the server to start, then turning off maintenance mode...
sleep 15
call heroku maintenance:off --app sharedearth-staging
echo Deployment complete