var selectedPlace;
var autocompleteOptions = {
  types: ['geocode'],
  componentRestrictions: {country: 'us'}
};

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

$(function()
{
  //var csrftoken = ;
  
  $.ajaxSetup({
    beforeSend: function(xhr, settings) {
      if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
        xhr.setRequestHeader("X-CSRFToken", $.cookie('csrftoken'));
      }
     }
  });
});

function locationError ( message )
{
  $('#error').text(message);
  //$("#addressField:text").val(" " + ($("#addressField:text").val().trim()))
  //var el = $("#addressField:text").get(0);
  //el.selectionStart = 0;
  //el.selectionEnd = 0;
  $("#addressField").select();
  $('#addressField').focus();
}


function locationChanged ( place )
{
  selectedPlace = place;

  var components = getAutocompleteComponents(place);
  if ( !('route' in components) || (components.route === '') )
  {
    locationError("Please enter your full street address so we can find matches near you.");
  }
}

function installAutocomplete ( )
{
  var elements = document.getElementsByClassName('address-autocomplete');

  for ( var i = 0; i < elements.length; ++ i )
  {
    var autocompleteElement = elements[i];
    var autocomplete = new google.maps.places.Autocomplete(autocompleteElement, autocompleteOptions);
    
    google.maps.event.addListener(autocomplete, 'place_changed', function ()
    {
        var place = autocomplete.getPlace();
        //document.getElementById('city2').value = place.name;
        //document.getElementById('cityLat').value = place.geometry.location.lat();
        //document.getElementById('cityLng').value = place.geometry.location.lng();
        
        if ( typeof place.address_components == 'undefined' )
        {
          // The user pressed enter in the input 
          // without selecting a result from the list
          // Let's get the list from the Google API so that
          // we can retrieve the details about the first result
          // and use it (just as if the user had actually selected it)
          var autocompleteService = new google.maps.places.AutocompleteService();
  
          var placeOptions = {
            'input': place.name,
            'offset': place.name.length
          };
          jQuery.extend(placeOptions, autocompleteOptions);
  
          autocompleteService.getPlacePredictions(
            placeOptions,
            function listentoresult ( list, status )
            {
              if(list === null || list.length === 0) {
                // There are no suggestions available.
                // The user saw an empty list and hit enter.
              } else {
                // Here's the first result that the user saw
                // in the list. We can use it and it'll be just
                // as if the user actually selected it
                // themselves. But first we need to get its details
                // to receive the result on the same format as we
                // do in the AutoComplete.
                var placesService = new google.maps.places.PlacesService(autocompleteElement);
                
                placesService.getDetails(
                  {'reference': list[0].reference},
                  function detailsresult(detailsResult, placesServiceStatus)
                  {
                    //setLocation(map, detailsResult);
                    
                    var components = getAutocompleteComponents(detailsResult);
                    if ( !('route' in components) || (components.route === '') )
                    {
                      locationError("Please enter your full street address so we can find matches near you.");
                    }
                    else
                    {
                      $(autocompleteElement).blur();
                      $(autocompleteElement).val(detailsResult.formatted_address);
                      locationChanged(detailsResult);
                    }
                  }
                );
              }
            }
          );
        }
        else
        {
          locationChanged(place);
        }
    }); 
  }   
}

function getAutocompleteComponents ( place )
{
  var address_components = place.address_components;
  var components={}; 
  jQuery.each(address_components, function(k,v1) {jQuery.each(v1.types, function(k2, v2){components[v2]=v1.long_name});});
  return components;    
}


function newLocation ( type )
{
  if ( selectedPlace )
  {
    var place = selectedPlace;
    var lat = place.geometry.location.lat()
    var lng = place.geometry.location.lng()
    var address = place.formatted_address;
   
    var components = getAutocompleteComponents(place);
    
    if ( !('route' in components) || (components.route === '') )
    {
      var what = type == 'have' ? 'gardeners' : 'land';  
    
      locationError("Please enter your full street address so we can find " + what + " near you.");
    }
    else
    {
      // Add it to our user
      delete components.political; // If there
      components.formatted_address = address;
      components.latitude = lat;
      components.longitude = lng;
      components.type = type;
      
      try
      {
        // Run the Google analytics. Ignore failures.
        ga('send', 'event', 'button', 'signup-' + cityName, type == 'have' ? 'I Have Land or Tools to Share' : 'I Need Land or Tools');
      }
      finally
      {
        $.post("/locations/add/", components, function(result) {
          window.location.replace("/app/");
        });
      }
    }    
  }
  else
  {
    $('#addressField').focus();
  }
}

var iframe = null;
var player;
var start;


$(function() {
  iframe = document.getElementById('embedded_video');
  player = $f(iframe);
  
  player.addEvent('ready', function() {
    player.addEvent('pause', onFinish);
    player.addEvent('finish', onFinish);
  });
  
  player.api($(this).text().toLowerCase());
    
  function onFinish ( )
  {
    $('#overlay').fadeOut(1);
  }
  
  $('#playvideobutton').bind('click', function() {
    $('#overlay').fadeIn(1);
    player.api($(this).text().toLowerCase());
    player.api("play");
  });
});

function allowFullscreen ( allow )
{
  if ( allow )
  {
    iframe.setAttribute('webkitAllowFullScreen', '');
    iframe.setAttribute('mozAllowFullScreen', '');
    iframe.setAttribute('allowFullScreen', '');
  }
  else
  {
    iframe.removeAttribute('webkitAllowFullScreen');
    iframe.removeAttribute('mozAllowFullScreen');
    iframe.removeAttribute('allowFullScreen');
  }
}

function pauseVideo ( )
{
  $('#overlay').fadeOut(1);
  player.api("pause");
}

