$(window).on("load", function() {
    $('.popeasy').popeasy({
        trigger: '.popeasy',            // id or class of link or button to trigger modal
        olay:'.overlay',                // id or class of overlay
        modals:'.modal',                // id or class of modal
        loadExternal: false,            // load external content
        animationEffect: 'slideDown',   // overlay effect | slideDown or fadeIn | default=fadeIn
        animationSpeed: 400,            // speed of overlay in milliseconds | default=400
        moveModalSpeed: 'slow',         // speed of modal movement when window is resized | slow or fast | 
        background: '2191bd',           // hexidecimal color code - DONT USE #
        maskOpacity: 0.8,               // opacity of mask |  0 - 1 | default = 0.8
        modalOpacity: 1,              // opacity of modals |  0 - 1 | default = 0.8
        openOnLoad: false,              // open modal on page load | true or false | default=false
        docClose: true,                 // click document to close | true or false | default=true    
        closeByEscape: true,            // close modal by escape key | true or false | default=true
        moveOnScroll: true,             // move modal when window is scrolled | true or false | default=false
        resizeWindow: true,             // move modal when window is resized | true or false | default=false
        videoClass:'video',             // class of video element(s)
        close:'.close'                  // id or class of close button
    });
});

