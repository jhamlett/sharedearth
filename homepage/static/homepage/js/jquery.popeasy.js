///////////////////////////////////////////////////////////////////////////////////////

//      Pop Easy | jQuery Modal Plugin
//      Version 2.0
//      Created 2014 by Thomas Grauer

///////////////////////////////////////////////////////////////////////////////////////

(function($){

    $.fn.popeasy= function(options, callback){
       
        options = $.extend({
            trigger: '.popeasy',
            olay: 'div.overlay',
            modals: 'div.modal',
            loadExternal: false,
            animationEffect: 'fadeIn',
            animationSpeed: 400,
            moveModalSpeed: 'slow',
            background: '000',
            maskOpacity: 0.8,
            modalOpacity:1,
            openOnLoad: false,
            docClose: true,
            closeByEscape: true,
            moveOnScroll: false,
            resizeWindow: true,
            videoClass:'video',
            close:'.closeBtn'
            
        },options);
       
        var olay = $(options.olay);
        var modals = $(options.modals);
        var currentModal;
        var getModal;
        var isopen=false;
        var src;
       
        if (options.animationEffect==='fadein'){options.animationEffect = 'fadeIn';}
        if (options.animationEffect==='slidedown'){options.animationEffect = 'slideDown';}
        
        $('.modal').each(function(){
            if($(this).children().hasClass(options.videoClass)){
                src=$(this).find('.'+options.videoClass).attr('src');
                $(this).attr('data-video', src);
            }
        });

        olay.css({opacity : 0});
                
        if(options.openOnLoad) {
            currentModal = $('.modal').first();
            openModal();
        }else{
            olay.hide();
            modals.hide();
        }
        
        $(options.trigger).on('click', function(e){
            e.preventDefault();
            
            if ($('.popeasy').length >1) {
                getModal = $(this).attr('href');
                currentModal = $(getModal);    
            }else{
                currentModal = $('.modal');
            }
            openModal();
        });
        
        function openModal(){
            if(options.loadExternal){
                var fetchContent = currentModal.attr('data-path');
                currentModal.load(fetchContent);
            }

            src=currentModal.attr('data-video');
            currentModal.children('.'+options.videoClass).attr('src',src);

            modals.hide();
            currentModal.css({
                top:$(window).height() /2 - currentModal.outerHeight() /2 + $(window).scrollTop(),
                left:$(window).width() /2 - currentModal.outerWidth() /2 + $(window).scrollLeft()
            });
                
            if(isopen===false){
                olay.css({opacity : options.maskOpacity, backgroundColor: '#'+options.background});
                modals.css({opacity : options.modalOpacity});
                olay[options.animationEffect](options.animationSpeed);
                currentModal.delay(options.animationSpeed)[options.animationEffect](options.animationSpeed); 
            }else{
                currentModal.show();
            }
            isopen=true;
        }
        
        function moveModal(){
            modals
            .stop(true)
            .animate({
            top:$(window).height() /2 - modals.outerHeight() /2 + $(window).scrollTop(),
            left:$(window).width() /2 - modals.outerWidth() /2 + $(window).scrollLeft()
            },options.moveModalSpeed);
        }
        
        function closeModal(){
            currentModal.find('.'+options.videoClass).attr('src',''); 
            isopen=false;
            modals.fadeOut(100, function(){
                if (options.animationEffect === 'slideDown') {
                    olay.slideUp();
                }else if (options.animationEffect === 'fadeIn') {
                    olay.fadeOut();
                }
            });
            return false;
        }
        
        if(options.docClose){
            olay.bind('click', closeModal);
        }
        
        $(document).on('click', options.close, closeModal);
        
        if(options.closeByEscape) {
            $(window).bind('keyup', function(e){
                if(e.which === 27){
                    closeModal();
                }
            });
        }
        
        if(options.resizeWindow) {
            $(window).bind('resize', moveModal);
        }else{
            return false;
        }
        
        if(options.moveOnScroll) {
            $(window).bind('scroll', moveModal);
        }else{
            return false;
        }
    };
})(jQuery);
