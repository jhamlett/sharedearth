from django.shortcuts import redirect
from django.template.context import RequestContext
from django.template.response import TemplateResponse
from django.views.decorators.csrf import ensure_csrf_cookie


# Create your views here.
@ensure_csrf_cookie
def homepage_force(request):
    context = RequestContext(request)
    if 'HTTP_REFERER' in request.META:
        if not request.session.get('origin'):
            request.session['origin'] = request.META['HTTP_REFERER']
    
    return TemplateResponse(request, 'homepage/index.html', context)


def check_bad(request, context):
    # This may be a spider that doesn't even have a proper version string. Just
    # let it be.
    if not request.user_agent.browser.version:
        return None
    
    family = request.user_agent.browser.family
    version = request.user_agent.browser.version[0]
    
    if len(request.user_agent.browser.version) > 1:
        minor_version = request.user_agent.browser.version[1]
    else:
        minor_version = 0

    bad = False
    
    if not request.user_agent.is_mobile and not request.user_agent.is_tablet:
        mobile = "(Desktop)"
        if family == "IE":
            family = "Internet Explorer"
            if version < 10:
                bad = True 
    
        if family == "Chrome":
            if version < 31:
                bad = True
    
        if family == "Firefox":
            if version < 34:
                bad = True
                
        if family == "Opera":
            if version < 26:
                bad = True
                
        if family == "Safari":
            if version < 7:
                bad = True
            elif version == 7 and minor_version < 1:
                bad = True
    
    if bad:
        context["browser"] = family 
        context["browser_version"] = ".".join(map(str, request.user_agent.browser.version))
        context["browser_mobile"] = mobile
        return TemplateResponse(request, 'bad_browser.html', context)
    else:
        return None


# Create your views here.
@ensure_csrf_cookie
def homepage(request, city=None):
    context = RequestContext(request)
    # tr = check_bad(request, context)
    # if tr is not None:
    #     return tr

    if not request.user.is_authenticated():
        savedLocations = request.session.get("locations")
        
        if savedLocations:
            if len(savedLocations):
                return redirect('main_site')

        if 'HTTP_REFERER' in request.META:
            if not request.session.get('origin'):
                request.session['origin'] = request.META['HTTP_REFERER']

        if city:
            city_list = {
                'chicago': ['179 gardens and gardeners<br/>in the Chicago area!'],
                'newyorkcity': ['239 gardens and gardeners<br/>in the New York City area!'], 
                'houston': ['110 gardens and gardeners<br/>in the Houston area!'],
                'austin': ['236 gardens and gardeners<br/>in the Austin area!'],
                'dallas': ['178 gardens and gardeners<br/>in the Dallas area!'],
            }
            
            if city in city_list:
                context["city"] = city 
                context["copy"] = city_list[city][0]
            else:
                return redirect('homepage')
                
        return TemplateResponse(request, 'homepage/index.html', context)
    else:
        return redirect('main_site')
