from django.conf.urls import patterns, url
from profiles import views

urlpatterns = patterns('',
    # HTML pages
    url(r"^500/$", views.error), # Allow forcing a server error, even in production, to validate error logging 
    url(r'^app/.*$', views.main_site, name='main_site'),
    url(r'^recover/.*$', views.main_site, name='recover_password'),

    # RESTful API
    url(r'^user/$', views.rest_user),
    url(r'^user/(?P<user_id>\d+)$', views.rest_user_basic_details),
    url(r'^signup/$', views.rest_signup),
    url(r'^login/$', views.rest_login),
    url(r'^logout/$', views.rest_logout),
    url(r'^password/$', views.rest_password),
    url(r'^friends/$', views.rest_friends),
    url(r'^full-debug/$', views.full_debug),
    
    # Stats
    url(r'^stats/$', views.stats),
    url(r'^stats/marketing-users$', views.marketing_user_list),
    url(r'^stats/users$', views.user_list),

    # Marketing
    url(r'^generate-marketing-tokens/$', views.generate_marketing_tokens)

    # Migration    
    # url(r'^migrate1/$', views.migrate1),
    # url(r'^migrate2/$', views.migrate2),
    # url(r'^migrate3/$', views.migrate3),
    # url(r'^migrate4/$', views.migrate4),
    # url(r'^migrate5/$', views.migrate5),
)
