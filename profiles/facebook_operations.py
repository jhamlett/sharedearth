import facebook
from sharedearth import settings
from django.contrib.auth.models import User
from django.contrib.auth import login
from locations.interface import merge_locations


def facebook_to_user(request):
    try:
        facebook_user_id = facebook.get_user_from_cookie(request.COOKIES, settings.FACEBOOK_APP_ID, settings.FACEBOOK_APP_SECRET)
        password = 'facebook$' + facebook_user_id
        user = User.objects.get(password=password)
        user.backend = 'django.contrib.auth.backends.ModelBackend'
        login(request, user)
        merge_locations(request)
        return user    
    except:
        return None
