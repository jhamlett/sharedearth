# Background tasks
from sharedearth import settings, celery_app
from PIL import Image as PilImage, ImageOps
import shortuuid
import requests
import boto
from io import BytesIO
from images.views import get_user_uuid_from_uid
from django.contrib.auth.models import User
from images.models import Image
from images.tasks import create_map_image_internal
from locations.models import Location
from messaging.apps import redis_pool
import redis
import json
from profiles.models import UserProfile, Notification, Friendship
from django.db.models.query_utils import Q
from locations.utils import haversine_distance
from profiles.operations import get_display_name
from django.utils import timezone
from datetime import timedelta
from mailer.mailer import send_template_mail


def save_into_s3(image, user_id, image_uuid, suffix):
    # Connect to our s3 bucket and upload from memory
    conn = boto.connect_s3(settings.UPLOADER_AWS_ACCESS_KEY, settings.UPLOADER_AWS_SECRET_KEY)
    
    # Connect to bucket and create key
    b = conn.get_bucket(settings.UPLOADER_S3_BUCKET)
    
    user_uuid = get_user_uuid_from_uid(user_id)
    fuid = image_uuid
    extension = "jpeg"
    
    object_name = 'm/' + user_uuid + "/" + fuid + "." + suffix + "." + extension

    k = b.new_key(object_name)
    
    # Note we're setting contents from the in-memory string provided by cStringIO
    k.set_contents_from_string(image.getvalue(), headers={"Content-Type": "image/jpeg"})
    
    return 'https://%s.s3.amazonaws.com/%s' % (settings.UPLOADER_S3_BUCKET, object_name)
    

def load_images_into_s3(user_id, location_id, original_url, image_uuid):
    # Retrieve our source image from a URL
    r = requests.get(original_url)
    im = PilImage.open(BytesIO(r.content))
    
    # Store the original size
    byteStream = BytesIO()
    im.save(byteStream, 'JPEG')
    url_large = save_into_s3(byteStream, user_id, image_uuid, "2400")
    
    im2 = im.copy()
    im2.thumbnail((1200, 1200), PilImage.ANTIALIAS)
    byteStream = BytesIO()
    im2.save(byteStream, 'JPEG')
    url_medium = save_into_s3(byteStream, user_id, image_uuid, "1200")

    (cwidth, cheight) = (240, 240)
    
    (width, height) = im.size
    if width > height:
        cwidth = 24000
    else:
        cheight = 24000
        
    im.thumbnail((cwidth, cheight), PilImage.ANTIALIAS)
    (cwidth, cheight) = (240, 240)
    
    output = ImageOps.fit(im, (cwidth, cheight), centering=(0.5, 0.5))
    
    # Save the image into a BytesIO() object to avoid writing to disk
    out_im2 = BytesIO()

    # Specify the file type since there is no file name to discern it from
    output.save(out_im2, 'JPEG')
    
    url_avatar = save_into_s3(byteStream, user_id, image_uuid, "240")
    url_map = create_map_image_internal(user_id, url_avatar, image_uuid)

    # Set the location avatar    
    location = Location.objects.get(id=location_id)
    location.avatar_url = url_avatar
    location.map_icon_url = url_map
    location.save(update_fields=['avatar_url', 'map_icon_url'])

    # Save the image
    user = User.objects.get(id=user_id)
    image = Image()
    image.user = user
    image.location = location
    image.uuid = str(shortuuid.uuid())
    image.url_large = url_large
    image.url_medium = url_medium
    image.url_avatar = url_avatar
    image.url_map = url_map 
    image.save()
    
    # Set the user avatar
    UserProfile.objects.filter(user=user).update(avatar_url=url_avatar)

    # Send out a message to let to user know this is done

    m = {
        'imageID': image.id,
        'avatarChange': True,
        'avatarURL': url_avatar,
        'mapURL': url_map,
        'largeURL': url_large,
        'locationUUID': location.uuid
    }
    
    channel = "messages_to_" + str(user_id)
    r = redis.Redis(connection_pool=redis_pool)
    r.publish(channel, json.dumps(m, separators=(',', ':')))


@celery_app.task()
def get_avatar_from_facebook_async(user_id, facebook_user_id):
    graph_url = 'http://graph.facebook.com/' + facebook_user_id + '/picture?type=large&redirect=false&width=2400&height=2400'

    r = requests.get(graph_url).json()
    image_url = r['data']['url']
    
    location_id = Location.objects.filter(user__id=user_id).first().id
    
    image_uuid = shortuuid.uuid()
    load_images_into_s3(user_id, location_id, image_url, image_uuid)
    
    
def get_avatar_from_facebook(user_id, facebook_user_id):
    get_avatar_from_facebook_async.apply_async(args=[user_id, facebook_user_id])


def email_matches(location, locations):
    lat = location.latitude
    lng = location.longitude
    
    for match in locations:
        distance = haversine_distance(lat, lng, match.latitude, match.longitude)
        max_distance = match.distance
        if max_distance == 0:
            max_distance = 0.5
            
        if distance <= max_distance:
            notification = Notification()
            notification.user_notified = match.user
            notification.location_notified_about = location
            notification.save()
            
            # We have found a match. Let them know
            print(get_display_name(match.user, match.user))
            print(match.formatted_address)
            print(distance)
            print("--------")
            pass


def basic_filter(location):
    lat = location.latitude
    lng = location.longitude
    
    # 1.0 degrees is roughly 50 miles W/E and 70 miles N/S for the US.
    min_lat = lat - 1 
    max_lat = lat + 1
    min_lng = lng - 1
    max_lng = lng + 1

    # All locations within this distance ...    
    locations = Location.objects.filter(latitude__gte=min_lat).filter(latitude__lte=max_lat).filter(longitude__gte=min_lng).filter(longitude__lte=max_lng)

    # People who want to be emailed ...
    locations = locations.filter(user__userprofile__email_on_new_matches=True)
    
    # And who haven't yet been notified
    locations = locations.filter(~Q(user__notification__location_notified_about=location))
    
    return locations


@celery_app.task()
def on_new_location_async(location_id):
    location = Location.objects.get(id=location_id)
    tool_list = list(location.tools_i_have.names())
    locations = basic_filter(location)
        
    # Of the correct type
    search_type = 'have' if location.type == 'need' else 'need'
    query = Q(type=search_type)
    
    # Or who want the tools we have
    if len(tool_list):
        query |= Q(tools_i_need__name__in=tool_list)
    
    locations = locations.filter(query).distinct()
    email_matches(location, locations)


@celery_app.task()
def on_new_tool_async(location_uuid, tool):
    location = Location.objects.get(uuid=location_uuid)
    tool_list = [tool]
    locations = basic_filter(location)
        
    # Who want this new tool
    query = Q(tools_i_need__name__in=tool_list)
    
    locations = locations.filter(query).distinct()
    email_matches(location, locations)

    
@celery_app.task()
def send_password_reset_email_async(user_id):
    # TODO: Send email
    
    user = User.objects.get(id=user_id)
    user_name = get_display_name(user, user)
    profile = user.userprofile
    
    profile.change_password_token = str(shortuuid.uuid())
    profile.change_password_token_expiration = timezone.now() + timedelta(days=7)
    profile.save(update_fields=['change_password_token', 'change_password_token_expiration'])
    
    password_reset_url = 'https://sharedearth.com/recover/' + profile.change_password_token
    
    send_template_mail('reset-your-password', [{'email': user.email, 'name': user_name}], {
        'PASSWORDLINK': password_reset_url, 
        'SOMENAME': get_display_name(user, user)
    })


@celery_app.task()
def notify_of_connection_request_async(user_from_id, user_to_id):

    profile = UserProfile.objects.get(user__id=user_to_id)
    
    if profile.email_on_connect:    
        # Make sure the reverse friendship does not exist
        
        user_from = User.objects.get(id=user_from_id)
        user_to = User.objects.get(id=user_to_id)
        
        if Friendship.objects.filter(friend=user_from, user=user_to).count() == 0:
            fss = Friendship.objects.filter(user=user_from, friend=user_to)
            
            if fss.count():
                fs = fss.first()
            
                if fs.autologin_token_expiration is None or fs.autologin_token_expiration < timezone.now():
                    fs.autologin_token = str(shortuuid.uuid())
        
                fs.autologin_token_expiration = timezone.now() + timedelta(days=7)
                fs.save(update_fields=['autologin_token', 'autologin_token_expiration'])
                
                url = 'https://sharedearth.com/connection/' + fs.autologin_token + '/'
                
                user = User.objects.get(id=user_to_id)
                user_name = get_display_name(user, user)
                sender = User.objects.get(id=user_from_id)
                sender_name = get_display_name(user, sender)
                
                print("Sending email notification of new connection from " + sender_name + " to " + user_name + " for " + url)
                send_template_mail('new-connection', [{'email': user.email, 'name': user_name}], {
                    'CONNECTIONUSER': sender_name, 
                    'CONNECTIONLINK': url,
                    'SOMENAME': user_name
                })
        else:
            print("No need to notify--this was completing a connection")


def on_new_location(location):
    on_new_location_async.apply_async(args=[location.id])
    

def on_new_tool(location_uuid, tag):
    on_new_tool_async.apply_async(args=[location_uuid, tag])


def send_password_reset_email(user):
    send_password_reset_email_async.apply_async(args=[user.id])


def notify_of_connection_request(user_from_id, user_to_id):
    notify_of_connection_request_async.apply_async(args=[user_from_id, user_to_id], countdown=settings.CONNECTION_NOTIFICATION_EMAIL_DELAY)


# EOF
