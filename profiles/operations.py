def get_display_name(current_user, other_user):
    
    show_full_name = False
    
    if current_user.id == other_user.id:
        show_full_name = True
    elif other_user.userprofile.share_full_name_all:
        show_full_name = True
    elif other_user.userprofile.share_full_name_connected and False:        # TODO - Check if connected
        show_full_name = True
    
    if len(other_user.last_name):
        if show_full_name:
            return "%s %s" % (other_user.first_name, other_user.last_name)
        else:
            return "%s %s." % (other_user.first_name, other_user.last_name[:1])
    else:
        return other_user.first_name
