# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import profiles.models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('profiles', '0017_auto_20150617_0105'),
    ]

    operations = [
        migrations.CreateModel(
            name='MarketingAutologin',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('autologin_token', models.CharField(max_length=256, null=True, default=None, blank=True)),
                ('autologin_token_expiration', models.DateTimeField(default=None)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
