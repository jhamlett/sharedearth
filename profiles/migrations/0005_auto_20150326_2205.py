# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0004_userprofile_experience'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='email_on_messages',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='userprofile',
            name='email_on_new_matches',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
    ]
