# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0014_notification'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='change_password_token',
            field=models.CharField(max_length=256, null=True, default=None, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='userprofile',
            name='change_password_token_expiration',
            field=models.DateTimeField(default=django.utils.timezone.now),
            preserve_default=True,
        ),
    ]
