# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0019_auto_20150617_1234'),
    ]

    operations = [
        migrations.AddField(
            model_name='marketingautologin',
            name='destination_url',
            field=models.CharField(null=True, blank=True, max_length=512, default=None),
            preserve_default=True,
        ),
    ]
