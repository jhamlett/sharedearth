# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0022_auto_20150617_1332'),
    ]

    operations = [
        migrations.AlterField(
            model_name='marketingautologin',
            name='autologin_token',
            field=models.CharField(null=True, db_index=True, blank=True, default=None, max_length=256),
            preserve_default=True,
        ),
    ]
