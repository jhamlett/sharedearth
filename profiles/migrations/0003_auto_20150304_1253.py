# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import taggit.managers


class Migration(migrations.Migration):

    dependencies = [
        ('taggit', '0001_initial'),
        ('profiles', '0002_userprofile_avatar_url'),
    ]

    operations = [
        migrations.CreateModel(
            name='TaggedInterests',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('content_object', models.ForeignKey(to='profiles.UserProfile')),
                ('tag', models.ForeignKey(related_name='profiles_taggedinterests_items', to='taggit.Tag')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TaggedWhatIGrow',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('content_object', models.ForeignKey(to='profiles.UserProfile')),
                ('tag', models.ForeignKey(related_name='profiles_taggedwhatigrow_items', to='taggit.Tag')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='userprofile',
            name='interests',
            field=taggit.managers.TaggableManager(verbose_name='Tags', through='profiles.TaggedInterests', help_text='A comma-separated list of tags.', to='taggit.Tag'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='userprofile',
            name='what_i_grow',
            field=taggit.managers.TaggableManager(verbose_name='Tags', through='profiles.TaggedWhatIGrow', help_text='A comma-separated list of tags.', to='taggit.Tag'),
            preserve_default=True,
        ),
    ]
