# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0021_auto_20150617_1331'),
    ]

    operations = [
        migrations.AlterField(
            model_name='marketingautologin',
            name='autologin_token',
            field=models.CharField(max_length=256, null=True, default=None, blank=True),
            preserve_default=True,
        ),
    ]
