# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0008_userprofile_email_on_announcements'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='share_full_name_all',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='userprofile',
            name='share_full_name_connected',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
    ]
