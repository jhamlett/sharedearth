# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0015_auto_20150522_2258'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='referring_url',
            field=models.TextField(default=None, null=True, blank=True),
            preserve_default=True,
        ),
    ]
