# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0005_auto_20150326_2205'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='email_on_connect',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
    ]
