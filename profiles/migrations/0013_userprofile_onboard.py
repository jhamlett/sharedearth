# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0012_friendship'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='onboard',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
