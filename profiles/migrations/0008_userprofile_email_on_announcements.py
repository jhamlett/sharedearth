# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0007_userprofile_email_on_resources'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='email_on_announcements',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
    ]
