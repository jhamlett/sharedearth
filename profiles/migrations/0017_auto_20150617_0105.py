# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0016_userprofile_referring_url'),
    ]

    operations = [
        migrations.AddField(
            model_name='friendship',
            name='autologin_token',
            field=models.CharField(max_length=256, default=None, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='friendship',
            name='autologin_token_expiration',
            field=models.DateTimeField(default=django.utils.timezone.now),
            preserve_default=True,
        ),
    ]
