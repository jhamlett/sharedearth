# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0020_marketingautologin_destination_url'),
    ]

    operations = [
        migrations.AlterField(
            model_name='marketingautologin',
            name='autologin_token',
            field=models.CharField(blank=True, db_index=True, null=True, default=None, max_length=256),
            preserve_default=True,
        ),
    ]
