# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0010_userprofile_share_address_connected'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='responsibilities',
            field=models.IntegerField(choices=[(0, "I'd like to work alone"), (1, "We could work together if you'd like"), (2, "I'm going to need some help")], default=0),
            preserve_default=True,
        ),
    ]
