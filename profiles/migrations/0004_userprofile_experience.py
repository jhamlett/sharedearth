# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0003_auto_20150304_1253'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='experience',
            field=models.IntegerField(choices=[(0, 'New to gardening'), (1, '1 year'), (2, '2-5 years'), (5, '5-10 years'), (10, '10+ years')], default=2),
            preserve_default=True,
        ),
    ]
