# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0018_marketingautologin'),
    ]

    operations = [
        migrations.AlterField(
            model_name='marketingautologin',
            name='autologin_token',
            field=models.CharField(max_length=256, default=None, blank=True, null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='marketingautologin',
            name='autologin_token_expiration',
            field=models.DateTimeField(default=django.utils.timezone.now),
            preserve_default=True,
        ),
    ]
