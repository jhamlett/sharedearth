from django.template.context import RequestContext
from locations.interface import merge_locations
from django.views.decorators.csrf import ensure_csrf_cookie
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.contrib.auth import login
from django.http.response import JsonResponse, HttpResponseForbidden,\
    HttpResponseBadRequest, HttpResponse
from profiles.models import UserProfile, Friendship, MarketingAutologin, generate_autologin_token, generate_autologin_expiration
from django.contrib.staticfiles.templatetags import staticfiles
import django.contrib.auth
from django.shortcuts import redirect
from django.core.exceptions import PermissionDenied
from sharedearth.decorators import ensure_user_authorized
import json
from django.views.decorators.cache import never_cache
from django.forms.models import model_to_dict
from sharedearth.custom_responses import HttpResponseNoContent,\
    JsonResponseForbidden
from tags.utilities import get_tag_field
from sharedearth import settings
from django.views.decorators.http import require_GET, require_POST
from profiles.operations import get_display_name
from profiles.facebook_operations import facebook_to_user
from django.template import loader
import os
from messaging.operations import send_connection
import facebook
from profiles.tasks import get_avatar_from_facebook, send_password_reset_email, notify_of_connection_request
from django.contrib.auth.hashers import make_password
from django.utils import timezone
from django.template.base import Template
from homepage.views import check_bad
from django.contrib.auth.decorators import permission_required
from django.db import connection
import csv


# request.user_agent.browser.family = "IE" for Internet Explorer
# request.user_agent.browser.version = [11] for example

# Create your views here.


@ensure_csrf_cookie
@ensure_user_authorized
@require_GET
def rest_user_basic_details(request, user_id):
    profile = UserProfile.objects.get(user__id=user_id)
    
    response = {
        'display_name': get_display_name(request.user, profile.user),
        'avatar_url': profile.avatar_url
    }
    
    return JsonResponse(response)


@ensure_csrf_cookie
@never_cache
def rest_user(request):
    if request.method == 'GET':
        # May want to verify in a different way eventually.
        if not request.user.is_authenticated():
            profile = {}
            profile['is_logged_in'] = False
            return JsonResponse(profile)
        else:
            user = request.user
            profileModel = UserProfile.objects.get(user=user)
            profile = model_to_dict(profileModel)
            profile['first_name'] = user.first_name
            profile['last_name'] = user.last_name
            profile['email'] = user.email
            profile['is_logged_in'] = True
            profile['display_name'] = get_display_name(request.user, user)  # Their own name!
            profile['experience'] = str(profile['experience'])
            profile['responsibilities'] = str(profile['responsibilities'])
            profile['can_change_password'] = not user.password.startswith('facebook$') 
            del profile['user']
            del profile['what_i_grow']
            del profile['interests']
            
            profile['what_i_like_to_grow'] = list(get_tag_field(request.user, None, 'what_i_like_to_grow').names())
            profile['interests'] = list(get_tag_field(request.user, None, 'interests').names())
                    
            return JsonResponse(profile)
    elif request.method == 'POST':
        if not request.user.is_authenticated():
            return HttpResponseForbidden()
        
        d = json.loads(request.body.decode())
        field_name = d['field_name']
        value = d['value']
        
        if isinstance(value, str):
            value = value.strip()  # Remove whitespace
        profile = UserProfile.objects.get(user=request.user)
    
        # It's farily important to have a whitelist of fields that can be updated.
        if field_name in ['about', 'experience', 'responsibilities', 'email_on_messages', 'email_on_connect', 'email_on_new_matches', 'share_full_name_connected', 'share_full_name_all', 'onboard', 'share_address_connected', 'email_on_resources', 'email_on_announcements']:
            setattr(profile, field_name, value)
            profile.save(update_fields=[field_name])
            return HttpResponseNoContent()
        elif field_name in ['first_name', 'last_name']:
            user = request.user
            old_value = getattr(user, field_name)
            setattr(user, field_name, value)
            try:
                if len(value) == 0:
                    raise Exception("Enter a name, please")
                
                user.save(update_fields=[field_name])
            except:
                response = {'field_name': field_name, 'value': old_value}
                return JsonResponseForbidden(response)
            return HttpResponseNoContent()
        elif field_name in ['email']:
            user = request.user
            old_value = user.email
            
            if user.username.startswith("facebook$"):
                user.email = value
                try:
                    user.save(update_fields=['email'])
                except:
                    # This might fail if (for example) someone else has that email address
                    response = {'field_name': field_name, 'value': old_value}
                    return JsonResponseForbidden(response)
            else:
                user.username = value
                user.email = value
                try:
                    user.save(update_fields=['username'])
                    user.save(update_fields=['email'])
                except:
                    # This might fail if (for example) someone else has that email address
                    response = {'field_name': field_name, 'value': old_value}
                    return JsonResponseForbidden(response)
            return HttpResponseNoContent()
        else:
            return HttpResponseBadRequest()
    else:
        return HttpResponseBadRequest()


@ensure_csrf_cookie
def main_site(request):
    # result = TemplateResponse(request, 'angular-base.html', context)
    
    # Check if this is a password reset request
    parts = request.get_full_path().split('/')
    if len(parts) == 3 and parts[0] == '' and parts[1] == 'recover':
        token = parts[2]
        if len(token):
            # This is weird case
            if request.user.is_authenticated():
                django.contrib.auth.logout(request)
            
            up = UserProfile.objects.filter(change_password_token=token)
            if up.count():
                if up.first().change_password_token_expiration > timezone.now():
                    profile = up.first()
                    user = profile.user
                    user.backend = 'django.contrib.auth.backends.ModelBackend'
                    login(request, user)
        
                # Valid one time only            
                profile.change_password_token = None
                profile.save(update_fields=['change_password_token'])
    else:
        # Perhaps we can log in a user magically. Might as well try.
        facebook_to_user(request)

        if not request.get_full_path().endswith("login") and not request.user.is_authenticated():
            savedLocations = request.session.get("locations")
            
            if savedLocations:
                if len(savedLocations) == 0:
                    return redirect('homepage')
            else:
                return redirect('homepage')

    # Tablet users should see the regular site
    mobile = '1' if request.user_agent.is_mobile and not request.user_agent.is_tablet else '0' 
    
    context = RequestContext(request)
    tr = check_bad(request, context)
    if tr is not None:
        return tr
    
    context['websocket_uri'] = settings.WEBSOCKET_URI
    # context['is_mobile'] = mobile

    # Include all of our templates    
    path = 'angular/templates/angular/se-templates/'
    file_list = os.listdir(path)   
    template_list = []
    for file in file_list:
        template_list.append({'name': file, 'path': ("angular/se-templates/%s" % file)})
    context['templates'] = template_list
    context['raven'] = settings.RAVEN 
    
    template = loader.get_template('angular-base.html')
    
    response = HttpResponse(template.render(context))

    userID = str(request.user.id) if request.user.id is not None else '0'
    print(userID)
    response.set_cookie('seUserID', userID, domain=settings.COOKIE_DOMAIN)
    response.set_cookie('seDetectedMobile', mobile, domain=settings.COOKIE_DOMAIN)
    
    return response


@ensure_csrf_cookie
@require_POST
def rest_signup(request):
    # This is weird case
    if request.user.is_authenticated():
        return JsonResponse({'success': 1})

    p = json.loads(request.body.decode())
    
    if 'first_name' in p and 'last_name' in p and 'email' in p and 'password' in p:
        first_name = p['first_name']
        last_name = p['last_name']
        email = p['email']
        password = p['password']
        
        if User.objects.filter(email=email).count():
            # User already exists. Try logging them in
            # if user.check_password(raw_password): ...
            
            user = authenticate(username=email, password=password)
            
            if user:
                login(request, user)
                merge_locations(request)
                return JsonResponse({'userID': user.id})
            else:
                response = {'switchto': 'login', 'message': 'INCORRECT_PASSWORD'}
                return JsonResponseForbidden(response)
        
        # User does not exist. Create him or her.
    
        try:
            user = User.objects.create_user(
                username=email,
                first_name=first_name,
                last_name=last_name,
                email=email,
                password=password)
        except:
            user = None
        
        if user:
            up = UserProfile()
            up.user = user
            up.avatar_url = staticfiles.static('profiles/img/avi-neutral-square.png')
            up.referring_url = request.session.get('origin')
            up.save()
            
            user = authenticate(username=email, password=password)
            if user:
                login(request, user)
                merge_locations(request)
                return JsonResponse({'userID': user.id})
        
        response = {'message': 'GENERIC'}
        return JsonResponseForbidden(response)
    elif 'facebook' in p:
        user = facebook_to_user(request) 
        if user:
            return JsonResponse({'userID': user.id})
        else:
            facebook_user_id = facebook.get_user_from_cookie(request.COOKIES, settings.FACEBOOK_APP_ID, settings.FACEBOOK_APP_SECRET)
            password = 'facebook$' + facebook_user_id
            
            graph = facebook.GraphAPI(access_token=settings.FACEBOOK_ACCESS_TOKEN, version='2.3')
            
            fields = ','.join([
                'email',
                'first_name',
                'last_name',
            ])
            
            facebook_user = graph.get_object(id=facebook_user_id, fields=fields)
            print("user:", facebook_user)

            try:       
                # TODO: Decide what to do if the user has no Facebook email address
                if 'email' not in facebook_user:
                    facebook_user['email'] = ''
                
                if 'last_name' not in facebook_user:
                    facebook_user['last_name'] = ''
                 
                user = User.objects.create_user(
                    username=password,
                    first_name=facebook_user['first_name'],
                    last_name=facebook_user['last_name'],
                    email=facebook_user['email'])
            except:
                user = None
            
            if user:
                user.password = password
                user.save()
                up = UserProfile()
                up.user = user
                up.save()
                
                get_avatar_from_facebook(user.id, facebook_user_id)
                
                user.backend = 'django.contrib.auth.backends.ModelBackend'
                login(request, user)
                merge_locations(request)
                return JsonResponse({'userID': user.id})
            
            response = {'message': 'GENERIC'}
            return JsonResponseForbidden(response)                     
    else:
        response = {'message': 'MISSING'}
        return JsonResponseForbidden(response)


@ensure_csrf_cookie
@require_POST
def rest_login(request):
    # This is weird case
    if request.user.is_authenticated():
        django.contrib.auth.logout(request)

    p = json.loads(request.body.decode())

    if 'email' in p and 'password' in p:
        email = p['email']
        password = p['password']
        
        if User.objects.filter(email=email).count():
            # User already exists. Try logging them in
            user = authenticate(username=email, password=password)
            
            if user:
                login(request, user)
                merge_locations(request)
                return JsonResponse({'userID': user.id})
            else:
                response = {'message': 'INCORRECT_PASSWORD'}
                return JsonResponseForbidden(response)
        else:
            response = {'message': 'EMAIL_NOT_FOUND'}
            return JsonResponseForbidden(response)
    elif 'facebook' in p:
        user = facebook_to_user(request) 
        if user:
            return JsonResponse({'userID': user.id})
        else:
            return JsonResponseForbidden({'message': 'USER_NOT_FOUND'})
    
    response = {'message': 'EMPTY'}
    return JsonResponseForbidden(response)


@ensure_csrf_cookie
@require_POST
def rest_password(request):
    p = json.loads(request.body.decode())
    
    if 'lost' in p:
        # This is weird case
        if request.user.is_authenticated():
            django.contrib.auth.logout(request)
    
        if 'email' in p:
            email = p['email']
            
            if User.objects.filter(email=email).count():
                user = User.objects.filter(email=email).first()
                
                if user.username.startswith('facebook$'):
                    response = {'message': 'FACEBOOK'}
                    return JsonResponseForbidden(response)                
                else:
                    # Success
                    send_password_reset_email(user)
                    return HttpResponseNoContent()
            else:
                response = {'message': 'EMAIL_NOT_FOUND'}
                return JsonResponseForbidden(response)
    elif 'password' in p:
        if request.user.is_authenticated():
            password = p['password']
            username = request.user.username
            
            user = request.user
            user.password = make_password(p['password'])
            user.save(update_fields=['password'])
            
            user = authenticate(username=username, password=password)
            if user:
                login(request, user)
            
            return HttpResponseNoContent()
    
    return JsonResponseForbidden({})


@ensure_csrf_cookie
@require_POST
def rest_logout(request):
    django.contrib.auth.logout(request)
    
    # Clear the Facebook cookie too
    fb_cookie_name = "fbsr_" + settings.FACEBOOK_APP_ID
    
    response = JsonResponse({'success': True})
    response.delete_cookie(fb_cookie_name, domain=settings.COOKIE_DOMAIN)

    return response


def error(request):
    raise Exception('Forcing a server error')


@ensure_csrf_cookie
@ensure_user_authorized
def rest_friends(request):
    user = request.user
    
    if request.method == 'GET':
        # Return a list of this user's friends. We need the ID, name, and avatar
        
        sentConnectionRequests = Friendship.objects.filter(user=user).values_list('friend__id', flat=True).order_by('-time')
        connectionRequests = Friendship.objects.filter(friend=user).values_list('user__id', flat=True).order_by('-time')
        
        connections = list(set(sentConnectionRequests).intersection(connectionRequests))
        connectionRequests = list(set(connectionRequests).difference(connections))
        sentConnectionRequests = list(set(sentConnectionRequests).difference(connections))

        all_ids = connections + connectionRequests + sentConnectionRequests
        
        data = UserProfile.objects.filter(user__id__in=all_ids)
        
        user_data = {}
        for v in data:
            user_data[v.user.id] = {
                'name': get_display_name(user, v.user),
                'avatar_url': v.avatar_url,
            }
        
        return JsonResponse({
            'userData': user_data,
            'connections': connections,
            'sentConnectionRequests': sentConnectionRequests,
            'connectionRequests': connectionRequests
        })
        
    elif request.method == 'POST':
        p = json.loads(request.body.decode())
        user_id_from = request.user.id
        user_id_to = p['recipient']
        connection = p['connect']
        
        send_connection(user_id_from, user_id_to, connection)
        
        if connection:
            # Create a connection
            friendship = Friendship()
            friendship.user = user
            friendship.friend = User.objects.get(id=user_id_to)
            friendship.save()
            notify_of_connection_request(user_id_from, user_id_to)
        else:
            # Destroy a connection, either way
            otherUser = User.objects.get(id=user_id_to)
            Friendship.objects.filter(user=request.user, friend=otherUser).delete()
            Friendship.objects.filter(friend=request.user, user=otherUser).delete()
            
        return HttpResponseNoContent()
    
    raise PermissionDenied


def full_debug(request):
    template = Template('<!DOCTYPE html><html><head><title>Debug</title></head><body>{{facebook_user_id}}</body></html>')
    context = RequestContext(request)
    context['facebook_user_id'] = facebook.get_user_from_cookie(request.COOKIES, settings.FACEBOOK_APP_ID, settings.FACEBOOK_APP_SECRET) 
    return HttpResponse(template.render(context))    
   
    
# This is really quite magical
def jump_to_connection(request, token):
    if len(token):
        fs = Friendship.objects.filter(autologin_token=token)
        
        if fs.count():
            fs = fs.first()
            if fs.autologin_token_expiration > timezone.now():
                # We have a valid login token
                user_id = fs.friend_id
            
                # Log the user in    
                user = User.objects.get(id=user_id)
                user.backend = 'django.contrib.auth.backends.ModelBackend'
                login(request, user)
                
                # Expire the login token
                fs.autologin_token = None
                fs.autologin_token_expiration = timezone.now()
                fs.save(update_fields=['autologin_token', 'autologin_token_expiration'])            
                
                # Redirect the user to the appropriate page now
                return redirect('/app/0/connections/-/0/')
    
    if request.user.is_authenticated():
        return redirect('/app/0/connections/-/0/')
    else:
        return redirect('/app/0/connections/-/0/login')


@permission_required('profiles.can_view_stats')
@never_cache
def generate_marketing_tokens(request):
    users = User.objects.all()
    
    for user in users:
        ma = MarketingAutologin()
        ma.user = user
        ma.autologin_token = generate_autologin_token()
        ma.autologin_token_expiration = generate_autologin_expiration(30)
        ma.destination_url = '/app/-/map/-/0/'
        ma.save()

    return JsonResponse({'done': 'and dusted'})


@permission_required('profiles.can_view_stats')
@never_cache
def marketing_user_list(request):
    users = User.objects.all()
    
    for user in users:
        ma = MarketingAutologin()
        ma.user = user
        ma.autologin_token = generate_autologin_token()
        ma.autologin_token_expiration = generate_autologin_expiration(30)
        ma.destination_url = '/app/-/map/-/0/'
        ma.save()
            
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="shared_earth_marketing_user_list.csv"'
    writer = csv.writer(response)
    
    cursor = connection.cursor()
    cursor.execute("""SELECT 
  this_user.id AS user_id, 
  this_user.first_name as first_name,
  this_user.last_name as last_name,
  this_user.email, 

  'https://sharedearth.com/mkt/'||(SELECT autologin_token FROM profiles_marketingautologin WHERE profiles_marketingautologin.user_id = this_user.id ORDER BY profiles_marketingautologin DESC LIMIT 1) AS autologin_url,   

  this_user.date_joined, 
  this_user.last_login,

  (SELECT COUNT(id) FROM locations_location WHERE locations_location.user_id=this_user.id) AS num_locations,
  (SELECT city FROM locations_location WHERE locations_location.user_id=this_user.id ORDER BY locations_location.id DESC LIMIT 1) AS latest_city,
  (SELECT state FROM locations_location WHERE locations_location.user_id=this_user.id ORDER BY locations_location.id DESC LIMIT 1) AS latest_state,
  (SELECT zip FROM locations_location WHERE locations_location.user_id=this_user.id ORDER BY locations_location.id DESC LIMIT 1) AS latest_zip,

  (SELECT COUNT(id) FROM messaging_conversationmember WHERE messaging_conversationmember.user_id = this_user.id) AS num_conversations,
  (SELECT COUNT(id) FROM messaging_message WHERE messaging_message.sender_id = this_user.id) AS num_messages_sent,
  (SELECT COUNT(id) FROM messaging_message WHERE messaging_message.sender_id != this_user.id AND messaging_message.conversation_id IN (SELECT conversation_id FROM messaging_conversationmember WHERE messaging_conversationmember.user_id = this_user.id)) AS num_messages_received,
  (SELECT COUNT(id) FROM profiles_friendship WHERE profiles_friendship.user_id = this_user.id) AS num_connections_sent,
  (SELECT COUNT(id) FROM profiles_friendship WHERE profiles_friendship.friend_id = this_user.id) AS num_connections_received
FROM 
  auth_user this_user
WHERE 
  this_user.id > 2 AND this_user.id != 844
GROUP BY
  this_user.id
ORDER BY
  user_id ASC;
""")

    result = cursor.fetchall()
    
    writer.writerow([i[0] for i in cursor.description])
    
    for row in result:
        writer.writerow(row)
            
    return response


@permission_required('profiles.can_view_stats')
@never_cache
def user_list(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="shared_earth_user_list.csv"'
    writer = csv.writer(response)
    
    cursor = connection.cursor()
    cursor.execute("""SELECT 
  this_user.id AS user_id, 
  this_user.first_name as first_name,
  this_user.last_name as last_name,
  this_user.email, 

  this_user.date_joined, 
  this_user.last_login,

  (SELECT COUNT(id) FROM locations_location WHERE locations_location.user_id=this_user.id) AS num_locations,
  (SELECT city FROM locations_location WHERE locations_location.user_id=this_user.id ORDER BY locations_location.id DESC LIMIT 1) AS latest_city,
  (SELECT state FROM locations_location WHERE locations_location.user_id=this_user.id ORDER BY locations_location.id DESC LIMIT 1) AS latest_state,
  (SELECT zip FROM locations_location WHERE locations_location.user_id=this_user.id ORDER BY locations_location.id DESC LIMIT 1) AS latest_zip,

  (SELECT COUNT(id) FROM messaging_conversationmember WHERE messaging_conversationmember.user_id = this_user.id) AS num_conversations,
  (SELECT COUNT(id) FROM messaging_message WHERE messaging_message.sender_id = this_user.id) AS num_messages_sent,
  (SELECT COUNT(id) FROM messaging_message WHERE messaging_message.sender_id != this_user.id AND messaging_message.conversation_id IN (SELECT conversation_id FROM messaging_conversationmember WHERE messaging_conversationmember.user_id = this_user.id)) AS num_messages_received,
  (SELECT COUNT(id) FROM profiles_friendship WHERE profiles_friendship.user_id = this_user.id) AS num_connections_sent,
  (SELECT COUNT(id) FROM profiles_friendship WHERE profiles_friendship.friend_id = this_user.id) AS num_connections_received
FROM 
  auth_user this_user
WHERE 
  this_user.id > 2 AND this_user.id != 844
GROUP BY
  this_user.id
ORDER BY
  user_id ASC;
""")

    result = cursor.fetchall()
    
    writer.writerow([i[0] for i in cursor.description])
    
    for row in result:
        writer.writerow(row)
            
    return response
    

# This is really quite magical
def jump_to_marketing(request, token):
    if len(token):
        mas = MarketingAutologin.objects.filter(autologin_token=token)
        
        if mas.count():
            ma = mas.first()
            if ma.autologin_token_expiration > timezone.now():
                # Log the user in    
                user = ma.user
                user.backend = 'django.contrib.auth.backends.ModelBackend'
                login(request, user)
                
                # Expire the login token
                ma.autologin_token_expiration = timezone.now()
                ma.save(update_fields=['autologin_token', 'autologin_token_expiration'])            
                
                # Redirect the user to the appropriate page now
                return redirect(ma.destination_url)
            else:
                if request.user.is_authenticated():
                    return redirect(ma.destination_url)
                else:
                    return redirect(ma.destination_url + 'login')
    
    return redirect('/')


@permission_required('profiles.can_view_stats')
@never_cache
def stats(request):
    
    cursor = connection.cursor()
    cursor.execute("SELECT date_added::date AS d,count(date_added::date) FROM locations_location WHERE date_added::date >= '2015-05-23' GROUP BY d ORDER BY d ASC")
    result = cursor.fetchall()
    
    rows = []
    for row in result:
        r = {}
        r['date'] = row[0].strftime("%Y-%m-%d")
        r['count'] = row[1]
        rows.append(r)
        
    cursor = connection.cursor()
    cursor.execute("SELECT date_trunc('month',date_added::date) AS d,count(date_trunc('month', date_added::date)) FROM locations_location WHERE date_added::date >= '2015-05-23' GROUP BY d ORDER BY d ASC")
    result = cursor.fetchall()
    
    rows2 = []
    for row in result:
        r = {}
        r['date'] = row[0].strftime("%Y-%m")
        r['count'] = row[1]
        rows2.append(r)
    
    context = RequestContext(request)
    context['by_day'] = rows
    context['by_month'] = rows2
    template = loader.get_template('stats.html')
    response = HttpResponse(template.render(context))
    return response
   

    
"""
def migrate1(request):
    import _mysql

    user_count = 0
    
    db = _mysql.connect(host="localhost", user="root", db="old_shared_earth")
    query = 'SELECT DISTINCT node.uid,node.type,FROM_UNIXTIME(users.created) AS created,FROM_UNIXTIME(users.access) AS access,fbid,users.name,users.pass as password,mail as email,street,additional,city,province,country FROM location as loc INNER JOIN location_instance as li ON loc.lid = li.lid INNER JOIN node ON node.nid=li.nid INNER JOIN users ON users.uid=node.uid LEFT JOIN fboauth_users AS fb ON users.uid=fb.uid WHERE country="us" AND (type = "garden" OR type="gardener")  ORDER BY uid'
    db.query(query)
    
    r = db.use_result()
    rows = {}
    
    def load_json(fn):
        with open(fn, "r") as f:
            return json.loads(f.read())
    
    while True:
        row = r.fetch_row(how=1)
        
        if len(row) == 0:
            break
            
        row = row[0]
        rows[row['uid']] = row
    
    fbur = load_json("../sharedearth-scripts/users-final.txt")
    
    fbu = {}
    for u in fbur:
        fbu[u['id']] = u
    
    users = {}
    
    for uid in rows:
        data = rows[uid]
        
        user = {}
        
        if data['fbid'] and data['fbid'] in fbu:
            fb = fbu[data['fbid']]
            user['fbid'] = data['fbid']
            user['first_name'] = fb['first_name']
            user['last_name'] = fb['last_name']
            
            user['password'] = 'facebook$' + data['fbid']
            
            if 'email' in fb:
                user['email'] = fb['email']
            else:
                user['email'] = "invalid-" + data['fbid'] + "@facebook.com"
        else:
            user['email'] = data['email'].decode()
            
            n = data['name'].decode().split(' ')
            
            if len(n) > 1:
                user['first_name'] = ' '.join(n[:-1])
                user['last_name'] = n[-1]
            else:
                user['first_name'] = n[0]
                user['last_name'] = ''
    
            user['password'] = "drupal$" + data['password'].decode()
    
        if user['email'] in users:
            print("duplicate email", user['email'])
            
        users[user['email']] = user
    
    for key in users:
        
        data = users[key]
        
        if len(data["email"]):
        
            q = User.objects.filter(email=data["email"])
            if q.count() == 0:
                
                if True:
                    user = User.objects.create_user(
                        username=(str(user_count) + data["email"])[:30],
                        email=data["email"],
                        first_name=data["first_name"][:30].title(),
                        last_name=data["last_name"][:30].title(),
                        # password='' #data["password"],
                    )
        
                    user.password = data["password"]
                    user.save()
                    
                    up = UserProfile()
                    up.user = user
                    up.avatar_url = staticfiles.static('profiles/img/avi-neutral-square.png')
                    up.save()  
                    
                    user_count += 1
                    # if user_count > 3:
                    #    break
            else:
                for user in q:
                    if UserProfile.objects.filter(user=user).count() == 0:
                        user_count += 1
                        up = UserProfile()
                        up.user = user
                        up.avatar_url = staticfiles.static('profiles/img/avi-neutral-square.png')
                        up.save()  
                    
                    # pass
                    # user.password = data["password"]
                    # user.save(update_fields=['password'])
                    # user_count += 1
                    # up = UserProfile()
                    # up.user = user
                    # up.avatar_url = staticfiles.static('profiles/img/avi-neutral-square.png')
                    # up.save()
                    # user.first_name = users[user.email]['first_name'][:30].title()
                    # user.last_name = users[user.email]['last_name'][:30].title()
                    # user.save()
    
    return JsonResponse({'users': user_count})


def migrate2(request):
    import _mysql
    from dateutil import parser
    from locations.models import Location
    import shortuuid

    import pytz
    
    db = _mysql.connect(host="localhost", user="root", db="old_shared_earth")
    query = 'SELECT DISTINCT node.uid,node.type,FROM_UNIXTIME(users.created) AS created,FROM_UNIXTIME(users.access) AS access,fbid,users.name,users.pass as password,mail as email,street,additional,city,province,country FROM location as loc INNER JOIN location_instance as li ON loc.lid = li.lid INNER JOIN node ON node.nid=li.nid INNER JOIN users ON users.uid=node.uid LEFT JOIN fboauth_users AS fb ON users.uid=fb.uid WHERE country="us" AND (type = "garden" OR type="gardener")  ORDER BY uid'
    db.query(query)
    
    r = db.use_result()
    rows = {}
    
    def load_json(fn):
        with open(fn, "r") as f:
            return json.loads(f.read())
    
    while True:
        row = r.fetch_row(how=1)
        
        if len(row) == 0:
            break
            
        row = row[0]
        rows[row['uid']] = row

    locations = load_json('../sharedearth-scripts/processed-locations.txt')
    location_count = 0

    for key in locations:
        loc = locations[key]
        
        if key in rows:
            row = rows[key]
            
            gardenAvatar = staticfiles.static('profiles/img/avi-garden-square.png')
            gardenerAvatar = staticfiles.static('profiles/img/avi-gardener-square.png')
            gardenMapIcon = staticfiles.static('profiles/img/have-land-96.png')
            gardenerMapIcon = staticfiles.static('profiles/img/need-land-96.png')
        
            location = Location()
            
            try:
                location.user = User.objects.get(email=row['email'])
                
                if Location.objects.filter(user=location.user).count() == 0:
                    # These fields are required in orde
                    location.formatted_address = loc['formatted_address']
                                                       
                    location.street_number = loc['street_number']
                    location.street_name = loc['street_name']
                    location.city = loc['city']
                    location.state = loc['state']
                    location.county = loc['county']
                    location.zip = loc['zip']
                    location.zip_plus_four = loc['zip_plus_four']
                    location.country = loc['country']
                                          
                    location.latitude = loc['latitude']
                    location.longitude = loc['longitude'] 
                    location.type = loc['type'] 
                                          
                    location.uuid = shortuuid.uuid()
                    
                    # See https://developers.google.com/maps/documentation/timezone/
                    
                    utc_dt = parser.parse(loc['date_added']).replace(tzinfo=pytz.utc) 
                    location.date_added = utc_dt
                                          
                    location.avatar_url = gardenAvatar if loc['type'] == 'have' else gardenerAvatar 
                    location.map_icon_url = gardenMapIcon if loc['type'] == 'have' else gardenerMapIcon 
                
                    location.save()     
                    location_count += 1
            except:
                pass 

    return JsonResponse({'locations': location_count})


def migrate3(request):

    import _mysql
    import shortuuid
    from locations.models import Location
    from images.models import Image
    
    db = _mysql.connect(host="localhost", user="root", db="old_shared_earth")
    query = 'SELECT DISTINCT node.uid,node.type,FROM_UNIXTIME(users.created) AS created,FROM_UNIXTIME(users.access) AS access,fbid,users.name,users.pass as password,mail as email,street,additional,city,province,country FROM location as loc INNER JOIN location_instance as li ON loc.lid = li.lid INNER JOIN node ON node.nid=li.nid INNER JOIN users ON users.uid=node.uid LEFT JOIN fboauth_users AS fb ON users.uid=fb.uid WHERE country="us" AND (type = "garden" OR type="gardener")  ORDER BY uid'
    db.query(query)
    
    r = db.use_result()
    rows = {}
    
    def load_json(fn):
        with open(fn, "r") as f:
            return json.loads(f.read())
    
    while True:
        row = r.fetch_row(how=1)
        
        if len(row) == 0:
            break
            
        row = row[0]
        
        if row['fbid']:
            rows[row['fbid']] = row

    images = load_json('../sharedearth-scripts/fb-images.json')
    image_count = 0
    
    skip = []
    done = []

    print(len(images), len(rows))
    
    for key in images:
        im = images[key]
        
        if key in rows:
            row = rows[key]
            
            image = Image()
            try:
                image.user = User.objects.get(email=row['email'])
                image.location = Location.objects.get(user=image.user)
                image.uuid = str(shortuuid.uuid())
                image.url_large = im['url_large']
                image.url_medium = im['url_medium']
                image.map_icon_url = im['url_map']
                image.url_avatar = im['url_avatar']
                
                done.append(row['email'].decode())
                image.save()
                
                image.location.avatar_url = image.url_avatar
                image.location.map_icon_url = image.map_icon_url 
                image.location.save()
                
                profile = UserProfile.objects.get(user=image.user)
                profile.avatar_url = image.url_avatar
                profile.save()
                   
                image_count += 1
            except:
                skip.append(row['email'].decode())
                pass
        else:
            skip.append(key)
    
    return JsonResponse({'images': image_count, 'done': done, 'skip': skip})


def migrate4(request):

    import _mysql
    import shortuuid
    from locations.models import Location
    from images.models import Image
    
    db = _mysql.connect(host="localhost", user="root", db="old_shared_earth")
    query = 'SELECT DISTINCT node.uid,node.type,FROM_UNIXTIME(users.created) AS created,FROM_UNIXTIME(users.access) AS access,fbid,users.name,users.pass as password,mail as email,street,additional,city,province,country FROM location as loc INNER JOIN location_instance as li ON loc.lid = li.lid INNER JOIN node ON node.nid=li.nid INNER JOIN users ON users.uid=node.uid LEFT JOIN fboauth_users AS fb ON users.uid=fb.uid WHERE country="us" AND (type = "garden" OR type="gardener")  ORDER BY uid'
    db.query(query)
    
    r = db.use_result()
    rows = {}
    
    def load_json(fn):
        with open(fn, "r") as f:
            return json.loads(f.read())
    
    while True:
        row = r.fetch_row(how=1)
        
        if len(row) == 0:
            break
            
        row = row[0]
        
        rows[row['uid']] = row

    images = load_json('../sharedearth-scripts/legacy-images.json')
    image_count = 0
    
    skip = []
    done = []

    print(len(images), len(rows))
    
    for key in images:
        im = images[key]
        
        if key in rows:
            row = rows[key]
            
            image = Image()
            try:
                image.user = User.objects.get(email=row['email'])
                image.location = Location.objects.get(user=image.user)
                image.uuid = str(shortuuid.uuid())
                image.url_large = im['url_large']
                image.url_medium = im['url_medium']
                image.map_icon_url = im['url_map']
                image.url_avatar = im['url_avatar']
                
                done.append(row['email'].decode())
                image.save()
                
                image.location.avatar_url = image.url_avatar
                image.location.map_icon_url = image.map_icon_url 
                image.location.save()
                
                profile = UserProfile.objects.get(user=image.user)
                profile.avatar_url = image.url_avatar
                profile.save()
                   
                image_count += 1
            except:
                skip.append(row['email'].decode())
                pass
        else:
            skip.append(key)
    
    return JsonResponse({'images': image_count, 'done': done, 'skip': skip})


def migrate5(request):

    import _mysql
    from locations.models import Location
    
    db = _mysql.connect(host="localhost", user="root", db="old_shared_earth")
    query = '''SELECT DISTINCT
  node.uid,node.type, FROM_UNIXTIME(users.created) AS created,FROM_UNIXTIME(users.access)AS access,fbid,users.name,mail AS email,street,additional,city,
  province,country,content_field_profile_headline.field_profile_headline_value AS headline,
  content_field_profile_description.field_profile_description_value AS description
FROM location as loc
INNER JOIN location_instance as li ON loc.lid = li.lid
INNER JOIN node ON node.nid=li.nid
INNER JOIN users ON users.uid=node.uid
LEFT JOIN fboauth_users AS fb ON users.uid=fb.uid
LEFT JOIN content_field_profile_description ON content_field_profile_description.nid = node.nid
LEFT JOIN content_field_profile_headline ON content_field_profile_headline.nid = node.nid
WHERE country="us" AND (type = "garden" OR type="gardener")  ORDER BY uid'''
    db.query(query)
    
    r = db.use_result()
    rows = {}
    
    def load_json(fn):
        with open(fn, "r") as f:
            return json.loads(f.read())
    
    while True:
        row = r.fetch_row(how=1)
        
        if len(row) == 0:
            break
            
        row = row[0]
        
        rows[row['uid']] = row
        
    done = []
    skip = []
    what = []
        
    for uid in rows:
        row = rows[uid]
        headline = row['headline']
        description = row['description']
        
        if headline:
            headline = headline.decode('cp437').encode("ascii", "ignore").decode()
        if description:
            description = description.decode('cp437').encode("ascii", "ignore").decode()
        
        text = ""
        
        if headline and description:
            text = headline + "\r\n\r\n" + description
        elif headline:
            text = headline
        else:
            text = description 
        
        try:
            if text:
                text = text.replace("\r\n", "\n")
                
                gardenType = row['type'].decode()
                user = User.objects.get(email=row['email'])

                # profile = UserProfile.objects.get(user=user)
                # profile.about = None
                # profile.save()

                # location = Location.objects.get(user=user)
                # location.about = None
                # location.save()

                if gardenType == 'gardener':
                    profile = UserProfile.objects.get(user=user)
                    profile.about = text
                    profile.save()
                    done.append(uid)
                elif gardenType == 'garden':
                    location = Location.objects.get(user=user)
                    location.about = text
                    location.save()
                    done.append(uid)
                else:
                    what.append(gardenType)
                    skip.append(uid)
                
        except:
            skip.append(uid)
        
    return JsonResponse({'done': done, 'skip': skip, 'what': what})
"""

# EOF
