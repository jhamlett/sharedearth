function updateUrlOnTabChange ( )
{
  updateUrl()
}

function updateUrl ( )
{
  var url;
  
  // Build a canoncial URL
  var leftTab = $('#tabs-left>dt.selected').data('tab');
  
  if ( leftTab == 'places' )
  {
    var location = $('#locations-list>li.active').data('id');
    var rightTab = $('#tabs-right>dt.selected').data('tab');
    url = "/" + leftTab + "/" + location + "/" + rightTab;
  }
  else
  {
    // TODO: Messages URLs
    url = "/" + leftTab;
  }
  
  history.replaceState(history.state, "", url);
}

function changeProfile ( location_id )
{
  changePhotosLocation(location_id);
}

var previousTab;

function viewLocationProfile ( location_id )
{
  dynamicGet('/locations/profile/', {id:location_id}, function()
  {
    var curTab = $('#tabs-right dt.selected').first();
    
    if ( curTab.attr('id') != 'tab-profile' ) 
      previousTab = curTab;
      
    //$('#tab-profile').html("Profile");
    //selectTab('#tab-profile');
    
    mainScope.showProfileTab(location_id);
    makeTinyMap();
  });
}

function profileBack ( )
{
  /*if ( previousTab )
  {
    previousTab.trigger("click");
    previousTab = null;
  }*/
  window.history.back();
}

function logOut ( )
{
  $.post('/logout/').done(function() {
    window.location.replace('/');
  });
}

function activateEditForm ( )
{
  $('#edit-location-details input, #edit-location-details textarea, #edit-location-details select').each(function( index ) {
    if ( $(this).data('link') == 'on-change' )
    {
      // Make this dynamic
      var element = $(this);
      var locationId = $('#edit-location-details').data('location-id');
      var fieldName = $(this).data('field');
      $(this).blur(function() {
        var val = element.val();
        $.post('/locations/change_field/', {location_id: locationId, field_name: fieldName, value: element.val()});
      });
    }
  });
}