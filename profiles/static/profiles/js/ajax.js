
function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

$(function()
{
  //var csrftoken = ;
  
  $.ajaxSetup({
    cache: false,
    beforeSend: function(xhr, settings) {
      if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
        xhr.setRequestHeader("X-CSRFToken", $.cookie('csrftoken'));
      }
     }
  });
});

function processDynamic (r)
{
  var data = r.response;
  for ( var i = 0; i < data.length; ++i )
  {
    var d = data[i];
    if ( d.action == 'html' )
    {
      $(d.what).html(d.content);
    }
    else if ( d.action == 'replaceWith' )
      $(d.what).replaceWith(d.content);
    else if ( d.action == 'eval' )
      eval(d.code);
  }
}

function dynamicGet( url, data, callbackAfter )
{
  var a = (data === undefined || data === null) ? $.getJSON(url) : $.getJSON(url, data);  
  
  a.done(function(r) {
    processDynamic(r);
    if ( callbackAfter )
    {
      callbackAfter();
    }
  })
  .fail(function() {
    // Failure. I suppose we have no reasonable course of action, other than ignorance.
  });
}

function dynamicPost ( url, data, callbackAfter )
{
  $.post(url, data, function(r) {
    processDynamic(r);
    if ( callbackAfter )
    {
      callbackAfter();
    }
  });
} 
