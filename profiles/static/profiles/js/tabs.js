function installTabs ( )
{
  $('.tabs > dt').click(function() {
    if ( $(this).hasClass('need-account') )
    {
      requireAccount();
    }
    else
    {
      $(this).addClass('selected');
      $(this).siblings().removeClass('selected');
    
      // Update our URL
      updateUrlOnTabChange();
    }
  });  
}

function selectTab ( tabID )
{
  console.log("Legacy tab change to ", tabID);
  debugger;
}

$(function()
{
  //installTabs();
});