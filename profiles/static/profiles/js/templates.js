function templateToString ( templateId, replace )
{
  // Insert template values
  var str = document.getElementById(templateId).innerHTML;
  for ( var key in replace )
  {
    str = str.split("[[" + key + "]]").join(replace[key]);
  }

  // If we need to later, we could make [[key=default]] work fairly easily here.
  // Not doing it yet because we don't need it yet.
  
  // Make all the others empty 
  str = str.replace(/\[\[.*?\]\]/g, "");
  return str;
}

// This function implements our lightweight template system.
// It expands the named template into the specified div (or
// other element, by ID) while replacing the specified
// variables.
//
// For example:
// { location : 'Here' }
// replaces all occurances of :[[location]]" with "Here"
function expandTemplate ( templateId, divId, replace )
{
  document.getElementById(divId).innerHTML = templateToString(templateId, replace);
}

function storeTemplateInData ( templateName, jQueryElement, dataName, replace )
{
  jQueryElement.data(dataName, templateToString("template-" + templateName, replace));
}

function doModal ( name, replace )
{
  expandTemplate("template-" + name, "modal", replace);
}

function closeModal ( )
{
  document.getElementById("modal").innerHTML = '';
}

