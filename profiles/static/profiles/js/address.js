
var selectedPlace;

function getAutocompleteComponents ( place )
{
  var address_components = place.address_components;
  var components={}; 
  jQuery.each(address_components, function(k,v1) {jQuery.each(v1.types, function(k2, v2){components[v2]=v1.long_name});});
  return components;    
}

function newLocation ( type )
{
  if ( $('form#lookup input[name=longitude]').val() )
  {
    // Hide the new address box
    $('#location-expansion').click();
    
    var lat = $('form#lookup input[name=latitude]').val();
    var lng = $('form#lookup input[name=longitude]').val();
    var address = $('form#lookup input[name=address]').val();
    var place = selectedPlace;
   
    var components = getAutocompleteComponents(place);
    
    // Clear the address field for a new search later. 
    $('#addressField').val("");

    // Add it to our user
    delete components.political; // If there
    components.formatted_address = address;
    components.latitude = lat;
    components.longitude = lng;
    components.type = type;
    
    $.post("/locations/add/", components, function(result) {
      var newLocationId = result.id;
      dynamicGet("/locations/render_list/", null, function() {
        $('#locations-list').removeClass('hidden');
        createLocationIcons();
        selectLocation(newLocationId);
      });
    });
    
  }
  else
  {
    $('#addressField').focus();
  }
}

function locationChanged ( place )
{
  selectedPlace = place;
  $('form#lookup input[name=address]').val(place.formatted_address);
  $('form#lookup input[name=longitude]').val(place.geometry.location.lng());
  $('form#lookup input[name=latitude]').val(place.geometry.location.lat());
}

var autocompleteOptions = {
  types: ['geocode'],
  componentRestrictions: {country: 'us'}
};

var autocompleteGuesses = { };
var initialAutocompleteService;
var guessingLocation = false;

function doAutoComplete ( str, func )
{
  str = '' + str;
  
  var placeOptions = {
    'input': str,
    'offset': str.length
  };
  
  jQuery.extend(placeOptions, autocompleteOptions);

  initialAutocompleteService.getPlacePredictions(placeOptions, func);
} 

function getPlaceCity ( place )
{
  var address_components = place.address_components;
  var components={}; 
  jQuery.each(address_components, function(k,v1) {jQuery.each(v1.types, function(k2, v2){components[v2]=v1.long_name});});
  
  if ( 'street_number' in components )
  {
    if ( 'locality' in components && 'administrative_area_level_1' in components )
    {
      return components.locality + ', ' + components.administrative_area_level_1; 
    }
  }
  
  return null;
}

function guessCity ( i )
{
  doAutoComplete('' + i, function(list, status)
  {
    var element = document.createElement('div');
    var placesService = new google.maps.places.PlacesService(element);
    placesService.getDetails(
      {'reference': list[0].reference},
      function detailsresult(place, placesServiceStatus)
      {
        var city = getPlaceCity(place);
        
        if ( city )
        {
          if ( city in autocompleteGuesses )
          {
            // We are done! We need to autocomplete for this city now.
            doAutoComplete(city, function()
            {
              placesService.getDetails(
                {'reference': list[0].reference}, function (place, placesServiceStatus)
                {
                  var lat = place.geometry.location.lat();
                  var lng = place.geometry.location.lng();
                  
                  runWhenMapIsReady(function(){
                    //console.log("Moving to ", lat, lng);
                    // TODO - Why doesn't this work anymore?
                    showMap();
                    var c = new google.maps.LatLng(lat, lng);
                    map.setCenter(c);

                  });  
              });             
            });
          }
          else
          {
            autocompleteGuesses[city] = true;
            
            if ( i < 4 )
              guessCity(i + 1);
            
            // Otherwise, give up.
          }
        }
      });
  });
}

function guessInitialLocation ( )
{
  guessingLocation = true;
  initialAutocompleteService = new google.maps.places.AutocompleteService();
  autocompleteGuesses = { };
  guessCity(1); 
}

function installAutocomplete ( )
{
  var elements = document.getElementsByClassName('address-autocomplete');

  for ( var i = 0; i < elements.length; ++ i )
  {
    var autocompleteElement = elements[i];
    var autocomplete = new google.maps.places.Autocomplete(autocompleteElement, autocompleteOptions);
    
    google.maps.event.addListener(autocomplete, 'place_changed', function ()
    {
        var place = autocomplete.getPlace();
        //document.getElementById('city2').value = place.name;
        //document.getElementById('cityLat').value = place.geometry.location.lat();
        //document.getElementById('cityLng').value = place.geometry.location.lng();
        
        if ( typeof place.address_components == 'undefined' )
        {
          // The user pressed enter in the input 
          // without selecting a result from the list
          // Let's get the list from the Google API so that
          // we can retrieve the details about the first result
          // and use it (just as if the user had actually selected it)
          var autocompleteService = new google.maps.places.AutocompleteService();
  
          var placeOptions = {
            'input': place.name,
            'offset': place.name.length
          };
          jQuery.extend(placeOptions, autocompleteOptions);
  
          autocompleteService.getPlacePredictions(
            placeOptions,
            function listentoresult ( list, status )
            {
              if(list === null || list.length === 0) {
                // There are no suggestions available.
                // The user saw an empty list and hit enter.
              } else {
                // Here's the first result that the user saw
                // in the list. We can use it and it'll be just
                // as if the user actually selected it
                // themselves. But first we need to get its details
                // to receive the result on the same format as we
                // do in the AutoComplete.
                var placesService = new google.maps.places.PlacesService(autocompleteElement);
                
                placesService.getDetails(
                  {'reference': list[0].reference},
                  function detailsresult(detailsResult, placesServiceStatus)
                  {
                    //setLocation(map, detailsResult);
                    $(autocompleteElement).blur();
                    $(autocompleteElement).val(detailsResult.formatted_address);
                    locationChanged(detailsResult);
                  }
                );
              }
            }
          );
        }
        else
        {
          locationChanged(place);
        }
    }); 
  }   
}
