from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
from taggit.managers import TaggableManager
from taggit.models import TaggedItemBase
from django.utils import timezone
from locations.models import Location
import shortuuid
from datetime import timedelta


# User.display_name = property(lambda u: 
#     "%s %s." % (u.first_name, u.last_name[:1])
# )


class TaggedWhatIGrow(TaggedItemBase):
    content_object = models.ForeignKey("UserProfile")


class TaggedInterests(TaggedItemBase):
    content_object = models.ForeignKey("UserProfile")


# TODO: See http://stackoverflow.com/a/965883 for additional details on what
# needs to be configured with this. Doing it as an additional model rather
# than extending the base model intentionally. 
class UserProfile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL)   # Not "User" since we may be using a different model.
    referring_url = models.TextField(default=None, blank=True, null=True)  # The original URL the user came from
    about = models.TextField(default=None, blank=True, null=True)
    
    email_on_new_matches = models.BooleanField(default=True)
    email_on_messages = models.BooleanField(default=True)
    email_on_connect = models.BooleanField(default=True)
    email_on_resources = models.BooleanField(default=True)
    email_on_announcements = models.BooleanField(default=True)
    share_full_name_all = models.BooleanField(default=False)
    share_full_name_connected = models.BooleanField(default=True)
    share_address_connected = models.BooleanField(default=True)
    onboard = models.BooleanField(default=False)

    avatar_url = models.CharField(max_length=256, default=None, blank=True, null=True)

    what_i_grow = TaggableManager(related_name='what_i_grow', through=TaggedWhatIGrow)
    interests = TaggableManager(related_name='interests', through=TaggedInterests)

    EXPERIENCE_CHOICES = (
        (0, "New to gardening"),
        (1, "1 year"),
        (2, "2-5 years"),
        (5, "5-10 years"),
        (10, "10+ years"),
    )
    experience = models.IntegerField(choices=EXPERIENCE_CHOICES, default=2)
    
    RESPONSIBILITIES_CHOICES = (
        (0, "I'd like to work alone"),
        (1, "We could work together if you'd like"),
        (2, "I'm going to need some help")
    )
    responsibilities = models.IntegerField(choices=RESPONSIBILITIES_CHOICES, default=0)

    change_password_token = models.CharField(max_length=256, default=None, blank=True, null=True)
    change_password_token_expiration = models.DateTimeField(default=timezone.now)
    
    def __str__(self):  
        return "%s's profile" % self.user
    
    class Meta:
        permissions = (
            ("can_view_stats", "Can view stats"),
        )    
    

class Friendship(models.Model):
    user = models.ForeignKey(User)
    friend = models.ForeignKey(User, related_name="friendship_friend")
    time = models.DateTimeField(default=timezone.now)
    autologin_token = models.CharField(max_length=256, default=None, blank=True, null=True)
    autologin_token_expiration = models.DateTimeField(default=timezone.now)


class Notification(models.Model):
    user_notified = models.ForeignKey(User)
    location_notified_about = models.ForeignKey(Location)
    time = models.DateTimeField(default=timezone.now)


def generate_autologin_token():
    return str(shortuuid.ShortUUID().random(length=30))


def generate_autologin_expiration(days):
    return timezone.now() + timedelta(days=days)


class MarketingAutologin(models.Model):
    user = models.ForeignKey(User)
    autologin_token = models.CharField(max_length=256, default=None, blank=True, null=True, db_index=True)
    autologin_token_expiration = models.DateTimeField(default=timezone.now)
    destination_url = models.CharField(max_length=512, default=None, blank=True, null=True)
    

# EOF
