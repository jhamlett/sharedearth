from django.conf import settings  # import the settings file


def global_template_vars(request):
    return {
        'TEMPLATE_DNS_PREFETCH': settings.TEMPLATE_DNS_PREFETCH
    }
    
    