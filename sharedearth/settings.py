"""
Django settings for sharedearth project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
import dj_database_url
import djcelery
from django.template.base import add_to_builtins
from sharedearth.celery import app
import urllib.parse

BASE_DIR = os.path.dirname(os.path.dirname(__file__))


def safe_get_env(name):
    if name not in os.environ:
        raise Exception(name + " environment variable not set")
    return os.environ[name]


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
# SECRET_KEY = 'u+lqb7jyo=!qv%yje*e7fv*+ihm$go)aj@ym^etv)+5n&d((=q'
SECRET_KEY = safe_get_env('DJANGO_SECRET_KEY')    

# SECURITY WARNING: don't run with debug turned on in production!
if 'DEBUG_SHAREDEARTH' in os.environ:
    USE_S3 = False
    DEBUG = True
    COLLAPSE_WHITESPACE = 'blank_lines_only'
    TEMPLATE_DEBUG = True
    ALLOWED_HOSTS = []
    SECURE_ONLY = False
    RAVEN = False
else:
    USE_S3 = True
    DEBUG = False
    COLLAPSE_WHITESPACE = True
    TEMPLATE_DEBUG = False
    ALLOWED_HOSTS = ['*']
    # ALLOWED_HOSTS = ['sharedearth.herokuapp.com', 'sharedearth-staging.herokuapp.com', 'sharedearth.com']
    SECURE_ONLY = True
    RAVEN = True

if 'ALLOW_INSECURE' in os.environ:
    SECURE_ONLY = False

if 'FORCE_DEBUG' in os.environ:
    DEBUG = True

WEBSOCKET_URI = safe_get_env('REALTIME_URI')    
WEBSOCKET_SECRET_KEY = safe_get_env('REALTIME_SECRET_KEY')
FACEBOOK_APP_ID = safe_get_env('FACEBOOK_APP_ID')
FACEBOOK_APP_SECRET = safe_get_env('FACEBOOK_APP_SECRET')
FACEBOOK_ACCESS_TOKEN = FACEBOOK_APP_ID + '|' + FACEBOOK_APP_SECRET
MANDRILL_API_KEY = safe_get_env('MANDRILL_APIKEY')                                
COOKIE_DOMAIN = ".sharedearth.com"

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_user_agents',               # Detect browser version easily. See https://github.com/selwin/django-user_agents
    'compressor',
    'storages',
    'taggit',                           # Nice little tagging system    
    'raven.contrib.django.raven_compat',  # Log to sentry

    # django-allauth
    # Facebook integration.
    'django.contrib.sites',             # Required by allauth
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    'allauth.socialaccount.providers.facebook',
    
    # Background tasks
    'djcelery',
    
    # Begin Shared Earth-specific apps
    'angular',                          # Angular support code
    'commands',                         # Additional management commands
    'profiles',                         # Shared Earth user profiles
    'map',                              # The embedded Google map
    'locations',                        # Gardens or gardeners
    'messaging',
    'tags',                             # Tags
    'images',                           # Uploading images to S3, etc.
    'homepage',                         # The homepage
    'resources',                        # Dynamic resources list
)


MIDDLEWARE_CLASSES = (
    # 'django.middleware.gzip.GZipMiddleware',  # This is now handled by nginx
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django_user_agents.middleware.UserAgentMiddleware',            # Adds a user_agent attribute to request
)

AUTHENTICATION_BACKENDS = (
    # Needed to login by username in Django admin, regardless of `allauth`
    "django.contrib.auth.backends.ModelBackend",

    # `allauth` specific authentication methods, such as login by e-mail
    "allauth.account.auth_backends.AuthenticationBackend",
)


if SECURE_ONLY:
    l = list(MIDDLEWARE_CLASSES)
    l.insert(0, 'sslify.middleware.SSLifyMiddleware')  # This must be first
    MIDDLEWARE_CLASSES = tuple(l)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "profiles.context_processors.global_template_vars",
    "django.core.context_processors.request",
    "allauth.account.context_processors.account",
    "allauth.socialaccount.context_processors.socialaccount",
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'compressor.finders.CompressorFinder',
)


PASSWORD_HASHERS = (
    'django.contrib.auth.hashers.BCryptSHA256PasswordHasher',  # Choosing this over scrypt since as of 2015, scrypt is still a bit new.   
    'django.contrib.auth.hashers.PBKDF2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher',
    'django.contrib.auth.hashers.BCryptPasswordHasher',
    'django.contrib.auth.hashers.SHA1PasswordHasher',
    'django.contrib.auth.hashers.MD5PasswordHasher',
    'django.contrib.auth.hashers.CryptPasswordHasher',
    'drupal6_password_hasher.drupal6_password_hasher.Drupal6PasswordHasher'  # This allows the migration of legacy user accounts with shitty MD5 hashing
)

ROOT_URLCONF = 'sharedearth.urls'

WSGI_APPLICATION = 'sharedearth.wsgi.application'

# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {}
DATABASES['default'] = dj_database_url.config() 

# Caching
#
# The instructions from
# http://docs.run.pivotal.io/marketplace/services/memcachedcloud.html#django
# did not work; I found these variables were set, so I'm using them.
#
# Looks like
# https://devcenter.heroku.com/articles/memcachedcloud#using-memcached-from-python
# tells how to do it properly.

if False:
    if 'MEMCACHEDCLOUD_SERVERS' in os.environ:
        CACHES = {
            'default': {
                'BACKEND': 'django_bmemcached.memcached.BMemcached',
                'LOCATION': os.environ['MEMCACHEDCLOUD_SERVERS'].split(','),
                'OPTIONS': {
                    'username': os.environ['MEMCACHEDCLOUD_USERNAME'],
                    'password': os.environ['MEMCACHEDCLOUD_PASSWORD']
                }
            }
        }
    else:
        CACHES = {
            'default': {
                'BACKEND': 'django.core.cache.backends.db.DatabaseCache',
                'LOCATION': 'django_cache',
                'TIMEOUT': None
            }
        }
else:
    # We don't need caching yet.
    if 'REDISCLOUD_URL' in os.environ:
        # redis://redistogo:b9fe24958d5fee75af2300d9295f178a@angelfish.redistogo.com:11296/
        url = urllib.parse.urlparse(os.environ.get('REDISCLOUD_URL'))
        # redis_pool = redis.ConnectionPool(host=url.hostname, port=url.port, password=url.password, max_connections=settings.REDIS_MESSAGING_MAX_CONNECTIONS)
    
        # When using TCP connections
        CACHES = {
            'default': {
                'BACKEND': 'redis_cache.RedisCache',
                'LOCATION': '%s:%s' % (url.hostname, url.port),
                'OPTIONS': {
                    'DB': 0,
                    'PASSWORD': url.password,
                    'PARSER_CLASS': 'redis.connection.HiredisParser',
                    'CONNECTION_POOL_CLASS': 'redis.BlockingConnectionPool',
                    'CONNECTION_POOL_CLASS_KWARGS': {
                        'max_connections': 3,  # Maximum of 10 per database....
                        'timeout': 20,
                    }
                },
            },
        }
    else:    
        CACHES = {
            'default': {
                'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
            }
        }
    
# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

TEMPLATE_DIRS = (
    BASE_DIR + '/templates/',
    BASE_DIR + '/profiles/templates/',
    BASE_DIR + '/images/templates/',
    BASE_DIR + '/locations/templates/',
    BASE_DIR + '/tags/templates/',
    BASE_DIR + '/homepage/templates/',
    BASE_DIR + '/resources/templates/',
    BASE_DIR + '/angular/templates/',
    BASE_DIR + '/static_pages/templates/'
)

# https only

if SECURE_ONLY:
    SSLIFY_DISABLE = False
    SESSION_COOKIE_SECURE = True
    CSRF_COOKIE_SECURE = True
    SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
else:
    SSLIFY_DISABLE = True
    SESSION_COOKIE_SECURE = False
    CSRF_COOKIE_SECURE = False

SESSION_ENGINE = "django.contrib.sessions.backends.signed_cookies"

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

# Do not include cloudfront here; that's added down below.

TEMPLATE_DNS_PREFETCH = [
    '//ajax.googleapis.com',
    '//cgi.gstatic.com',
    '//fonts.googleapis.com',
    '//fonts.gstatic.com',
    '//maps.googleapis.com',
    '//maps.gstatic.com',
    '//mts0.googleapis.com',
    '//mts1.googleapis.com',
    '//sharedearth-dev.s3.amazonaws.com',   # TODO - fIgure out why this is being queried!
]

if USE_S3:
    # Amazon S3 settings

    # See http://developer.yahoo.com/performance/rules.html#expires
    # Removed due to an encoding bug in django-storages as of 2015/01/27: it double-escape spaces so they end up as "%20" in the "Expires" field.
    # AWS_HEADERS = {  
    #        'Expires': 'Thu, 31 Dec 2099 20:00:00 GMT',
    #        'Cache-Control': 'max-age=94608000',
    # }
    
    AWS_S3_SECURE_URLS = True
    AWS_IS_GZIPPED = True
    GZIP_CONTENT_TYPES = ('text/css', 'application/javascript', 'application/x-javascript')
    
    AWS_STORAGE_BUCKET_NAME = safe_get_env('AWS_STORAGE_BUCKET_NAME')
    AWS_ACCESS_KEY_ID = safe_get_env('AWS_ACCESS_KEY_ID')
    AWS_SECRET_ACCESS_KEY = safe_get_env('AWS_SECRET_ACCESS_KEY')
    AWS_QUERYSTRING_AUTH = False
    
    # Tell django-storages that when coming up with the URL for an item in S3 storage, keep
    # it simple - just use this domain plus the path. (If this isn't set, things get complicated).
    # This controls how the `static` template tag from `staticfiles` gets expanded, if you're using it.
    # We also use it in the next setting.
    # AWS_S3_CUSTOM_DOMAIN = '%s.s3.amazonaws.com' % AWS_STORAGE_BUCKET_NAME
    AWS_S3_CUSTOM_DOMAIN = safe_get_env('AWS_S3_CUSTOM_DOMAIN')
        
    # Tell the staticfiles app to use S3Boto storage when writing the collected static files (when
    # you run `collectstatic`). Store static and media files in separate URLs.
    STATICFILES_LOCATION = 's'
    STATICFILES_STORAGE = 'sharedearth.custom_storages.CachedS3BotoStorage'  # 'sharedearth.custom_storages.StaticStorage'
    STATIC_URL = "http%s://%s/%s/" % (('s' if AWS_S3_SECURE_URLS else ''), AWS_S3_CUSTOM_DOMAIN, STATICFILES_LOCATION)
    
    MEDIAFILES_LOCATION = 'm'
    DEFAULT_FILE_STORAGE = 'sharedearth.custom_storages.MediaStorage'
    MEDIA_URL = "http%s://%s/%s/" % (('s' if AWS_S3_SECURE_URLS else ''), AWS_S3_CUSTOM_DOMAIN, MEDIAFILES_LOCATION)

    COMPRESS_ENABLED = True
    COMPRESS_OFFLINE = True
    COMPRESS_STORAGE = 'sharedearth.custom_storages.CachedS3BotoStorage'
    COMPRESS_URL = STATIC_URL
    # COMPRESS_DATA_URI_MAX_SIZE = 16384  # Since this is served from our CDN anyway, go relatively big here.

    COMPRESS_CSS_FILTERS = [
        # 'compressor.filters.datauri.CssDataUriFilter',  #  This doesn't appear to work as of 2015/04/16
        'compressor.filters.css_default.CssAbsoluteFilter',
        'compressor.filters.cleancss.CleanCSSFilter',
    ]
    CLEAN_CSS_BINARY = 'cleancss'
    CLEAN_CSS_ARGUMENTS = ''

    USE_YUGLIFY = False
    
    if USE_YUGLIFY:
        COMPRESS_JS_FILTERS = ['compressor.filters.yuglify.YUglifyJSFilter']
        COMPRESS_YUGLIFY_BINARY = 'yuglify'
        COMPRESS_YUGLIFY_JS_ARGUMENTS = ''
    else:
        COMPRESS_JS_FILTERS = ['compressor.filters.closure.ClosureCompilerFilter']
        COMPRESS_CLOSURE_COMPILER_BINARY = 'java -jar /applications/closure/compiler.jar'
        COMPRESS_CLOSURE_COMPILER_ARGUMENTS = ''  # May want to try this one: '--compiler_flags="--language_in=ECMASCRIPT5"'

    STATIC_ROOT = BASE_DIR + '/static/'
    
    # End Amazon S3 settings
    
    TEMPLATE_DNS_PREFETCH.insert(0, '//%s' % (AWS_S3_CUSTOM_DOMAIN))
else:
    if False:
        COMPRESS_ENABLED = True
        COMPRESS_JS_FILTERS = ['compressor.filters.closure.ClosureCompilerFilter']
        COMPRESS_CLOSURE_COMPILER_BINARY = 'java -jar /applications/closure/compiler.jar'
        COMPRESS_CLOSURE_COMPILER_ARGUMENTS = '--compiler_flags="--language_in=ECMASCRIPT5"'
    else:
        COMPRESS_ENABLED = False
    
    STATIC_ROOT = BASE_DIR + '/static/'
    STATIC_URL = '/s/'
    MEDIA_URL = '/m/'

UPLOADER_S3_BUCKET = safe_get_env('AWS_STORAGE_BUCKET_NAME')
UPLOADER_AWS_ACCESS_KEY = safe_get_env('AWS_ACCESS_KEY_ID')
UPLOADER_AWS_SECRET_KEY = safe_get_env('AWS_SECRET_ACCESS_KEY')

# <allauth>
SITE_ID = 1

ACCOUNT_AUTHENTICATION_METHOD = 'email'
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_UNIQUE_EMAIL = True
ACCOUNT_USERNAME_REQUIRED = False
ACCOUNT_LOGOUT_ON_GET = True

SOCIALACCOUNT_QUERY_EMAIL = True
SOCIALACCOUNT_AUTO_SIGNUP = True
SOCIALACCOUNT_EMAIL_REQUIRED = True
LOGIN_URL = '/accounts/login/'

LOGIN_REDIRECT_URL = '/'
# </allauth>

RAVEN_CONFIG = {
    'dsn': 'https://2024dfec813e410b84949eeabc0c8c07:c388380e6e24414d91f92bbeb5aa9192@sharedearth-sentry.herokuapp.com/2',
}

EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'debug@ocupop.com'
EMAIL_HOST_PASSWORD = 'Fj6aFCPpYk9%Bd'
EMAIL_PORT = 587

ADMINS = (('Robert Sundling', 'rjs-sharedearth-debug@zensoft.com'))

add_to_builtins('angular.templatetags.collapsewhitespace')

# We are using a free redis server that only allows 10 total connections.
# We need one or two for our Tornado backend
REDIS_MESSAGING_MAX_CONNECTIONS = 3

# Session options. 
# See https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-SESSION_COOKIE_AGE etc.
# SESSION_COOKIE_AGE = 1209600 # Two weeks in seconds 
# SESSION_SAVE_EVERY_REQUEST = True # Send with every request and not just when changed. Probably not needed since we have the two week thing going on.

SESSION_COOKIE_AGE = 31536000
SESSION_SAVE_EVERY_REQUEST = True  # Send with every request and not just when changed

#
# Celery 
#

djcelery.setup_loader()
app.conf.update(
    CELERY_RESULT_BACKEND='djcelery.backends.database:DatabaseBackend',
)

BROKER_URL = safe_get_env('CELERY_REDISCLOUD_URL')
BROKER_POOL_LIMIT = 2
CELERY_RESULT_BACKEND = safe_get_env('CELERY_REDISCLOUD_URL')
CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'

if 'DEBUG_SHAREDEARTH' in os.environ:
    NOTIFICATION_EMAIL_DELAY = 15  # 15 seconds
    NOTIFICATION_EMAIL_MIN_ELAPSED = 60  # One minute
    CONNECTION_NOTIFICATION_EMAIL_DELAY = 5  # 5 seconds
else:
    NOTIFICATION_EMAIL_DELAY = 300  # 5 minutes
    NOTIFICATION_EMAIL_MIN_ELAPSED = 60 * 60  # One hour
    CONNECTION_NOTIFICATION_EMAIL_DELAY = 300  # 5 minutes

# Enables error emails.
CELERY_SEND_TASK_ERROR_EMAILS = True
SERVER_EMAIL = 'debug@ocupop.com'  # Email address used as sender (From field).

# EOF
