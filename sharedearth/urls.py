from django.conf.urls import patterns, include, url
from django.contrib import admin
import homepage.views
from django.views.generic.base import RedirectView
from sharedearth import settings
import static_pages.views
from django.views.generic.base import TemplateView
from messaging.views import jump_to_conversation
from profiles.views import jump_to_connection
from profiles.views import jump_to_marketing


admin.site.site_header = 'Shared Earth administration'
admin.site.site_title = 'Shared Earth administration'

urlpatterns = patterns('',
    # Actual pages
    url(r'^$', homepage.views.homepage, name='homepage'),
    url(r'^homepage$', homepage.views.homepage_force),
    url(r'^local-(?P<city>[^/]+)$', homepage.views.homepage),
    
    url(r'^robots\.txt$', TemplateView.as_view(template_name='robots.txt', content_type='text/plain')),
    url(r'^privacy$', static_pages.views.privacy_policy),
    url(r'^admin/', include(admin.site.urls)),

    # RESTful API 
    url(r'^images/', include('images.urls')),
    url(r'^locations/', include('locations.urls')),
    url(r'^tags/', include('tags.urls')),
    url(r'^messaging/', include('messaging.urls')),
    
    # All other URLs
    url(r'^conversation/(?P<token>[^/]+)/(?P<conversation_id>[0-9]+)', jump_to_conversation),
    url(r'^connection/(?P<token>[^/]+)/', jump_to_connection),
    url(r'^mkt/(?P<token>[^/]+)/', jump_to_marketing),
    url(r'^', include('profiles.urls')),
)

# Redirect all other pages to the homepage
if not settings.DEBUG:
    urlpatterns += patterns('',
        url('^.*/$', RedirectView.as_view(url='/', permanent=False)),
    )
