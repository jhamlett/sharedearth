from django.http.response import HttpResponse, JsonResponse


class HttpResponseNoContent(HttpResponse):
    status_code = 204


class JsonResponseForbidden(JsonResponse):
    status_code = 403

#    def __init__(self, **args):
#        super().__init__(**args)
