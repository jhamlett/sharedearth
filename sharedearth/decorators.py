from django.core.exceptions import PermissionDenied


def ensure_user_authorized(original_function):
    def new_function(request, *args, **kwargs):
        if not request.user.is_authenticated():  # TODO - When fixing authentication, we can change it here
            raise PermissionDenied
        else:
            return original_function(request, *args, **kwargs)
    return new_function
  
# EOF
