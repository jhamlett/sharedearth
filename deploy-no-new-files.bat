call venv\Scripts\activate.bat
set DEBUG_SHAREDEARTH=

call heroku maintenance:on --app sharedearth
call heroku ps:scale --app sharedearth web=0

git add --all .
@if %errorlevel% neq 0 exit /b %errorlevel%

git commit -m "Automatic commit before heroku deployment"
@if %errorlevel% neq 0 exit /b %errorlevel%

git push bitbucket master
@if %errorlevel% neq 0 exit /b %errorlevel%

git push heroku master
@if %errorlevel% neq 0 exit /b %errorlevel%

call heroku run --app sharedearth python manage.py migrate

call setenv.bat

call heroku ps:scale --app sharedearth web=1
echo Waiting fifteen seconds for the server to start, then turning off maintenance mode...
sleep 15
call heroku maintenance:off --app sharedearth
echo Deployment complete