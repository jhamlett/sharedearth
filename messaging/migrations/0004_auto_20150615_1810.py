# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('messaging', '0003_auto_20150401_1711'),
    ]

    operations = [
        migrations.AddField(
            model_name='conversationmember',
            name='autologin_token',
            field=models.CharField(blank=True, default=None, max_length=256, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='conversationmember',
            name='autologin_token_expiration',
            field=models.DateTimeField(default=django.utils.timezone.now),
            preserve_default=True,
        ),
    ]
