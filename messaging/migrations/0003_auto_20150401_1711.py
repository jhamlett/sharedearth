# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('messaging', '0002_auto_20150324_2253'),
    ]

    operations = [
        migrations.AddField(
            model_name='conversationmember',
            name='last_message_notified',
            field=models.ForeignKey(related_name='conversationmember_last_message_notified', blank=True, default=None, to='messaging.Message', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='conversationmember',
            name='last_message_notified_time',
            field=models.DateTimeField(blank=True, default=None, null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='conversationmember',
            name='last_message_read',
            field=models.ForeignKey(related_name='conversationmember_last_message_read', blank=True, default=None, to='messaging.Message', null=True),
            preserve_default=True,
        ),
    ]
