import tornado.websocket
import tornado.ioloop
import tornado.web
import tornadoredis.pubsub
import json
import raven
import os
import urllib
import time
from django.core import signing
from django.conf import settings
from itsdangerous import TimestampSigner
from messaging.apps import redis_pool
import redis
import sys
from tornado.websocket import WebSocketClosedError
import shortuuid


_startup_cwd = os.getcwd()


def random_string(length=30):
    return str(shortuuid.ShortUUID().random(length=length))

heartbeat_callback = None
heartbeat_time = 3000
heartbeat_sent = 0
heartbeat_received = 0
heartbeat_channel = 'heartbeat_channel_' + random_string()


def send_heartbeart():
    global heartbeat_received
    global heartbeat_sent

    if heartbeat_received != heartbeat_sent:
        main_ioloop.stop()
    else:
        heartbeat_sent += 1
        r = redis.Redis(connection_pool=redis_pool)
        r.publish(heartbeat_channel, str(heartbeat_sent))
        # print("Lub")


class HeartbeatReceiver():
    def send_message(self, msg):
        global heartbeat_received
        heartbeat_received = int(msg)
        # print("Dub")

    
class WebsocketSubscriber(tornadoredis.pubsub.BaseSubscriber):
    def on_message(self, msg):
        
        if not msg:
            return
        if msg.kind == 'message' and msg.body:
            # print("Received message from redis: ", msg)
            
            # Get the list of subscribers for this channel. May be more than
            # one if a single user is logged in from multiple computers.
            subscribers = list(self.subscribers[msg.channel].keys())
            if subscribers:
                for s in subscribers:
                    s.send_message(msg.body)
        super(WebsocketSubscriber, self).on_message(msg)


class ChatHandler(tornado.websocket.WebSocketHandler):
    def initialize(self, subscriber, *args, **kwargs):
        self.subscriber = subscriber
        self.unique_id = random_string(8)
        self.user_id = 0  # Used only for debugging
        self.channel_name = None

        # Never use cookie auth anymore        
        if False:
            session_id = self.get_cookie('sessionid', None)
            
            if session_id:
                try:
                    # Find the user ID from their Django session cookie
                    r = signing.loads(session_id, key=settings.SECRET_KEY, salt='django.contrib.sessions.backends.signed_cookies')
                    self.user_id = int(r['_auth_user_id'])
                    self.channel_name = "messages_to_" + str(self.user_id)
                    self.subscriber.subscribe(self.channel_name, self)
                    print(self.unique_id, "User", self.user_id, "Websocket opened")
                except:
                    print(self.unique_id, "Invalid signature - Should probably close this socket, but we can't apparently.")
                    # self.close(1008, "Invalid session signature")
            else:
                print("Websocket opened without authentication cookie")
            
    def on_message(self, message):
        global signer
        print(self.unique_id, "User", self.user_id, "Received message from websocket: ", message)
        try:
            message = json.loads(message)
        except:
            self.close(1008, "Invalid message")
        
        if 'crash' in message:
            try:
                crash = int(signer.unsign(message['crash'], max_age=60))
                if crash == 'crash':
                    raise Exception("Requested crash; let's do it.")    
            except:
                pass
                
        elif 'ping' in message:
            self.send_message(json.dumps({'pong': 'ping'}, separators=(',', ':')))
        elif 'auth' in message:
            try:
                self.user_id = int(signer.unsign(message['auth'], max_age=60))
                self.channel_name = "messages_to_" + str(self.user_id)
                print(self.unique_id, "User", self.user_id, "Websocket authenticated")
                self.subscriber.subscribe(self.channel_name, self)
                print(self.unique_id, "User", self.user_id, "Subscribed to", self.channel_name, "as", self)
            except:
                print(self.unique_id, "Invalid or expired session signature")
                self.user_id = 0
                self.close(1008, "Invalid session signature")
        
    def send_message(self, message):
        try:
            print(self.unique_id, "User", self.user_id, "Sending message out over websocket: ", message)
            self.write_message(message)
        except WebSocketClosedError:
            print(self.unique_id, "User", self.user_id, "Premature socket close; send raised WebSocketClosedError")
            # self.close(1008, "Expired socket")
            pass

    def on_close(self):
        print(self.unique_id, "User", self.user_id, "Websocket closed; unsubscribing as", self)
        if self.channel_name:
            self.subscriber.unsubscribe(self.channel_name, self)    
            print(self.unique_id, "User", self.user_id, "Successful unsubscribe")
            
    def check_origin(self, origin):
        return True


def websocket_server():
    global signer
    global heartbeat_callback
    global heartbeat_channel
    global heartbeat
    global main_ioloop
    client = None
     
    if 'REDISCLOUD_URL' in os.environ:
        url = urllib.parse.urlparse(os.environ.get('REDISCLOUD_URL'))
        client = tornadoredis.Client(host=url.hostname, port=url.port, password=url.password)
    else:
        error = "REDISCLOUD_URL environment variable not set"
        print(error)
        raise Exception(error)
        
    if 'REALTIME_SECRET_KEY' in os.environ:
        signer = TimestampSigner(os.environ['REALTIME_SECRET_KEY'], salt='websockets_auth')        
    else:
        error = "REALTIME_SECRET_KEY environment variable not set"
        print(error)
        raise Exception(error)
                
    subscriber = WebsocketSubscriber(client)
    application = tornado.web.Application([(r'/ws', ChatHandler, {'subscriber': subscriber})])
    application.listen(int(os.environ['REALTIME_BACKEND_PORT']) if 'REALTIME_BACKEND_PORT' in os.environ else 8988)

    heartbeat_receiver = HeartbeatReceiver()
    subscriber.subscribe(heartbeat_channel, heartbeat_receiver)
    
    hearbeat_callback = tornado.ioloop.PeriodicCallback(send_heartbeart, heartbeat_time)
    hearbeat_callback.start()
    
    main_ioloop = tornado.ioloop.IOLoop.instance()
    print("Shared Earth chat server started.")

    main_ioloop.start()


def respawn():
    args = sys.argv[:]
    print('Re-spawning %s' % ' '.join(args))

    args.insert(0, sys.executable)
    if sys.platform == 'win32':
        args = ['"%s"' % arg for arg in args]

    os.chdir(_startup_cwd)
    os.execv(sys.executable, args)
        

def run_server():
    while True:
        sentry_var = 'REALTIME_BACKEND_SENTRY_URL'
        raven_client = raven.Client(os.environ[sentry_var]) if sentry_var in os.environ else None
    
        try:
            websocket_server()
            print("Websocket server terminated")
        except:
            print("Shared Earth chat server restarting due to error.")
            # Log this exception
            if raven_client:
                raven_client.captureException()

        # Restart on error
        time.sleep(1)
        respawn()
    
    
if __name__ == "__main__":
    run_server()
