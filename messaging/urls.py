from django.conf.urls import patterns, url
from messaging import views

urlpatterns = patterns('',
    url(r'^get/messages/', views.json_get_messages),
    url(r'^get/conversations/', views.json_get_conversations),
    url(r'^get/auth_token/', views.json_get_auth_token),
    url(r'^send/', views.json_send),
    url(r'^read/', views.json_read),
)
