function onBeginMessages ( )
{
  $('#tabs-right').addClass('hidden');  
  $('#messages-right').removeClass('hidden');
  $('#messages-bottom textarea').focus();  
}

function onEndMessages ( )
{
  $('#tabs-right').removeClass('hidden');  
  $('#messages-right').addClass('hidden');  
}

function beginMessage ( userID, locationID )
{
  mainScope.beginMessage(userID, locationID);
}

function switchToMessages ( )
{
}

function sendMessage ( )
{
  $('#messages-bottom textarea').val('');
  $('#messages-bottom textarea').focus();
  return false;
}

$(function()
{
  $('#places-tab').click(function(){
    onEndMessages();
  });
  
  $('#messages-bottom textarea').shiftenter();  
});

