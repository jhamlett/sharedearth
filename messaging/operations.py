from messaging.models import ConversationMember, Conversation, Message
from datetime import datetime
from django.contrib.auth.models import User
from messaging.apps import redis_pool
import redis
import json
from profiles.operations import get_display_name
from messaging.tasks import notify_of_new_message


# We only call this if we know a conversation does not already exist
def _start_conversation(user1, user2):
    c = Conversation()
    c.save()
    
    c1 = ConversationMember()
    c1.conversation = c
    c1.user = user1
    c1.save()

    c2 = ConversationMember()
    c2.conversation = c
    c2.user = user2
    c2.save()
    
    return c
    
    
def find_conversation(user1, user2):
    try:
        return ConversationMember.objects.get(user=user1, conversation__conversationmember__user=user2).conversation
    except ConversationMember.DoesNotExist:
        return _start_conversation(user1, user2)


def send_message(user_from, user_to, text):
    
    user1 = User.objects.get(id=user_from)
    user2 = User.objects.get(id=user_to)
    
    message = Message()
    message.sender = user1
    message.conversation = find_conversation(user1, user2)
    message.text = text
    message.save()
    
    m = {
        'sender': user1.id,
        'recipient': user2.id,
        'id': message.id,
        'conversation': message.conversation.id,
        'text': text
    }
    
    r = redis.Redis(connection_pool=redis_pool)

    # In case the user is logged in on other systems
    channel = "messages_to_" + str(user1.id)
    r.publish(channel, json.dumps(m, separators=(',', ':')))

    # And for the other user
    channel = "messages_to_" + str(user2.id)
    r.publish(channel, json.dumps(m, separators=(',', ':')))

    notify_of_new_message(user_from, user_to, message.conversation.id)

    return m


def send_connection(user_from, user_to, connect):
    
    user1 = User.objects.get(id=user_from)
    user2 = User.objects.get(id=user_to)

    c = {
        'sender': {'id': user1.id, 'avatar_url': user1.userprofile.avatar_url, 'name': get_display_name(user2, user1)}, 
        'recipient': {'id': user2.id, 'avatar_url': user2.userprofile.avatar_url, 'name': get_display_name(user1, user2)},
        'connect': connect,
    }
    
    r = redis.Redis(connection_pool=redis_pool)

    # In case the user is logged in on other systems
    channel = "messages_to_" + str(user1.id)
    r.publish(channel, json.dumps(c, separators=(',', ':')))

    # And for the other user
    channel = "messages_to_" + str(user2.id)
    r.publish(channel, json.dumps(c, separators=(',', ':')))

    return c


# Since our 'id' field auto increments, we can easily use it to select just the
# messages we want.
def get_messages(conversation, count, before_message=None):
    messages = Message.objects.filter(conversation=conversation)
    
    if before_message:
        messages = messages.filter(id__lt=before_message.id)

    messages = messages.order_by('-id')[:count]
    messages = reversed(messages)
    return messages
