from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

# Create your models here.


class Conversation(models.Model):
    pass


class Message(models.Model):
    sender = models.ForeignKey(User)
    conversation = models.ForeignKey(Conversation)
    sent = models.DateTimeField(default=timezone.now)           # For group conversations, a user can see messages between their "join_time" and "leave_time"
    text = models.TextField()


class ConversationMember(models.Model):
    conversation = models.ForeignKey(Conversation)

    user = models.ForeignKey(User)
    last_message_read = models.ForeignKey(Message, default=None, blank=True, null=True, related_name="conversationmember_last_message_read")
    last_message_read_time = models.DateTimeField(default=timezone.now)
    
    last_message_notified = models.ForeignKey(Message, default=None, blank=True, null=True, related_name="conversationmember_last_message_notified")
    last_message_notified_time = models.DateTimeField(default=None, blank=True, null=True)

    autologin_token = models.CharField(max_length=256, default=None, blank=True, null=True)
    autologin_token_expiration = models.DateTimeField(default=timezone.now)

    # Since we aren't doing group conversations in Shared Earth 1.0, we do
    # not need to allow leaving a conversation.
    # join_time = models.DateTimeField(default=timezone.now)
    # leave_time = models.DateTimeField(default=None, blank=True, null=True)


'''

Message design notes


For every individual message:

    id
    sender : User
    conversation : Conversation
    sent : date
    message : text


For every conversation member:

    conversation : Conversation
    user : User
    last_message_read : Message
    last_message_read_time : date








'''
