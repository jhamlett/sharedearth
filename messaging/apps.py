from django.apps import AppConfig
from sharedearth import settings
import os
import urllib.parse
import redis


redis_pool = None


class MessagingAppConfig(AppConfig):
    name = 'messaging'
    verbose_name = "Shared Earth Messaging"

    def ready(self):
        global redis_pool
        
        if 'REDISCLOUD_URL' in os.environ:
            url = urllib.parse.urlparse(os.environ.get('REDISCLOUD_URL'))
            print("Connecting to redis at", url.hostname, ":", url.port)
            redis_pool = redis.BlockingConnectionPool(host=url.hostname, port=url.port, password=url.password, max_connections=settings.REDIS_MESSAGING_MAX_CONNECTIONS)
        else:
            print("Missing REDISCLOUD_URL environment variable")
