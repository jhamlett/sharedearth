from sharedearth import celery_app, settings
from messaging.models import ConversationMember, Message
from profiles.operations import get_display_name
from django.contrib.auth.models import User
from django.utils import timezone
from mailer.mailer import send_template_mail
from datetime import timedelta
from profiles.models import UserProfile
import shortuuid


@celery_app.task()
def new_messages_email(user_from_id, user_to_id, conversation_id):
    profile = UserProfile.objects.get(user__id=user_to_id)
    
    if profile.email_on_messages:
        cm = ConversationMember.objects.get(user__id=user_to_id, conversation__id=conversation_id)
        messages = Message.objects.filter(conversation=cm.conversation)
        
        if cm.last_message_notified:
            messages = messages.filter(id__gt=cm.last_message_notified.id)
    
        last_message_id = 0
        message_text = []
        
        for message in messages:
            message_text.append(message.text)
            if message.id > last_message_id:
                last_message_id = message.id 
    
        # Does this user still have unread/un-notified messages?        
        if len(message_text):
            min_duration = timedelta(seconds=settings.NOTIFICATION_EMAIL_MIN_ELAPSED)
            
            if cm.last_message_notified_time is None or timezone.now() - cm.last_message_notified_time > min_duration:
                # Here is where we would send an email
                # print("Messages from", sender_name, ":")
                # for message in message_text:
                #    print(message)
                #    
                # print("---")
    
                # Need a new token
                if cm.autologin_token_expiration is None or cm.autologin_token_expiration < timezone.now():
                    cm.autologin_token = str(shortuuid.uuid())
    
                cm.autologin_token_expiration = timezone.now() + timedelta(days=7)
                cm.save(update_fields=['autologin_token', 'autologin_token_expiration'])
        
                user = User.objects.get(id=user_to_id)
                user_name = get_display_name(user, user)
                sender = User.objects.get(id=user_from_id)
                sender_name = get_display_name(user, sender)
    
                url = 'https://sharedearth.com/conversation/' + cm.autologin_token + '/' + str(conversation_id)
        
                print("Sending email notification of new message from " + sender_name + " to " + user_name + " for " + url)
                send_template_mail('you-have-a-new-message-singular', [{'email': user.email, 'name': user_name}], {
                    'MESSAGESENDER': sender_name, 
                    'MESSAGELINK': url,
                    'SOMENAME': user_name
                })
                
                cm.last_message_notified = Message.objects.get(id=last_message_id)
                cm.last_message_notified_time = timezone.now()
                cm.save(update_fields=['last_message_notified', 'last_message_notified_time'])
                
            else:
                print("Not sending email notification of new messages since one was sent recently")
        else:
            print("Not sending email notification of new messages since they've been read")
    else:
        print("User does not want notifications of new messages")


def notify_of_new_message(user_from_id, user_to_id, conversation_id):
    # task_id = "message_" + str(user_id) + "_" + str(conversation_id)
    
    # Revoke any existing task, to reset the countdown.
    # try:
    #    app.control.revoke(task_id)  # @UndefinedVariable
    # except:
    #    pass
    
    new_messages_email.apply_async(args=[user_from_id, user_to_id, conversation_id], countdown=settings.NOTIFICATION_EMAIL_DELAY)  # settings.NOTIFICATION_EMAIL_DELAY)  # , task_id=task_id)
        
 
# EOF
