from sharedearth.decorators import ensure_user_authorized
from django.http.response import JsonResponse
import json
from messaging.operations import send_message, get_messages, send_connection
from django.views.decorators.http import require_POST
from django.forms.models import model_to_dict
from messaging.models import ConversationMember, Conversation, Message
from django.db.models.query_utils import Q
from sharedearth.custom_responses import HttpResponseNoContent
from django.utils import timezone
from profiles.operations import get_display_name
from itsdangerous import TimestampSigner
from sharedearth import settings
from django.contrib.auth.models import User
from django.contrib.auth import login
from django.shortcuts import redirect


def raw_messages_to_json_ready(raw_messages):
    messages = []
    for raw_message in raw_messages:
        message = model_to_dict(raw_message)
        
        # Clean up the message
        message['sender'] = raw_message.sender.id
        del message['conversation']
        
        messages.append(message)
    return messages


@ensure_user_authorized
@require_POST
def json_send(request):
    params = json.loads(request.body.decode())
    return JsonResponse(send_message(request.user.id, params['to'], params['text']))


@ensure_user_authorized
@require_POST
def json_read(request):
    params = json.loads(request.body.decode())
    message = Message.objects.get(id=params['id'])
    member = ConversationMember.objects.get(user=request.user, conversation=message.conversation)
    member.last_message_read = message
    member.last_message_notified = message
    member.last_message_read_time = timezone.now()
    member.save(update_fields=['last_message_read', 'last_message_read_time', 'last_message_notified'])
    
    return HttpResponseNoContent()


# Retrieve more messages for a conversation
@ensure_user_authorized
def json_get_messages(request):
    params = json.loads(request.body.decode())
    if 'before_message' in params:
        before_message = params['before_message']
    else:
        before_message = None
    
    raw_messages = get_messages(params['conversation'], params['count'], before_message)
    messages = raw_messages_to_json_ready(raw_messages)
    
    return JsonResponse({'messages': messages})


@ensure_user_authorized
def json_get_conversations(request):
    raw_conversations = Conversation.objects.filter(conversationmember__user=request.user)
    
    num_messages = 100  # Just a random default value
    conversations = []
    
    for raw_conversation in raw_conversations:
        raw_messages = get_messages(raw_conversation, num_messages)
        messages = raw_messages_to_json_ready(raw_messages)
        
        # No group conversations simplifies this
        this_member = ConversationMember.objects.get(Q(conversation=raw_conversation), Q(user=request.user))
        other_member = ConversationMember.objects.get(Q(conversation=raw_conversation), ~Q(user=request.user)) 
        user = other_member.user
        
        last_message_read_id = 0 if (this_member.last_message_read is None) else this_member.last_message_read.id
        
        conversation = {
            'name': get_display_name(request.user, user),
            'id': raw_conversation.id,
            'userID': user.id,
            'last_message_read_id': last_message_read_id,
            'avatar_url': user.userprofile.avatar_url,
            'messages': messages,
        }
        
        conversations.append(conversation)

    return JsonResponse({'conversations': conversations})


@ensure_user_authorized
def json_get_auth_token(request):
    userId = request.user.id
    signer = TimestampSigner(settings.WEBSOCKET_SECRET_KEY, salt='websockets_auth')
    authToken = signer.sign(str(userId))
    
    return JsonResponse({'authToken': authToken.decode("utf-8")})


# This is really quite magical
def jump_to_conversation(request, token, conversation_id):
    if len(token):
        cm = ConversationMember.objects.filter(autologin_token=token)
        
        if cm.count():
            cm = cm.first()
            if cm.autologin_token_expiration > timezone.now():
                # We have a valid login token
                user_id = cm.user_id
            
                # Log the user in    
                user = User.objects.get(id=user_id)
                user.backend = 'django.contrib.auth.backends.ModelBackend'
                login(request, user)
                
                # Expire the login token
                cm.autologin_token = None
                cm.autologin_token_expiration = timezone.now()
                cm.save(update_fields=['autologin_token', 'autologin_token_expiration'])            
                
                # Redirect the user to the appropriate page now
                conversation_id = cm.conversation_id
                return redirect('/app/0/messages/' + str(conversation_id) + '/0/')
    
    if request.user.is_authenticated():
        return redirect('/app/0/messages/' + str(conversation_id) + '/0/')
    else:
        return redirect('/app/0/messages/' + str(conversation_id) + '/0/login')
     
        
# EOF
