from django.core.management.base import BaseCommand
from messaging.tornado import zenchat


class Command(BaseCommand):
    help = 'Starts the Shared Earth messaging server as a blocking task'

    def handle(self, *args, **options):
        zenchat.run_server()
