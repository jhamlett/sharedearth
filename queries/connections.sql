SELECT  DISTINCT
 f1.user_id, f1.friend_id, u1.first_name || ' ' || u1.last_name as name1, u2.first_name || ' ' || u2.last_name AS name2, GREATEST(f1.time,f2.time) as confirm_time
FROM
 profiles_friendship f1
INNER JOIN
  profiles_friendship f2
 ON
  f1.friend_id = f2.user_id AND f1.user_id = f2.friend_id
INNER JOIN
  auth_user u1
 ON
  f1.user_id = u1.id
INNER JOIN
  auth_user u2
 ON
  f2.user_id = u2.id
WHERE
 f1.friend_id > 17 AND f1.user_id > 17 AND
 f1.user_id < f1.friend_id
ORDER BY
 confirm_time
 