import json
import _mysql
from dateutil import parser

DJANGO_SETTINGS_MODULE = 'sharedearth.settings'

from django.conf import settings

from django.contrib.auth.models import User



db=_mysql.connect(host="localhost",user="root",db="old_shared_earth")
query = 'SELECT DISTINCT node.uid,node.type,FROM_UNIXTIME(users.created) AS created,FROM_UNIXTIME(users.access) AS access,fbid,users.name,users.pass as password,mail as email,street,additional,city,province,country FROM location as loc INNER JOIN location_instance as li ON loc.lid = li.lid INNER JOIN node ON node.nid=li.nid INNER JOIN users ON users.uid=node.uid LEFT JOIN fboauth_users AS fb ON users.uid=fb.uid WHERE country="us" AND (type = "garden" OR type="gardener")  ORDER BY uid'
db.query(query)


r=db.use_result()
rows = {}


def load_json(fn):
    with open(fn, "r") as f:
        return json.loads(f.read())

while True:
    row = r.fetch_row(how=1)
    
    if len(row) == 0:
        break
        
    row = row[0]
    rows[row['uid']] = row



fbur = load_json("../sharedearth-scripts/users.txt")

fbu = {}
for u in fbur:
    fbu[u['id']] = u

users = {}

for uid in rows:
    data = rows[uid]
    
    user = {}
    
    if data['fbid'] and data['fbid'] in fbu:
        fb = fbu[data['fbid']]
        user['fbid'] = data['fbid']
        user['first_name'] = fb['first_name']
        user['last_name'] = fb['last_name']
        
        user['password'] = 'facebook$' + data['fbid']
        
        if 'email' in fb:
            user['email'] = fb['email']
        else:
            user['email'] = "invalid-" + data['fbid'] + "@facebook.com"
    else:
        user['email'] = data['email'].decode()
        
        n = data['name'].decode().split(' ')
        fn = ''
        
        if len(n) > 1:
            user['first_name'] = ' '.join(n[:-1])
            user['last_name'] = n[-1]
        else:
            user['first_name'] = n[0]
            user['last_name'] = n[0]

        user['password'] = "drupal$" + data['password'].decode()

    if user['email'] in users:
        print("duplicate email", user['email'])
        
    users[user['email']] = user

count = 0

for key in users:
    
    data = users[key]
    
    user = User.objects.create_user(
        username = data["email"],
        email = data["email"],
        first_name = data["first_name"],
        last_name = data["last_name"],
        password = data["password"],
    )
    user.save()        
    
    count += 1
    if count > 3:
        break
    

# Create user accounts from user list
#
# If Facebook:
#  First name
#  Last name
# If name has spaces:
#  Everything before last space, first name
#  After last space, last name
# Otherwise
#  Both first and last name = Username
#


