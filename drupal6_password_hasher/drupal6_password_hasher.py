from django.contrib.auth.hashers import BasePasswordHasher
from django.utils.crypto import get_random_string
from collections import OrderedDict
import hashlib


class Drupal6PasswordHasherCannotEncodeException(Exception):
    pass


class Drupal6PasswordHasher(BasePasswordHasher):
    """
        Authenticate against Drupal 6 passwords.

        The passwords should be prefixed with drupal$ upon importing, such that
        Django recognizes them correctly. Drupal's method does some funny stuff
        (like truncating the hashed password), so you might not want to use this
        hasher for storing new passwords.
    """
    algorithm = "drupal"

    _itoa64 = './0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'

    def _get_settings(self, encoded):
        settings_bin = encoded[:12]
        count_log2 = self._itoa64.index(settings_bin[3])
        count = 1 << count_log2
        salt = settings_bin[4:12]
        return {
            'count': count,
            'salt': salt
        }

    def salt(self):
        return get_random_string(8)

    def encode(self, password, salt):
        raise Drupal6PasswordHasherCannotEncodeException

    def verify(self, password, encoded):
        encoded = encoded.split("$", 1)[1]
        
        m = hashlib.md5()
        m.update(password.encode('utf-8'))
        return encoded.lower() == m.hexdigest().lower()

    def safe_summary(self, encoded):
        encoded = encoded.split("$", 1)[1]
        settings = self._get_settings(encoded)
        return OrderedDict([
            (_('algorithm'), self.algorithm),
            (_('iterations'), settings["count"]),
            (_('salt'), settings["salt"]),
            (_('hash'), encoded[12:]),
        ])

    def must_update(self, encoded):
        return True
