import mandrill
from sharedearth import settings


def send_template_mail(template_name, email_to, context):
    mandrill_client = mandrill.Mandrill(settings.MANDRILL_API_KEY)
    
    message = {
        'to': [],
        'global_merge_vars': []
    }
    for em in email_to:
        message['to'].append({'email': em['email'], 'name': em['name']})
 
    for k, v in context.items():
        message['global_merge_vars'].append(
            {'name': k, 'content': v}
        )

    mandrill_client.messages.send_template(template_name, [], message)
