import re
from django import template
from django.utils.encoding import force_text
from sharedearth.settings import COLLAPSE_WHITESPACE

register = template.Library()

betweenTags = re.compile(r'>\s+<')
repeatedSpaces = re.compile(r'\s+')
trailingSpaces = re.compile(r'\s+\n')


class CollapseWhitespace(template.base.Node):
    def __init__(self, nodelist):
        self.nodelist = nodelist

    def render(self, context):
        if COLLAPSE_WHITESPACE is True:
            return self.collapse_whitespace(self.nodelist.render(context).strip())
        elif COLLAPSE_WHITESPACE == 'blank_lines_only':
            return self.remove_blank_lines_only(self.nodelist.render(context).strip())
        else:
            return self.nodelist.render(context)

    def collapse_whitespace(self, value):
        value = betweenTags.sub('><', force_text(value))
        return repeatedSpaces.sub(' ', value)

    def remove_blank_lines_only(self, value):
        return trailingSpaces.sub('\n', force_text(value))


@register.tag(name='collapsewhitespace')
def collapsewhitespace(parser, token):
    """   
    Remove all whitespace except for one space from content, but remove spaces between tags
    """
    nodelist = parser.parse(('endcollapsewhitespace',))
    parser.delete_first_token()
    return CollapseWhitespace(nodelist)
