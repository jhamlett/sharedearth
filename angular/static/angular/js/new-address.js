
(function(){

  seApp.controller('NewAddressController', 
  ['$scope', '$rootScope', '$log', '$http', 
    function($scope, $rootScope, $log, $http)
  {
    $scope.autocompleteOptions = {
      types: ['geocode'],
      componentRestrictions: {country: 'us'}
    };
    
    $scope.addressElement = document.getElementById('addressField');
  
    $scope.newAddress = {};
    $scope.newAddress.place = null;
    $scope.newAddress.field = '';

    function getAutocompleteComponents ( place )
    {
      var address_components = place.address_components;
      var components={}; 
      jQuery.each(address_components, function(k,v1) {jQuery.each(v1.types, function(k2, v2){components[v2]=v1.long_name;});});
      return components;    
    }

    $scope.newLocation = function ( type )
    {
      if ( $scope.newAddress.place )
      {
        var place = $scope.newAddress.place;
  
        $scope.newAddress.place = null;
        $scope.newAddress.field = '';
        $('#location-expansion').click();
  
        var components = getAutocompleteComponents(place);
      
        // Add it to our user
        delete components.political; // If there
        components.formatted_address = place.formatted_address;
        components.latitude = place.geometry.location.lat();
        components.longitude = place.geometry.location.lng();
        components.type = type;
  
        $http.post('/locations/add/', components).success(function(result) {
          $log.log("Added location");
          $rootScope.$broadcast("updatePlaces", result);
          $rootScope.$broadcast("newLocation");
        });
      } 
    };
    
    var onPlaceChanged = function ()
    {
      var place = $scope.autoComplete.getPlace();
      
      if ( typeof place.address_components == 'undefined' )
      {
        // The user pressed enter in the input 
        // without selecting a result from the list
        // Let's get the list from the Google API so that
        // we can retrieve the details about the first result
        // and use it (just as if the user had actually selected it)
        var autocompleteService = new google.maps.places.AutocompleteService();
  
        var placeOptions = {
          'input': place.name,
          'offset': place.name.length
        };
        
        _.extend(placeOptions, $scope.autocompleteOptions);
  
        autocompleteService.getPlacePredictions(
          placeOptions,
          function listentoresult ( list, status )
          {
            if(list === null || list.length === 0) {
              // There are no suggestions available.
              // The user saw an empty list and hit enter.
              // Dumb user. ;-)
            } else {
              // Here's the first result that the user saw
              // in the list. We can use it and it'll be just
              // as if the user actually selected it
              // themselves. But first we need to get its details
              // to receive the result on the same format as we
              // do in the AutoComplete.
              var placesService = new google.maps.places.PlacesService($scope.addressElement);
              
              placesService.getDetails(
                {'reference': list[0].reference},
                function detailsresult(detailsResult, placesServiceStatus)
                {
                  $scope.newAddress.field = detailsResult.formatted_address;
                  $scope.newAddress.place = detailsResult;
                  $rootScope.safeApply();
                }
              );
            }
          }
        );
      }
      else
      {
        $scope.newAddress.field = place.formatted_address;
        $scope.newAddress.place = place;
        $rootScope.safeApply();
      }
    };
    
    $rootScope.$on("mapReady", function (args)
    {
      $scope.autoComplete = new google.maps.places.Autocomplete($scope.addressElement, $scope.autocompleteOptions);
      google.maps.event.addListener($scope.autoComplete, 'place_changed', onPlaceChanged);
    });

  }]);
})();
