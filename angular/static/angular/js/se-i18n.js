var translations = 
{
  'en' :
  {
    PAGE: {
      TITLE: "Shared Earth"
    },
  
    // Desktop site header
    HEADER: {
      LOG_IN: "Log in"
    },
    
    // Area on the upper left with the user's avatar and name, or a sign-up message if they
    // are not logged in yet.
    UPPER_LEFT: {
      EDIT_PROFILE: "Edit Profile",
      
      CREATE_ACCOUNT: {
        TITLE: "Create Account",
        LINKED_TEXT: "Create an account",
        FOLLOW_TEXT: " on Shared Earth to connect with others and start growing food together."
      }
    },
  
    // Main navigational menu shown on the left hand side of the site
    MAIN_MENU: {
      MAP: "Map",
      LIST: "List",
      MESSAGES: "Messages",
      CONNECTIONS: "Connections",
      MY_PROFILE: "My Profile",
      RESOURCES: "Resources",
      SETTINGS: "Settings",
      SIGN_OUT: "Sign out"
    },
    
    RESOURCES: {
      TITLE: "Resources"
    },

    ONBOARDING: {
      NEEDS_LAND: {
        WELCOME: {
          MESSAGE: "Welcome to Shared Earth! Here are three quick questions to help you find a match:",
          RESPONSIBILITIES_QUESTION: "How would you like to work with the landowner?",
          EXPERIENCE_QUESTION: "How much gardening experience do you have?",
          WHAT_I_GROW_QUESTION: "What are some things you like to grow?",
          BUTTON: "Continue"
        },

        MATCHES: {
          MESSAGE: "Thanks! Take a look around and find your perfect garden!",
          BUTTON: "Get started"
        },
        
        NO_MATCHES: {
          MESSAGE: "We are sorry, but it doesn’t look like there are any gardens in your area. Shared Earth will notify you as soon as one is available. In the meantime, check out our gardening resources or invite your friends to join!",
          BUTTON: "Continue"
        }
      },
      
      HAS_LAND: {
        WELCOME: {
          MESSAGE: "Welcome to Shared Earth! Here are three quick questions to help you find a match:",
          SIZE_QUESTION: "What size is your garden?",
          STATUS_QUESTION: "What condition is your garden in?",
          COMPENSATION_QUESTION: "How will gardeners compensate you for using your garden?",
          COMPENSATION_CHOICES: {
            "0": "I won’t expect anything",
            "1": "They’ll pay a fee",
            "2": "They’ll share what they grow",
            "3": "Some other arrangement"
          },
          BUTTON: "Continue"
        },

        MATCHES: {
          MESSAGE: "Thanks! Take a look around and find someone to share your garden with!",
          BUTTON: "Get started"
        },
        
        NO_MATCHES: {
          MESSAGE: "We are sorry, but it doesn’t look like there is anyone looking for land in your area. Shared Earth will notify you as soon any new gardeners sign up. In the meantime, check out our gardening resources or invite your friends to join!",
          BUTTON: "Continue"
        }
      }
    },
    
    // Search options
    SEARCH_OPTIONS: {
      BUTTON: "Search options",
      DONE_BUTTON: "Search",
      TITLE: "What to show",
      MATCHES: "Matches (all matches to your profile automatically)",
      GARDENS: "Gardens",
      ALL: "Everyone",
      GARDENERS: "Gardeners",
      VIEW_TYPE: "Display results as",
      VIEW_TYPE_MAP: "Map",
      VIEW_TYPE_LIST: "List",
      TOOLS: "Specific tools",
      TOOLS_DESC: 'Find people with any of these tools:'
    },
    
    // Viewing someone's profile
    PROFILE: {
      HEADER: "View Profile",
      
      TYPE:
      {
        HAVE: "Garden",
        NEED: "Looking for a garden"
      },
      
      BUTTONS: {
        MESSAGE: "Message"
      },
      
      TITLES: {
        PHOTOS: "Photos",
        ABOUT_GARDEN: "About this garden",
        ABOUT_USER: "About {{name}}",
        EXPERIENCE: "Experience",
        ADDRESS: "Address", // Only shown if connected
        DISTANCE: "Search radius",
        WHAT_I_LIKE_TO_GROW: "Likes to grow",
        INTERESTS: "Interests",
        TOOLS_I_HAVE: "Tools shared",
        TOOLS_I_NEED: "Tools needed",
        RESPONSIBILITIES: "Who does what",
        SIZE: "Garden Size",
        COMPENSATION: "Compensation",

        MEMBER_SINCE: "Member since",
        LAST_VISITED: "Last visited",
                
        OTHER_GARDENS: "Other gardens", // Used if this IS a garden
        GARDENS: "Gardens", // Used if this is NOT a garden
        
        OTHER_LOOKING: "Looking for gardens", // Used if this is NOT a garden
        LOOKING: "Looking for gardens" // Used if this IS a garden
      }
      
    },
    
    // Edit my profile page
    EDIT_PROFILE: {
      TITLE: "Edit My Profile",
      
      HEADER: {
        HAVE_GARDEN: "You are sharing a garden at {{address}}.",
        NEED_GARDEN: "You are looking for a garden near {{address}}."
      },
      
      GARDEN_NAME: {
        TITLE: "Garden name",
        PLACEHOLDER: "Enter a unique name for your garden"
      },
      
      PHOTOS: {
        TITLE: "Photos",
        TEXT: "Select any photo to set your profile picture.",
        ADD_PHOTO: "Add Photo"
      },
      
      ABOUT_MY_GARDEN: {
        TITLE: "About my garden",
        PLACEHOLDER: "Tell other users about your garden"
      },
      
      MAX_DISTANCE: {
        TITLE: "How far I’m looking",
        TEXT: "How many miles are you willing to travel to your garden?"
      },
      
      TOOLS_I_HAVE: {
        TITLE: "Tools I have",
        TEXT: "Add any tools you are willing to share by typing them in the field below."
      },

      TOOLS_I_NEED: {
        TITLE: "Tools I need",
        TEXT: "Add any tools you are looking to borrow by typing them in the field below."
      },
      
      ABOUT_ME: {
        TITLE: "About me",
        TEXT: "Give a little more info about yourself—your background, your interests and your Shared Earth goals.",
        PLACEHOLDER: "Tell other users about you"
      },
      
      WHAT_I_LIKE_TO_GROW: {
        TITLE: "What I like to grow",
        TEXT: "List the plants and produce you like to grow in the field below."
      },
      
      INTERESTS: {
        TITLE: "What I am interested in"
      },
      
      YEARS_EXPERIENCE: {
        TITLE: "Experience",
        TEXT: "How long have you been gardening?"
      },
      
      GARDEN_SIZE: {
        TITLE: "Garden size"
      },
      
      GARDEN_STATUS: {
        TITLE: "Garden status"
      },
      
      GARDENER_RESPONSIBILITIES: {
        TITLE: "Who does what",
        TEXT: "How would you like to distribute the work in your ideal garden?"
      },
      
      COMPENSATION: {
        TITLE: "Compensation arrangement",
        TEXT: "Please select from the list below."
      }
    },
  
    // Profile options
    PROFILE_OPTIONS: {
      EXPERIENCE: {
        "0": "New to gardening", 
        "1": "1-2 years", 
        "2": "2-5 years", 
        "5": "5-10 years", 
        "10": "10+ years" 
      },
      DISTANCES: {
        "0": "0.5 miles", 
        "1": "1 mile", 
        "2": "2 miles", 
        "5": "5 miles", 
        "10": "10 miles",
        "20": "20 miles",
        "50": "50 miles" 
      },
      GARDEN_SIZE: {
        "0": "< 50 sq ft",
        "1": "50 - 250 sq ft",
        "2": "250 - 500 sq ft",
        "3": "500 - 1000 sq ft",
        "4": "1000 - 1500 sq ft",
        "5": "1 acre",
        "6": "5 acres",
        "7": "10 acres",
        "8": "20 acres",
        "9": "50 acres",
        "10": "100+ acres"
      },
      GARDEN_STATUS: {
        "0": "Ready to plant",
        "1": "Needs some work",
        "2": "Isn’t a garden yet"
      },
      GARDENER_RESPONSIBILITIES: {
        "0": "I’d like to work alone",
        "1": "We could work together if you’d like",
        "2": "I’m going to need some help"
      },
      COMPENSATION: {
        "0": "Free to use",
        "1": "Pay a fee",
        "2": "Share the harvest",
        "3": "Other arrangement"
      }
    },


    // Map page
    MAP: {
      SEARCH: {
        GARDENS: "Gardens",
        GARDENERS: "Gardeners",
        TOOLS: "Tools"
      },
      
      // These are not used during onboarding, but are used later. 
      OWN_LOCATION: {
        GARDEN_TEXT: "This is your garden.",
        GARDENER_TEXT: "This is you.",
        PROFILE_LINK: "Add details to your profile to find more matches."
      },
      
      TITLE: {
        MAP: "Map",
        MATCHES_NEAR: "Matches near {{address}}", // Also used for list view
        GARDENS_NEAR: "Gardens near {{address}}", // Also used for list view
        GARDENERS_NEAR: "Gardeners near {{address}}", // Also used for list view
        ALL_NEAR: "Everyone near {{address}}", // Also used for list view
        TOOLS_NEAR: "Tools near {{address}}", // Also used for list view
        SELECT_ADDRESS: "Add/Edit Location",
        SEARCH_OPTIONS: "Search options"
      },
      
      TOOL_SEARCH: {
        TEXT: "Find people with these tools:"
      }
    },
    
    // My locations list
    MY_LOCATIONS: {
      BUTTON: "Add/Edit Location",
      CHOOSE_A_PLACE: {
        TITLE: "Choose an existing address"
      },
      ADD_A_PLACE: {
        TITLE: {
          HAS_NO_PLACES: "Add an address",
          HAS_PLACES: "Add a new address"
        },
        ADDRESS_PLACEHOLDER: "Address",
        BUTTONS: {
          NEED_LAND: "I Need Land<br>or Tools<br>to Garden",
          HAVE_LAND: "I Have Land<br>or Tools<br>to Share"
        }
      }
    },
    
    // Match list
    MATCH_LIST: {
      TITLE: "List",
      VIEW_PROFILE: "View Profile",
      MESSAGE: "Message",
      GARDEN: "Garden",
      GARDENER: "Gardener",
      NO_MATCHES: "There were no matches found in your area."
    },
    
    // Messaging page
    MESSAGES: {
      TITLE: "Messages",
      
      NO_MESSAGES: "You have no messages.",
      
      DESKTOP: {
        CONVERSATION_HEADER: "Conversation with {{name}}",  
        PLACEHOLDER_MESSAGE: "Enter a message to send to {{name}}",
        CONNECT_BUTTON: "Connect",
        VIEW_PROFILE_BUTTON: "View Profile",
        REPORT_BUTTON: "Report abuse",
        SEND_BUTTON: "Send"
      },

      // The mobile versions of these are more concise to conserve space
      MOBILE: {
        CONVERSATION_HEADER: "{{name}}",
        PLACEHOLDER_MESSAGE: "Enter a message",
        CONNECT_BUTTON: "Connect",
        VIEW_PROFILE_BUTTON: "Profile",
        REPORT_BUTTON: "Report",
        SEND_BUTTON: "Send"
      },
      
      CONNECT: {
        REQUEST_SENT: "A connection request has been sent to {{name}}.",
        BUTTON: "OK"
      }
    },
    
    // Connections page
    CONNECTIONS: {
      TITLE: "Connections",
      
      VIEW_PROFILE: "View profile",
      MESSAGE: "Message",
      DELETE: "Remove",
      CANCEL: "Cancel",
      CONFIRM_BUTTON: "Confirm",
      DENY_BUTTON: "Deny",
      
      PENDING_TITLE: "Pending connections",
      PENDING_EXPLANATION: "Confirm connections when land is being shared.",
      SENT_TITLE: "Sent connections",
      CONFIRMED_TITLE: "Confirmed connections",
      
      NO_PENDING_CONNECTIONS: "You have no pending connections.",
      NO_CONNECTIONS: "You have no connections.",
      
      CONFIRM: {
        HEADER: "{{name}} would like to connect with you",
        MESSAGE: "Would you like to make this connection?",
        BUTTONS: {
          YES: "Yes",
          NO: "No"
        }
      }
    },

    // Settings page
    SETTINGS: {
      TITLE: "Site Settings",
  
      MOBILE: "Use mobile site",
      FIRST_NAME: "First Name",
      LAST_NAME: "Last Name",
      EMAIL: "Email Address",

      PASSWORD:
      {
        TITLE: "Password",
        CHANGE_PASSWORD: "Change password"
      },
        
      PRIVACY:
      {
        TITLE: "Privacy Settings",
        SHARE_FULL_NAME_ALL: "Share my full name with all other users",
        SHARE_FULL_NAME_CONNECTIONS: "Share my full name with users I’ve connected with",
        SHARE_ADDRESS_CONNECTIONS: "Share my addresses with users I’ve connected with"
      },
      
      NOTIFICATIONS:
      {
        TITLE: "Notification Settings",
        HEADER: "Send me an email message when:",
        ON_MESSAGE: "I receive a message",
        ON_CONNECT: "Someone wants to connect",
        ON_MATCH: "A new match is found",
        ON_RESOURCE: "A new resource is available",
        ON_ANNOUNCEMENT: "There is an important Shared Earth announcement"
      },
      
      SIGN_OUT:
      {
        TITLE: "Sign out",
        HEADER: "If you’re using a shared computer, sign out to prevent others from accessing your Shared Earth account.",
        BUTTON: "Sign out"
      }
    },
    
    // Modal dialogs
    MODAL: {
      // Deleting an active location
      DELETE: {
        TEXT: "You are attempting to delete<br>{{name}}",
        QUESTION: "Are you sure?",
        BUTTONS: {
          NO: "No, I changed my mind.",
          YES: "Yes, delete this entry."
        }
      },
      
      // Logging in to an existing account
      LOG_IN: {
        TITLE: "Log in to Shared Earth",

        FACEBOOK: "Log in with Facebook",
        
        OR: "or", // [log in with facebook] or [fill out these fields]
        
        // There are no form headers, only placeholder text, so these are important.
        PLACEHOLDERS: { 
          EMAIL: "Email Address",
          PASSWORD: "Password"
        },
        
        ERRORS: {
          NONE: "", // Used before a new log in attempt
          EMPTY: "Please enter your email address and password.", // User enters no email and no password then tries to click "log in"
          USER_NOT_FOUND: "You have not yet created an account on Shared Earth.",
          EMAIL_NOT_FOUND: "This email address was not found. Please try again.",
          INCORRECT_PASSWORD: "We found your account, but this password seems to be incorrect. Please try again.",
          GENERIC: "There was an error logging you in."  
        },
        
        BUTTONS: {
          LOG_IN: "Log In",
          FORGOT_PASSWORD: "I forgot my password",
          SIGN_UP: "I need to create an account.",
          CANCEL: "Cancel"
        }
      },

      // Logging in to an existing account
      CHANGE_PASSWORD: {
        TITLE: "Change your password",
        INSTRUCTIONS: "Please enter a new password.",  
        DONE: "Your password has been successfully changed.",
        
        // There are no form headers, only placeholder text, so these are important.
        PLACEHOLDERS: { 
          PASSWORD: "New Password"
        },
        
        ERRORS: {
          NONE: "", // Used before an attempt
          EMPTY: "You must enter a password",
          GENERIC: "There was an error changing your password.",
          EXPIRED: "The password reset link you used has expired."
        },
        
        BUTTONS: {
          CHANGE_PASSWORD: "Change Password",
          DONE: "OK",
          CANCEL: "Cancel"
        }      
      },


      // Logging in to an existing account
      LOST_PASSWORD: {
        TITLE: "Retrieve your lost password",
        SUCCESS: "Thank you. We've emailed a reset password link to you. Please check your email.",  
        
        // There are no form headers, only placeholder text, so these are important.
        PLACEHOLDERS: { 
          EMAIL: "Email Address"
        },
        
        ERRORS: {
          NONE: "Enter your email address below and we will send you a password reset link.", // Used before a new log in attempt
          EMAIL_NOT_FOUND: "This email address was not found. Please try again.",
          FACEBOOK: "This account was registered with Facebook and has no separate password. Please log in using the “Log in with Facebook” button.",
          GENERIC: "Please enter a valid email address."
        },
        
        BUTTONS: {
          OK: "Send password reset link",
          LOG_IN: "Return to “Log in” page",
          CANCEL: "Take me back to the homepage."
        }      
      },
      
      // Creating a new account
      SIGN_UP: {
        TITLE: "Create an account",

        FACEBOOK: "Sign up with Facebook",
        
        OR: "or", // [sign up with facebook] or [fill out these fields]
        
        ERRORS: {
          NONE: "You need to create an account to do more with this site. It only takes a moment.", // Before signup attempt
          MISSING: "Please complete all required fields.",
          INCORRECT_PASSWORD: "An account already exists with that email address, but the password is different.",          
          GENERIC: "There was an error creating your account."
        },

        // There are no form headers, only placeholder text, so these are important.
        PLACEHOLDERS: { 
          FIRST_NAME: "First Name",
          LAST_NAME: "Last Name",
          EMAIL: "Email Address",
          PASSWORD: "Password"
        },
        
        BUTTONS: {
          SIGN_UP: "Sign Up",
          LOG_IN: "I already have an account.",
          CANCEL: "Take me back to the homepage."
        }        
      }
    },
    
    // Note: The "photo" text below is not used as of May 9, 2015 since the photo button template
    // isn't (yet) Angular-based; the text is hard-coded in the template instead. It's a To-Do item. 
    PHOTO:
    {
      SET_PROFILE: "Set as Profile Picture",
      SET_PLACE: "Set as Place Picture",
      DELETE: "Delete" 
    }
  }
};

var defaultLanguage = 'en';
