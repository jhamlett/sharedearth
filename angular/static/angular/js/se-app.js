/*jslint browser: true */
/*global $, _, jQuery, angular, google, translations, defaultLanguage, userID:true, websocketURI, storeTemplateInData, FB, detectedMobile, dynamicGet, makeTinyMap, toRadians, UpdateClusters, lowerBound, deactivateCluster */
/*jshint -W061 */

var seApp = angular.module("seApp", [
  "uiGmapgoogle-maps",
  "ngRoute",
  "ngRaven",
  "luegg.directives",
  "pascalprecht.translate",
  "ngSanitize",
  "ngTouch"
]);
var rootScope = null;
var mainScope = null;

function AllowZoom(flag) {
  if (flag) {
    $("head meta[name=viewport]").remove();
    $("head").prepend(
      '<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=10, minimum-scale=1, user-scalable=1" />'
    );
  } else {
    $("head meta[name=viewport]").remove();
    $("head").prepend(
      '<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=0" />'
    );
  }
}

function RemoveZoomHeader() {
  $("head meta[name=viewport]").remove();
}

function requireAccount() {
  rootScope.safeApply(function() {
    mainScope.signUp();
  });

  return false;
}

function toInt(string, defaultValue) {
  var i = parseInt(string);
  return isNaN(i) ? defaultValue : i;
}

(function() {
  var mapLowHighlightZ = 100;
  var mapIconZ = 101;
  var mapHighlightZ = 102;
  var mapSelectedZ = 103;

  var locations = {};
  var selNormalOpacity = 0.6;
  var selSelectOpacity = 1.0;

  function injectMarkerOptions(loc) {
    loc.topIcon = {
      url: loc.map_icon_url,
      size: new google.maps.Size(96, 96),
      origin: new google.maps.Point(0, 0),
      scaledSize: new google.maps.Size(48, 48),
      anchor: new google.maps.Point(24, 24)
    };

    loc.bottomIcon = {
      url: loc.map_sel_icon_url,
      size: new google.maps.Size(120, 120),
      origin: new google.maps.Point(0, 0),
      scaledSize: new google.maps.Size(60, 60),
      anchor: new google.maps.Point(30, 30)
    };

    loc.topMarkerOptions = {
      zIndex: mapIconZ
    };

    loc.bottomMarkerOptions = {
      zIndex: mapLowHighlightZ,
      opacity: selNormalOpacity
    };

    loc.activeTopMarkerOptions = {
      zIndex: mapSelectedZ,
      anchorPoint: new google.maps.Point(0, -8)
    };

    loc.activeBottomMarkerOptions = {
      zIndex: mapHighlightZ,
      opacity: selSelectOpacity
    };

    return loc;
  }

  var activeLanguage;
  var activeTranslations;

  seApp.config([
    "$provide",
    "$routeProvider",
    "$locationProvider",
    "$httpProvider",
    "uiGmapGoogleMapApiProvider",
    "$ravenProvider",
    "$translateProvider",
    function(
      $provide,
      $routeProvider,
      $locationProvider,
      $httpProvider,
      uiGmapGoogleMapApiProvider,
      $ravenProvider,
      $translateProvider
    ) {
      /*$provide.decorator('translateDirective', function($delegate) {
          var directive = $delegate[0];
          directive.terminal = true;

          return $delegate;
      });*/

      uiGmapGoogleMapApiProvider.configure({
        key: "AIzaSyBF1n-ZnvOzewmkqUTbHEMrxPmdU0mVI7Y",
        v: "3.18",
        libraries: "places"
      });

      // Install all translations.
      for (var key in translations) {
        $translateProvider.translations(key, translations[key]);
      }

      // Set the default language and install it
      activeLanguage = defaultLanguage;
      $translateProvider.preferredLanguage(activeLanguage);
      activeTranslations = translations[activeLanguage];

      // Escaping does not appear to actually work, as HTML code can be embedded in the translations. Oh-well.
      $translateProvider.useSanitizeValueStrategy("escaped");

      // "true" means log errors rather than sending to Sentry
      $ravenProvider.development(false);

      $httpProvider.defaults.xsrfCookieName = "csrftoken";
      $httpProvider.defaults.xsrfHeaderName = "X-CSRFToken";

      $locationProvider.html5Mode(true);

      $routeProvider
        .when("/recover/:recoverToken?", {
          action: "recoverUrl"
        })
        .when("/app/:placeid/:tab1/:tab1opts/:profileid/:modal?", {
          action: "desktopUrl"
        })
        .otherwise({
          redirectTo: "/app/-/map/-/0/"
        });
    }
  ]);

  seApp.filter("inactivePlaces", function() {
    return function(input, activePlaceId) {
      if (activePlaceId in input) {
        var filtered = _.clone(input);
        delete filtered[activePlaceId];
        return filtered;
      }

      return input;
    };
  });

  seApp.filter("urlEncode", [
    function() {
      return window.encodeURIComponent;
    }
  ]);

  seApp.filter("activePlace", function() {
    return function(input, activePlaceId) {
      if (activePlaceId in input) {
        return { activePlaceId: input[activePlaceId] };
      }

      return {};
    };
  });

  function camelCase(name) {
    var SPECIAL_CHARS_REGEXP = /([\:\-\_]+(.))/g;
    var MOZ_HACK_REGEXP = /^moz([A-Z])/;
    return name
      .replace(SPECIAL_CHARS_REGEXP, function(_, separator, letter, offset) {
        return offset ? letter.toUpperCase() : letter;
      })
      .replace(MOZ_HACK_REGEXP, "Moz$1");
  }

  // Allow SVG height and width to be delayed
  angular.forEach(["x", "y", "width", "height"], function(name) {
    var ngName = "ng" + name[0].toUpperCase() + name.slice(1);
    seApp.directive(ngName, function() {
      return function(scope, element, attrs) {
        attrs.$observe(ngName, function(value) {
          attrs.$set(name, value);
        });
      };
    });
  });

  function SimpleTemplate(tag, replace, transclude) {
    seApp.directive(camelCase(tag), function() {
      return {
        restrict: "E",
        transclude: transclude,
        replace: replace,
        templateUrl: "/" + tag + ".html"
      };
    });
  }

  // An experiment
  SimpleTemplate("se-settings", false, false);
  SimpleTemplate("se-conversations", true, false);
  SimpleTemplate("se-map", false, false);
  SimpleTemplate("se-mobile", true, false);
  SimpleTemplate("se-mobile-subhead", true, false);
  SimpleTemplate("se-mobile-messages", true, false);
  SimpleTemplate("se-mobile-conversations", true, false);
  SimpleTemplate("se-resources", true, false);
  SimpleTemplate("se-header", true, false);
  SimpleTemplate("se-modal", false, true);
  SimpleTemplate("se-my-places-list", true, false);
  SimpleTemplate("se-onboarding", false, false);
  SimpleTemplate("se-desktop-messaging", true, false);
  SimpleTemplate("se-address-choice", false, false);
  SimpleTemplate("se-search-options", true, false);

  seApp.directive("seXbutton", function() {
    return {
      template:
        '<div class="xbutton" style="width:{{::size}}; height:{{::size}}; font-size:{{::size}}"><div class="left" style="background-color:{{::color}};"></div><div class="right" style="background-color:{{::color}};"></div></div>',
      restrict: "E",
      replace: true,
      scope: {
        size: "@",
        color: "@"
      }
    };
  });

  seApp.directive("seIcon", function() {
    return {
      restrict: "E",
      replace: true,
      scope: {
        iconName: "@iconName",
        iconSize: "@iconSize"
      },
      templateUrl: "/se-icon.html"
    };
  });

  seApp.directive("seMatchesList", function() {
    return {
      restrict: "E",
      templateUrl: "/se-matches-list.html"
    };
  });

  seApp.directive("seConnections", function() {
    return {
      restrict: "E",
      templateUrl: "/se-connections.html"
    };
  });

  seApp.directive("seEditProfile", function() {
    return {
      restrict: "E",
      replace: true,
      templateUrl: "/se-edit-profile.html"
    };
  });

  seApp.directive("seMain", function() {
    return {
      restrict: "E",
      replace: true,
      templateUrl: "/se-main.html"
    };
  });

  seApp.directive("seFancybox", function() {
    return function(scope, element, attrs) {
      var what = element;
      what.fancybox({
        transitionIn: "elastic",
        transitionOut: "elastic",
        titlePosition: "inside",
        easingIn: "easeOutBack",
        easingOut: "easeInBack",
        hideOnContentClick: true,
        hideOnOverlayClick: true,
        overlayColor: "#000000",
        overlayOpacity: 0.85,
        overlayShow: true
      });

      if ("seEditable" in attrs) {
        what.each(function() {
          storeTemplateInData("photo-menu", $(this), "controls", {
            id: scope.image.id,
            location_id: scope.activePlaceId
          });
        });
      }
    };
  });

  seApp.directive("seTags", function() {
    return {
      restrict: "E",
      replace: true,
      templateUrl: "/se-tags.html",
      link: function(scope, element, attrs) {
        var dataSource = eval(attrs.seTagSource);
        var fieldName = attrs.seField;
        var init = false;

        element.tokenInput(dataSource, {
          theme: "sharedearth",
          hintText: "",
          preventDuplicates: false, // This is broken; inserts next item in a weird place
          propertyToUse: "v",
          onAdd: function(hidden_input, item) {
            if (!init && scope.activePlaceId) {
              var tagText = hidden_input.v;

              if (attrs.seTagType == "web") {
                rootScope.safeApply(function() {
                  // This is for search fields, etc.
                  mainScope[fieldName] = _.without(
                    mainScope[fieldName],
                    tagText
                  );
                  mainScope[fieldName].push(tagText);
                });
                return;
              } else if (attrs.seTagType == "location") {
                scope.activePlace()[fieldName] = _.without(
                  scope.activePlace()[fieldName],
                  tagText
                );
                scope.activePlace()[fieldName].push(tagText);
              } else if (attrs.seTagType == "user") {
                scope.user[fieldName] = _.without(
                  scope.user[fieldName],
                  tagText
                );
                scope.user[fieldName].push(tagText);
              }

              $.post(
                "/tags/add/",
                {
                  field: fieldName,
                  location: scope.activePlaceId,
                  tag: tagText
                },
                function(data) {
                  // TODO: Detect failed tags here and kill them.
                }
              );
            }
          },
          onDelete: function(hidden_input, item) {
            if (!init && scope.activePlaceId) {
              var tagText = hidden_input.v;

              if (attrs.seTagType == "web") {
                // This is for search fields, etc.
                rootScope.safeApply(function() {
                  mainScope[fieldName] = _.without(
                    mainScope[fieldName],
                    tagText
                  );
                });
                return;
              } else if (attrs.seTagType == "location") {
                scope.activePlace()[fieldName] = _.without(
                  scope.activePlace()[fieldName],
                  tagText
                );
              } else if (attrs.seTagType == "user") {
                scope.user[fieldName] = _.without(
                  scope.user[fieldName],
                  tagText
                );
              }

              $.post(
                "/tags/delete/",
                {
                  field: fieldName,
                  location: scope.activePlaceId,
                  tag: tagText
                },
                function(data) {
                  // TODO: Detect failed tags here and ... not delete them.
                }
              );
            }
          },
          onReady: function() {
            var onChange = function(newValue, oldValue) {
              if (oldValue != newValue) {
                var tags;

                if (attrs.seTagType == "web") {
                  // This is for search fields, etc.
                  tags = mainScope[fieldName];
                } else if (attrs.seTagType == "location") {
                  tags = scope.activePlace()[fieldName];
                } else if (attrs.seTagType == "user") {
                  tags = scope.user[fieldName];
                }

                if (tags !== undefined) {
                  init = true;
                  element.tokenInput("clear");
                  for (var i = 0; i < tags.length; ++i) {
                    element.tokenInput("add", { v: tags[i], id: tags[i] });
                  }
                  init = false;
                }

                $("input").blur();
                $(".scroller").scrollTop(0);
                $(".scroll").scrollTop(0);
              }
            };

            scope.$watch("activePlaceChangeCount", onChange);
          }
        });
      }
    };
  });

  seApp.controller("SearchOptionsController", [
    "$scope",
    "$rootScope",
    "$log",
    "$http",
    "uiGmapGoogleMapApi",
    "$route",
    "$location",
    function(
      $scope,
      $rootScope,
      $log,
      $http,
      uiGmapGoogleMapApi,
      $route,
      $location
    ) {
      $scope.mainScope = mainScope; // Nasty hack
    }
  ]);

  seApp.controller("SharedEarthController", [
    "$scope",
    "$rootScope",
    "$log",
    "$http",
    "uiGmapGoogleMapApi",
    "$route",
    "$location",
    "$interval",
    "$window",
    function(
      $scope,
      $rootScope,
      $log,
      $http,
      uiGmapGoogleMapApi,
      $route,
      $location,
      $interval,
      $window
    ) {
      // To save time during initial development, $scope got treated as if it's the model. Obviously, that's not so good.
      // Eventually, everything that is in $scope could be moved inside of $scope.model, that could be made a separate
      // object, and then we could cleanly cut over.
      // See: https://www.youtube.com/watch?v=ZhfUv0spHCY&feature=youtu.be&t=30m

      // Store the root scope to provide access to it to external functions.
      rootScope = $rootScope;
      mainScope = $scope;
      $scope.scope = $scope;
      $scope.model = {};
      $scope.userID = userID;
      $scope.inlineModal = false;
      $scope.searchOptions = false;

      $scope.activeLanguage = activeLanguage;
      $scope.activeTranslations = activeTranslations;
      $window.document.title = $scope.activeTranslations.PAGE.TITLE;

      $scope.updateClusters = false;
      var mapInstance = null;
      $scope.mobile = false;
      $scope.mobileTab = "";
      $scope.toolSearchTags = [];

      $rootScope.safeApply = function(fn) {
        var phase = this.$root.$$phase;
        if (phase == "$apply" || phase == "$digest") {
          if (fn && typeof fn === "function") {
            fn();
          }
        } else {
          this.$apply(fn);
        }
      };

      $scope.mainMenu = [
        { tab: "profile", icon: "profile", text: "MAIN_MENU.MY_PROFILE" },
        { tab: "map", icon: "map", text: "MAIN_MENU.MAP" },
        { tab: "list", icon: "list", text: "MAIN_MENU.LIST" },
        { tab: "messages", icon: "messages", text: "MAIN_MENU.MESSAGES" },
        {
          tab: "connections",
          icon: "connections",
          text: "MAIN_MENU.CONNECTIONS"
        },
        { tab: "resources", icon: "resources", text: "MAIN_MENU.RESOURCES" },
        { tab: "settings", icon: "settings", text: "MAIN_MENU.SETTINGS" }
      ];

      //
      // Facebook
      //

      $scope.facebook = {
        appID: "621155931309148",
        response: {}
      };

      $scope.onFacebook = function() {
        // Check if user is logged in to Facebook, but not here.
        if ($scope.userID === 0) {
          FB.getLoginStatus(function(response) {
            if (response.status === "connected") {
              // TODO: Log the user in automatically
              $log.log("User was already logged in via Facebook:");
              $log.log(response);
            }
          });
        }
      };

      $scope.facebookLoginClicked = function() {
        var loginParameters = {
          client_id: $scope.facebook.appID,
          scope: "public_profile,email"
        };

        FB.login(function(response) {
          $scope.$apply(function() {
            if (response.status == "connected") {
              $http
                .post("/login/", { facebook: true })
                .success(function(data) {
                  // Redirect to a logged-in page
                  // window.location.replace("/app/");
                  userID = data.userID;
                  $scope.userID = userID;
                  $scope.onUserChange();
                  $scope.hideModal();
                })
                .error(function(data, status, header, config) {
                  if ("message" in data) {
                    $scope.modal.login.message =
                      $scope.activeTranslations.MODAL.LOG_IN.ERRORS[
                        data.message
                      ];
                  } else {
                    $scope.modal.login.message =
                      $scope.activeTranslations.MODAL.LOG_IN.ERRORS.GENERIC;
                  }
                });
            }
          });
        }, loginParameters);
      };

      $scope.facebookSignupClicked = function() {
        fbq("track", "CompleteRegistration");

        var loginParameters = {
          client_id: $scope.facebook.appID,
          scope: "public_profile,email"
        };

        FB.login(function(response) {
          $scope.$apply(function() {
            if (response.status == "connected") {
              $http
                .post("/signup/", { facebook: true })
                .success(function(data) {
                  // Redirect to a logged-in page
                  // window.location.replace("/app/");
                  userID = data.userID;
                  $scope.userID = userID;
                  $scope.onUserChange();
                  $scope.hideModal();
                })
                .error(function(data, status, header, config) {
                  if ("message" in data) {
                    $scope.modal.login.message =
                      $scope.activeTranslations.MODAL.LOG_IN.ERRORS[
                        data.message
                      ];
                  } else {
                    $scope.modal.login.message =
                      $scope.activeTranslations.MODAL.LOG_IN.ERRORS.GENERIC;
                  }
                });
            }
          });
        }, loginParameters);
      };

      $window.fbAsyncInit = function() {
        FB.init({
          appId: $scope.facebook.appID,
          status: true,
          cookie: true,
          xfbml: false,
          version: "v2.3"
        });

        rootScope.safeApply(function() {
          $scope.onFacebook();
        });
      };

      (function(d) {
        // load the Facebook javascript SDK

        var js,
          id = "facebook-jssdk",
          ref = d.getElementsByTagName("script")[0];

        if (d.getElementById(id)) {
          return;
        }

        js = d.createElement("script");
        js.id = id;
        js.async = true;
        js.src = "https://connect.facebook.net/en_US/sdk.js";

        ref.parentNode.insertBefore(js, ref);
      })(document);

      //
      // Desktop
      //

      $scope.getDesktopTitle = function() {
        // No place selected
        if (typeof $scope.activePlace() === "undefined") return "";

        var st = $scope.search.searchType;

        if (st == "match") {
          switch ($scope.activePlace().type) {
            case "have":
              st = "need";
              break;
            case "need":
              st = "have";
              break;
          }
        }

        if (st == "tools" && $scope.toolSearchTags.length === 0) {
          st = "all";
        }

        if (st == "have") return "MAP.TITLE.GARDENS_NEAR";
        if (st == "need") return "MAP.TITLE.GARDENERS_NEAR";
        if (st == "all") return "MAP.TITLE.ALL_NEAR";
        if (st == "tools") return "MAP.TITLE.TOOLS_NEAR";
      };

      $scope.getDesktopAddress = function() {
        var ap = $scope.activePlace();

        if (!ap) return "";

        if (ap.name) return ap.name;

        if (ap.address1) return ap.address1 + ", " + ap.address2;

        return ap.address2;
      };

      // iOS 8.4 displays a status bar OVER the bottom of the webpage. We need to keep that area clear.
      $scope.ios84 = navigator.userAgent.indexOf("iPhone OS 8_4") > -1;

      //
      // Mobile
      //

      if ($.cookie("mobile")) {
        // Force to true/false
        $scope.mobile = $.cookie("mobile") === "true" ? true : false;
      } else {
        $scope.mobile = detectedMobile;
      }

      $scope.override = {};
      $scope.override.mobile = $scope.mobile;

      // No need to set this unless the user wants to override
      $.cookie("mobile", $scope.mobile, {
        expires: 365,
        path: "/",
        secure: "https:" == document.location.protocol
      });

      // We never allow zoom
      //AllowZoom(false);

      if ($scope.mobile) {
        AllowZoom(false);
      }

      $scope.onMobileChange = function() {
        $.cookie("mobile", $scope.override.mobile, {
          expires: 365,
          path: "/",
          secure: "https:" == document.location.protocol
        });
        $scope.mobile = $scope.override.mobile;

        if ($scope.mobile) {
          AllowZoom(false);
        } else {
          RemoveZoomHeader();
        }
      };

      $scope.getMobileLeftIconClass = function() {
        switch ($scope.mainTab) {
          case "map":
          case "list":
            if ($scope.showProfile) return "left";
            break;

          case "messages":
            if (
              $scope.mobileBottom != "conversations" &&
              $scope.currentConversation
            )
              return "left";
            break;
        }

        return "invisible";
      };

      $scope.getMobileRightIconClass = function() {
        switch ($scope.mainTab) {
          case "map":
          case "list":
          case "profile":
            if (
              $scope.searchOptions ||
              $scope.showProfile ||
              $scope.chooseAddress
            )
              return "x";
            else return "invisible";

            break;
          case "messages":
            if (
              $scope.mobileBottom != "conversations" &&
              $scope.currentConversation
            )
              return "x";
            break;
        }

        return "invisible";
      };

      $scope.onMobileHeaderClick = function() {
        switch ($scope.mainTab) {
          case "map":
          case "list":
          case "profile":
            if ($scope.searchOptions) {
              $scope.searchOptions = false;
              return;
            }

            if ($scope.showProfile) {
              $scope.showProfile = 0;
              if (!$scope.changingRoute) {
                $location.path(buildUrl());
              }
              return;
            }

            if ($scope.chooseAddress) {
              $scope.chooseAddress = false;
            }

            break;
          case "messages":
            $scope.mobileBottom = "conversations";
            $scope.currentConversationID = -1;

            if (!$scope.changingRoute) {
              $location.path(buildUrl());
            }

            break;
        }

        return "invisible";
      };

      $scope.getMobileTabTitle = function() {
        switch ($scope.mainTab) {
          case "map":
          case "list":
          case "profile":
            if ($scope.searchOptions) return "Search Options";

            if ($scope.showProfile) return "Viewing Profile";

            if ($scope.activePlace()) {
              if ($scope.activePlace().name) return $scope.activePlace().name;

              if ($scope.activePlace().address1)
                return $scope.activePlace().address1;

              return $scope.activePlace().address2;
            }

            return "";

          case "messages":
            if ($scope.mobileBottom == "conversations") {
              return "Messages";
            } else if ($scope.currentConversation) {
              return $scope.currentConversation.name;
            }

            return "Messages";

          case "resources":
            return "Resources";

          case "settings":
            return "Settings";

          case "connections":
            return "Connections";
        }

        // This shouldn't happen...
        return "";
      };

      $scope.setMobileTab = function(newTab) {
        // There are five things to set:
        // 1. The active tab on top
        // 2. The active pane on bottom
        // 3. The title for the title bar
        // 4. The icon for the title bar
        // 5. Where to go when the title bar is clicked

        $scope.searchOptions = false;
        $scope.chooseAddress = false;
        $scope.showProfile = 0;
        $scope.mainTab = newTab;

        switch (newTab) {
          case "map":
          case "list":
            $scope.mobileTabTitle = "Shared Earth";
            $scope.mobileBottom = "map";
            break;
          case "connections":
            $scope.mobileTabTitle = "Connections";
            $scope.mobileBottom = "connections";
            break;
          case "messages":
            $scope.mobileTabTitle = "Messages";
            $scope.mobileBottom = "conversations";
            break;
          case "profile":
            $scope.mobileTabTitle = "Profile";
            $scope.mobileBottom = "profile";
            break;
          case "resources":
            $scope.mobileTabTitle = "Resources";
            $scope.mobileBottom = "resources";
            break;
          case "settings":
            $scope.mobileTabTitle = "Settings";
            $scope.mobileBottom = "settings";
            break;
        }

        $location.path(buildUrl());
      };

      $scope.choices = {
        experience: $scope.activeTranslations.PROFILE_OPTIONS.EXPERIENCE,
        distances: $scope.activeTranslations.PROFILE_OPTIONS.DISTANCES,
        garden_size: $scope.activeTranslations.PROFILE_OPTIONS.GARDEN_SIZE,
        garden_status: $scope.activeTranslations.PROFILE_OPTIONS.GARDEN_STATUS,
        compensation: $scope.activeTranslations.PROFILE_OPTIONS.COMPENSATION,
        compensationOnboarding:
          $scope.activeTranslations.ONBOARDING.HAS_LAND.WELCOME
            .COMPENSATION_CHOICES,
        responsibilities:
          $scope.activeTranslations.PROFILE_OPTIONS.GARDENER_RESPONSIBILITIES
      };

      function buildName(firstName, lastName) {
        return [firstName, lastName].join(" ");
      }

      $scope.$watch("user.first_name", function(newValue, oldValue) {
        if ("user" in $scope) {
          $scope.user.display_name = buildName(newValue, $scope.user.last_name);
        }
      });

      $scope.$watch("user.last_name", function(newValue, oldValue) {
        if ("user" in $scope) {
          $scope.user.display_name = buildName(
            $scope.user.first_name,
            newValue
          );
        }
      });

      $scope.changingRoute = false;

      $scope.$on("$routeChangeSuccess", function(event, next, current) {
        $scope.changingRoute = true;

        if ($route.current.action == "recoverUrl") {
          $scope.mainTab = "map";
          $scope.activePlaceId = "-";
          $scope.showProfile = 0;

          if (userID) {
            $scope.forceChangePassword();
          } else {
            $scope.modal.changePassword.step = 2;
            $scope.modal.changePassword.show = true;
          }
        } else {
          // /desktop/:placeid/:tab1/:tab1opts/:profileid/:modal?

          $scope.activePlaceId = $route.current.params.placeid;

          if (
            typeof $scope.activePlaceId != "string" ||
            $scope.activePlaceId.length < 5
          ) {
            $scope.activePlaceId = 0;
          }

          $scope.mainTab = $route.current.params.tab1;

          if ($scope.mainTab == "map" || $scope.mainTab == "list") {
            $scope.search.displayType = $scope.mainTab;
          }

          $scope.showProfile = toInt($route.current.params.profileid, 0);

          if ($scope.showProfile !== 0) {
            dynamicGet(
              "/locations/profile/",
              { id: $scope.showProfile },
              function() {
                //$('#edit-my-profile').scrollTop(0); // This thing sure likes to scroll down
                makeTinyMap();
              }
            );
          }

          var tabopts = $route.current.params.tab1opts;

          if ($scope.mainTab == "messages") {
            if (tabopts != "-") {
              var nto = Number(tabopts);
              if ($scope.currentConversationID != nto) {
                $scope.selectConversation(nto);
              }
            }
          }

          /*
        $scope.tab1 = $route.current.params.tab1;
        $scope.tab1opts = $route.current.params.tab1opts;
        $scope.tab2 = $route.current.params.tab2;
        $scope.tab2opts = $route.current.params.tab2opts;
        var modalActive = $route.current.params.modal;
        
        if ( $scope.tab1 == "messages" )
        {
          if ( $scope.tab1opts != '-' )
          { 
            $scope.tab1opts = Number($scope.tab1opts);
            if ( $scope.currentConversationID != $scope.tab1opts )
            {
              $scope.selectConversation($scope.tab1opts);
            }
          }          
        }
        
        if ( $scope.tab1 == "myplaces" )
        {
          // Select the correct location in the list at left
          if ( $scope.tab1opts != '-' )
          { 
            if ( $scope.activePlaceId != $scope.tab1opts )
            {
              $scope.activePlaceId = $scope.tab1opts;
              $scope.onActivePlaceChange();
            }
          }          
        }
        
        if ( $scope.tab2 == 'profile' )
        {
          dynamicGet('/locations/profile/', {id:$scope.tab2opts}, function()
          {
            $('#edit-my-profile').scrollTop(0); // This thing sure likes to scroll down
            makeTinyMap();
          });
        }
        
        if ( modalActive && modalActive in $scope.modal )
        {
          $scope.modal[modalActive].show = true;
        }
        else
        {
          $scope.hideModal();
        }
        
        */
        }

        $scope.changingRoute = false;
      });

      function buildUrl() {
        $scope.tab1opts = "-";

        if ($scope.mainTab == "messages") {
          $scope.tab1opts = $scope.currentConversationID;
        }

        if ($scope.mainTab === undefined) {
          $scope.mainTab = "map";
          $scope.tab1opts = "0";
        }

        if ($scope.tab2 === undefined) {
          $scope.tab2 = "map";
          $scope.tab2opts = "-";
        }

        // /desktop/:placeid/:tab1/:tab1opts/:profileid/:modal?
        var url =
          "/app/" +
          $scope.activePlaceId +
          "/" +
          $scope.mainTab +
          "/" +
          $scope.tab1opts +
          "/" +
          $scope.showProfile +
          "/";
        var modal = null;

        angular.forEach($scope.modal, function(value, key) {
          if (value.show) modal = key;
        });

        if (modal) {
          url += modal;
        }

        return url;
      }

      $scope.profileHeaderClicked = function() {
        $scope.showProfile = 0;
        $location.path(buildUrl());
      };

      $scope.profileEditButtonClicked = function(locationUuid) {
        // For now, we can ignore userID and locationID because they
        // can only click on the active profile
        if ($scope.activePlaceId != locationUuid) {
          $scope.selectMyPlace(locationUuid);
        }
        $scope.setMainTab("profile");
      };

      $scope.numLocationMatches = function() {
        return $("#location-results-list tr").length;
      };

      $scope.updatePlace = function(field) {
        var place = $scope.activePlace();
        var data = {
          location_id: place.num_id,
          field_name: field,
          value: place[field]
        };

        // Update this one too
        if (field == "distance") {
          var v = parseInt(place[field]);
          if (v === 0) v = 0.5;
          place.distance_miles = v;
        }

        $http.post("/locations/change_field/", data);
      };

      $scope.placeCount = function() {
        if ($scope.myPlaces === null) return 0;

        return Object.keys($scope.myPlaces).length;
      };

      $scope.updateUser = function(field) {
        var user = $scope.user;
        var data = {
          field_name: field,
          value: user[field]
        };

        $http
          .post("/user/", data)
          .error(function(data, status, header, config) {
            // Revert to the old value
            if (data && "field_name" in data) {
              $scope.user[data.field_name] = data.value;
            }
          });
      };

      $scope.myPlaces = {};
      $http.get("/user/").success(function(data) {
        $scope.user = data;
      });

      $scope.mapCtrl = {};
      var clusterer = null;
      $scope.activePlaceId = 0;

      $scope.setMainTab = function(value) {
        rootScope.safeApply(function() {
          if (userID) {
            $scope.mainTab = value;
            $scope.chooseAddress = false;
            $scope.searchOptions = false;
            $scope.showProfile = 0;

            // Do this first to ensure a conversation in the URL, if there is one.
            if ($scope.mainTab == "messages") {
              $scope.activateMessagesTab();
            }

            $location.path(buildUrl());
          } else {
            $scope.signUp();
          }
        });
      };

      $scope.setTab = function(tabNumber, value) {
        rootScope.safeApply(function() {
          if (userID) {
            var tabName = "tab" + tabNumber;
            $scope[tabName] = value;
            $location.path(buildUrl());

            if (tabNumber == 1 && value == "messages") {
              $scope.activateMessagesTab();
            }
          } else {
            $scope.signUp();
          }
        });
      };

      $scope.activePlace = function() {
        if ($scope.activePlaceId) {
          return $scope.myPlaces[$scope.activePlaceId];
        } else {
          return {};
        }
      };

      $scope.map = {};
      $scope.map.ready = false;
      $scope.map.overlay = null;
      $scope.map.center = { latitude: 43.0344755, longitude: -89.4218245 };

      uiGmapGoogleMapApi.then(function(maps) {
        $rootScope.$broadcast("mapReady", { maps: maps });
      });

      $scope.safeBounds = null;

      $rootScope.$on("mapReady", function(event, args) {
        $scope.map.ready = true;
        $scope.map.zoom = 11;
        $scope.map.options = {
          panControl: !$scope.mobile,
          streetViewControl: false,
          zoomControlOptions: {
            style: google.maps.ZoomControlStyle.LARGE
          },
          mapTypeControl: false,
          minZoom: 10,
          maxZoom: 14,
          mapTypeId: google.maps.MapTypeId.ROADMAP,

          styles: [
            {
              featureType: "poi",
              stylers: [
                { saturation: -10 },
                { lightness: 10 },
                { visibility: "simplified" }
              ]
            },
            {
              featureType: "poi",
              elementType: "labels",
              stylers: [{ visibility: "off" }]
            },
            { featureType: "transit", stylers: [{ visibility: "off" }] },
            {
              featureType: "administrative.province",
              stylers: [{ visibility: "off" }]
            }
          ]
        };

        $scope.mapReady = true;
        $scope.loadMyPlaces();
      });

      $scope.startChatConnection = function() {
        $scope.chat = new ChatConnection(websocketURI);

        $scope.$on("focus", function() {
          $scope.selectConversation($scope.currentConversationID);
        });

        $interval(function() {
          $scope.chat.sendPing();
        }, 25 * 1000);
      };

      $scope.onUserChange = function() {
        $scope.myPlaces = {};

        $http.get("/user/").success(function(data) {
          $scope.user = data;
          $scope.activePlaceId = 0;
          $scope.loadMyPlaces();
          $scope.startChatConnection();
          $scope.loadFriends();
          $scope.reloadConversations();
        });
      };

      $scope.activePlaceChangeCount = 0;

      $scope.onActivePlaceChange = function() {
        if (_.isEmpty($scope.myPlaces)) return;

        $scope.safeApply(function() {
          // Hide any images that were uploading; hopefully they'll be removed in due course.
          $("li.uploading").hide();

          if (!_.isEmpty($scope.activePlace())) {
            $scope.map.center = {
              latitude: $scope.activePlace().lat,
              longitude: $scope.activePlace().lng
            };
          }

          if ($scope.activePlace() === undefined) return;

          // The type of search may change
          var newSearchType = "match";

          ++$scope.activePlaceChangeCount;
          if (newSearchType != $scope.search.searchType) {
            $scope.search.searchType = newSearchType;
          } else {
            $scope.loadMatches();
          }
        });
      };

      $scope.getBottomMarkerOptions = function(place) {
        return {
          zIndex: mapLowHighlightZ
        };
      };

      $scope.detailedMatches = [];
      $scope.detailedMatchesLoading = true;
      $scope.matches = [];
      $scope.imageList = [];

      $scope.search = {};
      $scope.search.displayType = "map";
      $scope.search.searchType = "";

      $scope.loadMatches = function() {
        var place = $scope.activePlace();

        if (_.isEmpty(place)) return;

        // 0.5 degrees is roughly 25 miles W/E and 35 miles N/S for the US.

        var searchOptions = {
          min_lat: Number(place.lat) - 0.5,
          max_lat: Number(place.lat) + 0.5,
          min_lng: Number(place.lng) - 0.5,
          max_lng: Number(place.lng) + 0.5,
          selected_location: $scope.activePlaceId
        };

        if (
          $scope.search.searchType == "have" ||
          $scope.search.searchType == "need"
        ) {
          // Include the correct search type
          searchOptions.type = $scope.search.searchType;
        } else if ($scope.search.searchType == "match") {
          switch ($scope.activePlace().type) {
            case "have":
              searchOptions.type = "need";
              break;
            case "need":
              searchOptions.type = "have";
              break;
          }

          // Add any tools they're looking for, too
          searchOptions.have_tools = $scope.activePlace().tools_i_need;
        } else if ($scope.search.searchType == "tools") {
          searchOptions.have_tools = $scope.toolSearchTags;
        } else if ($scope.search.searchType == "all") {
          // Nothing to do
        }

        $scope.safeBounds = new google.maps.LatLngBounds(
          new google.maps.LatLng(
            Number(place.lat) - 0.4,
            Number(place.lng) - 0.4
          ),
          new google.maps.LatLng(
            Number(place.lat) + 0.4,
            Number(place.lng) + 0.4
          )
        );

        $scope.lastValidCenter = google.maps.LatLng(
          Number(place.lat),
          Number(place.lng)
        );

        $.get("/locations/get/", searchOptions, function(data) {
          var matches = [];
          var locs = data.locations;
          for (var i = 0; i < locs.length; ++i) {
            var y = locs[i][0];
            var x = locs[i][1];
            var id = locs[i][2];
            var type = locs[i][3];
            var url = locs[i][4];
            matches.push({
              x: x,
              y: y,
              id: id,
              iter: 0,
              url: url,
              type: type,
              lngr: toRadians(x),
              latr: toRadians(y),
              latlng: new google.maps.LatLng(y, x)
            });
          }

          $scope.matches = matches;
          $scope.updateClusters = true;
          onMapIdle(null, null, null);
        });

        // Don't bother to fetch this if they aren't logged in.
        if (userID) {
          $.get("/locations/json_matches_list/", searchOptions, function(data) {
            rootScope.safeApply(function() {
              var locs = data.locations;
              $scope.detailedMatches = locs;
              $scope.detailedMatchesLoading = false;
            });
          });
        }
      };

      function onNewMyPlaces(places) {
        $scope.myPlaces = {};

        angular.forEach(places, function(value) {
          $scope.myPlaces[value.id] = injectMarkerOptions(value);
          $scope.myPlaces[value.id].coords = {
            longitude: value.lng,
            latitude: value.lat
          };
        });

        if (places.length) {
          if ($scope.activePlaceId === 0) {
            $scope.selectMyPlace(places[0].id);

            if ($scope.userID) {
              $scope.modal.onboarding.show = !$scope.user.onboard;
            }
          } else {
            $scope.onActivePlaceChange();
          }
        }
      }

      $scope.uploadFinished = function(uploaderDetails, results, url) {
        $rootScope.safeApply(function() {
          if (uploaderDetails.locationId in $scope.myPlaces) {
            var place = $scope.myPlaces[uploaderDetails.locationId];
            place.images.unshift({
              url_large: url,
              url_avatar: uploaderDetails.img240,
              id: results.id
            });
          }
        });
      };

      $scope.setPlacePicture = function(locationId, avatarURL, mapURL) {
        $rootScope.safeApply(function() {
          if (locationId in $scope.myPlaces) {
            $scope.myPlaces[locationId].avatar_url = avatarURL;
            $scope.myPlaces[locationId].topIcon.url = mapURL;
          }
        });
      };

      $scope.setProfilePicture = function(avatarURL) {
        $rootScope.safeApply(function() {
          $scope.user.avatar_url = avatarURL;
        });

        $rootScope.safeApply();
      };

      $scope.deletePicture = function(locationId, pictureId) {
        $rootScope.safeApply(function() {
          if (locationId in $scope.myPlaces) {
            pictureId = Number(pictureId);
            var images = $scope.myPlaces[locationId].images;
            $scope.myPlaces[locationId].images = _.without(
              images,
              _.findWhere(images, { id: pictureId })
            );
          }
        });
      };

      $rootScope.$on("updatePlaces", function(event, args) {
        rootScope.safeApply(function() {
          $scope.activePlaceId = args.id;
          onNewMyPlaces(args.places);
        });
      });

      $rootScope.$on("newLocation", function(event, args) {
        $scope.chooseAddress = false;
      });

      $scope.loadMyPlaces = function() {
        $http.get("/locations/json_list/").success(function(places) {
          onNewMyPlaces(places);
        });
      };

      $scope.removeActivePlace = function() {
        if (!$scope.activePlaceId) return;
      };

      $scope.activePlaceInfoWindowOptions = { visible: false, zIndex: 150 };

      $scope.selectMyPlace = function(id) {
        $scope.chooseAddress = false;
        $scope.activePlaceInfoWindowOptions.visible = false;
        if ($scope.activePlaceId != id) {
          $scope.activePlaceId = id;
          $scope.onActivePlaceChange();
          $location.path(buildUrl());
        }
      };

      // We show this user's profile now, rather than switching to this place,
      // since we're only showing the active profile.
      $scope.selectMyPlaceMap = function(id) {
        // $scope.selectMyPlace(id);
        deactivateCluster(); // Remove highlighting from any cluster
        $scope.showProfileTab($scope.myPlaces[id].num_id);
      };

      $scope.closeInfoWindowClick = function() {
        $scope.activePlaceInfoWindowOptions.visible = false;
      };

      $scope.chooseAddress = false;
      $scope.toggleAddressSelection = function() {
        $scope.chooseAddress = !$scope.chooseAddress;
      };

      $scope.activateAddressSelection = function() {
        $scope.chooseAddress = true;
      };

      $scope.headerClicked = function() {
        switch ($scope.mainTab) {
          case "map":
          case "profile":
          case "list":
            if ($scope.searchOptions) {
              $scope.searchOptions = false;
              return;
            }
            $scope.toggleAddressSelection();
        }
      };

      //
      // Modal dialogs
      //

      $scope.facebookActive = function() {
        return "FB" in window;
      };

      $scope.modal = {};
      $scope.modal.deletePlace = {};
      $scope.modal.deletePlace.show = false;

      $scope.modal.login = {};
      $scope.modal.login.show = false;

      $scope.modal.signup = {};
      $scope.modal.signup.show = false;

      $scope.modal.lostPassword = {};
      $scope.modal.lostPassword.show = false;
      $scope.modal.lostPassword.step = 0;

      $scope.modal.onboarding = {};
      $scope.modal.onboarding.show = false;
      $scope.modal.onboarding.step = 0;

      $scope.modal.changePassword = {};
      $scope.modal.changePassword.show = false;
      $scope.modal.changePassword.step = 0;

      $scope.doChangePassword = function() {
        $scope.modal.changePassword.canCancel = true;
        $scope.modal.changePassword.show = true;
        $scope.modal.changePassword.password = "";
        $scope.modal.changePassword.message = "";
        $scope.modal.changePassword.step = 0;
      };

      $scope.forceChangePassword = function() {
        $scope.modal.changePassword.canCancel = false;
        $scope.modal.changePassword.show = true;
        $scope.modal.changePassword.password = "";
        $scope.modal.changePassword.message = "";
        $scope.modal.changePassword.step = 0;
      };

      $scope.changePasswordSubmit = function() {
        if ($scope.modal.changePassword.password.length === 0) {
          $scope.modal.changePassword.message =
            $scope.activeTranslations.MODAL.CHANGE_PASSWORD.ERRORS.EMPTY;
          return;
        }

        $http
          .post("/password/", {
            password: $scope.modal.changePassword.password
          })
          .success(function(data) {
            $scope.modal.changePassword.step = 1;
          })
          .error(function(data, status, header, config) {
            if ("message" in data) {
              $scope.modal.changePassword.message =
                $scope.activeTranslations.MODAL.CHANGE_PASSWORD.ERRORS[
                  data.message
                ];
            } else {
              $scope.modal.changePassword.message =
                $scope.activeTranslations.MODAL.CHANGE_PASSWORD.ERRORS.GENERIC;
            }
          });
      };

      $scope.changePasswordCancel = function() {
        $scope.modal.changePassword.show = false;
      };

      $scope.onboardContinue = function(step) {
        $scope.modal.onboarding.step = step;

        if (step == 1) {
          $scope.user.onboard = true;
          $scope.updateUser("onboard");
        }

        if (step == 2) {
          $scope.modal.onboarding.show = false;
        }
      };

      $scope.hideModal = function() {
        angular.forEach($scope.modal, function(value, key) {
          value.show = false;
        });
      };

      $scope.closeModal = function() {
        $scope.hideModal();

        $location.path(buildUrl());
      };

      $scope.deletePlaceConfirmed = function(id) {
        $scope.closeModal();
        $http.post("/locations/delete/", { id: id }).success(function(data) {
          // delete $scope.myPlaces[id];
          $scope.activePlaceId = 0; // No active place anymore
          onNewMyPlaces(data.places); // Just reload the entire list
        });
      };

      $scope.deleteMyPlace = function(id) {
        // $log.log("Deleting " + id);
        $scope.modal.deletePlace.id = id;
        $scope.modal.deletePlace.name = $scope.myPlaces[id].address1
          ? $scope.myPlaces[id].address1
          : $scope.myPlaces[id].address2;
        $scope.modal.deletePlace.show = true;
      };

      $scope.logIn = function() {
        $scope.modal.login.message =
          $scope.activeTranslations.MODAL.LOG_IN.ERRORS.NONE;
        $scope.modal.login.email = "";
        $scope.modal.login.password = "";
        $scope.modal.login.show = true;

        $location.path(buildUrl());
      };

      $scope.lostPassword = function() {
        $scope.modal.lostPassword.message =
          $scope.activeTranslations.MODAL.LOG_IN.ERRORS.NONE;
        $scope.modal.lostPassword.step = 0;
        $scope.modal.lostPassword.email = "";
        $scope.modal.lostPassword.show = true;

        $location.path(buildUrl());
      };

      $scope.logOut = function() {
        $.post("/logout/").done(function() {
          window.location.replace("/");
        });
      };

      $scope.signUp = function() {
        $scope.modal.signup.message =
          $scope.activeTranslations.MODAL.SIGN_UP.ERRORS.NONE;
        $scope.modal.signup.firstname = "";
        $scope.modal.signup.lastname = "";
        $scope.modal.signup.email = "";
        $scope.modal.signup.password = "";
        $scope.modal.signup.show = true;

        $location.path(buildUrl());
      };

      $scope.lostPasswordSubmit = function() {
        $http
          .post("/password/", {
            email: $scope.modal.lostPassword.email,
            lost: true
          })
          .success(function(data) {
            $scope.modal.lostPassword.step = 1;
          })
          .error(function(data, status, header, config) {
            if ("message" in data) {
              if (data.message == "FACEBOOK") {
                $scope.modal.lostPassword.step = 2;
              } else {
                $scope.modal.lostPassword.message =
                  $scope.activeTranslations.MODAL.LOST_PASSWORD.ERRORS[
                    data.message
                  ];
              }
            } else {
              $scope.modal.lostPassword.message =
                $scope.activeTranslations.MODAL.LOST_PASSWORD.ERRORS.GENERIC;
            }
          });
      };

      $scope.logInSubmit = function() {
        $http
          .post("/login/", {
            email: $scope.modal.login.email,
            password: $scope.modal.login.password
          })
          .success(function(data) {
            // Redirect to a logged-in page
            // window.location.replace("/app/");
            userID = data.userID;
            $scope.userID = userID;
            $scope.onUserChange();
            $scope.hideModal();
          })
          .error(function(data, status, header, config) {
            if ("message" in data) {
              $scope.modal.login.message =
                $scope.activeTranslations.MODAL.LOG_IN.ERRORS[data.message];
            } else {
              $scope.modal.login.message =
                $scope.activeTranslations.MODAL.LOG_IN.ERRORS.GENERIC;
            }
          });
      };

      $scope.signUpSubmit = function() {
        $http
          .post("/signup/", {
            first_name: $scope.modal.signup.firstname,
            last_name: $scope.modal.signup.lastname,
            email: $scope.modal.signup.email,
            password: $scope.modal.signup.password
          })
          .success(function(data, status, header, config) {
            // window.location.replace("/app/");
            userID = data.userID;
            $scope.userID = userID;
            $scope.onUserChange();
            $scope.hideModal();
          })
          .error(function(data, status, header, config) {
            if ("message" in data) {
              $scope.modal.signup.message =
                $scope.activeTranslations.MODAL.SIGN_UP.ERRORS[data.message];
            } else {
              $scope.modal.signup.message =
                $scope.activeTranslations.MODAL.SIGN_UP.ERRORS.GENERIC;
            }
          });

        fbq("track", "CompleteRegistration");
      };

      var onMapIdle = function(map, eventName, args) {
        if (map === null) map = mapInstance;
        else mapInstance = map;

        if (map === null) return;

        var localUpdateClusters = function() {
          if ($scope.updateClusters) {
            clusterer = UpdateClusters(
              clusterer,
              map,
              $scope.matches,
              $scope.map.overlay
            );

            if (clusterer) {
              $scope.updateClusters = false;
            } else {
              // Try again in 0.1 seconds
              setTimeout(function() {
                localUpdateClusters();
              }, 100);
            }
          }
        };

        localUpdateClusters();
      };

      var onMapCenterChanged = function(map, eventName, args) {
        if ($scope.safeBounds) {
          var tc = map.getCenter();
          if ($scope.safeBounds.contains(tc)) {
            // Still within valid bounds, so save the last valid position
            $scope.lastValidCenter = tc;
            return;
          }

          // Not valid anymore; return to last valid position
          if ($scope.lastValidCenter) {
            map.panTo($scope.lastValidCenter);
          }
        }
      };

      var oldZoom = 0;

      var onMapZoomChanged = function(map, eventName, args) {
        $scope.updateClusters = true;

        //clusterer = UpdateClusters(clusterer, map, $scope.matches);
      };

      var onMapProjectionChanged = function(map, eventName, args) {
        $scope.map.overlay = new google.maps.OverlayView();
        $scope.map.overlay.draw = function() {};
        $scope.map.overlay.setMap(map);
        $scope.updateClusters = true;
      };

      $scope.$watch("search.displayType", function(newValue, oldValue) {
        $scope.mainTab = $scope.search.displayType;

        if ($scope.map.ready) {
          $location.path(buildUrl());
        }
      });

      $scope.$watch("search.searchType", function(newValue, oldValue) {
        if (newValue == oldValue) return;

        $scope.loadMatches();
      });

      $scope.$watch("toolSearchTags", function(newValue, oldValue) {
        if (newValue == oldValue) return;

        $scope.loadMatches();
      });

      $scope.showProfileByUser = function(id) {
        if ($scope.mobile) {
          $scope.mainTab = $scope.search.displayType;
        }
        window.viewLocationProfile(-id);
      };

      $scope.seViewLocationProfile = function(id) {
        window.viewLocationProfile(id);
      };

      $scope.mapEvents = {
        idle: onMapIdle,
        projection_changed: onMapProjectionChanged,
        center_changed: onMapCenterChanged,
        zoom_changed: onMapZoomChanged
      };

      $scope.showProfileTab = function(location_id) {
        rootScope.safeApply(function() {
          $scope.showProfile = location_id;
          $("#profile").scrollTop(0);
          $("#mobile-profile").scrollTop(0);
          $location.path(buildUrl());
        });
      };

      $scope.hasPlaces = function() {
        if ($scope.myPlaces === null) return false;

        return Object.keys($scope.myPlaces).length > 0;
      };

      $scope.showProfile = 0;

      // If the user is not logged in, make them create an account (or log in)
      if (!$scope.userID) {
        if ($location.path().startsWith("/recover")) {
          // Do nothing here
        } else {
          if ($location.path().endsWith("login"))
            $scope.modal.login.show = true;
          else $scope.signUp();
        }
      }

      //
      // SEARCH OPTIONS
      //

      $scope.onSearchDone = function() {
        $scope.searchOptions = false;
      };

      $scope.onSearchOptions = function() {
        $scope.searchOptions = true;
      };

      //
      // CONNECTIONS
      //

      $scope.connectionUserData = {};
      $scope.connections = [];
      $scope.sentConnectionRequests = [];
      $scope.connectionRequests = [];
      $scope.pendingConnectionCount = 0;
      $scope.connectionsLoading = true;

      $scope.loadFriends = function() {
        $http.get("/friends/").success(function(data) {
          $scope.connectionsLoading = false;
          $scope.connectionUserData = data.userData;
          $scope.sentConnectionRequests = data.sentConnectionRequests;
          $scope.connectionRequests = data.connectionRequests;
          $scope.connections = data.connections;
        });
      };

      $scope.getConnectionName = function(userID) {
        return $scope.connectionUserData[userID].name;
      };

      $scope.getConnectionAvatarUrl = function(userID) {
        return $scope.connectionUserData[userID].avatar_url;
      };

      $scope.connectedTo = function(userID) {
        if ($.inArray(userID, $scope.connections) >= 0) return true;

        if ($.inArray(userID, $scope.sentConnectionRequests) >= 0) return true;

        return false;
      };

      $scope.sendConnectionRequest = function(userID) {
        var c = {
          recipient: userID,
          connect: true
        };

        $http.post("/friends/", c);
      };

      $scope.cancelConnectionRequest = function(userID) {
        var c = {
          recipient: userID,
          connect: false
        };
        $http.post("/friends/", c);
      };

      $scope.handleConnection = function(connection) {
        rootScope.safeApply(function() {
          var senderData = connection.sender;
          var recipientData = connection.recipient;

          $scope.connectionUserData[senderData.id] = {
            name: senderData.name,
            avatar_url: senderData.avatar_url
          };
          $scope.connectionUserData[recipientData.id] = {
            name: recipientData.name,
            avatar_url: recipientData.avatar_url
          };

          var sender = senderData.id;
          var recipient = recipientData.id;

          if (connection.connect) {
            if ($scope.userID == sender) {
              // We initiated this
              if ($.inArray(recipient, $scope.connectionRequests) >= 0) {
                // Confirm a connection
                $scope.connectionRequests = _.without(
                  $scope.connectionRequests,
                  recipient
                );
                $scope.connections.push(recipient);
              } else {
                // Make a connection pending (if it's not already there)
                $scope.sentConnectionRequests = _.without(
                  $scope.sentConnectionRequests,
                  recipient
                );
                $scope.sentConnectionRequests.push(recipient);
              }
            } else {
              // The other user initiated this
              if ($.inArray(sender, $scope.sentConnectionRequests) >= 0) {
                // They confirmed our connection
                $scope.sentConnectionRequests = _.without(
                  $scope.sentConnectionRequests,
                  sender
                );
                $scope.connections.push(sender);
              } else {
                // Make the connection pending (if it's not already there).
                $scope.connectionRequests = _.without(
                  $scope.connectionRequests,
                  sender
                );
                $scope.connectionRequests.push(sender);
              }
            }
          } else {
            var user = $scope.userID == sender ? recipient : sender;
            // Remove from all connection lists
            $scope.connections = _.without($scope.connections, user);
            $scope.sentConnectionRequests = _.without(
              $scope.sentConnectionRequests,
              user
            );
            $scope.connectionRequests = _.without(
              $scope.connectionRequests,
              user
            );
          }
        });
      };

      if ($scope.userID) {
        $scope.loadFriends();
      }

      //
      // MESSAGING
      //

      function isMessagesActive() {
        return $scope.mainTab == "messages";
      }

      $scope.unreadMessageCount = 0;
      $scope.conversations = [];
      $scope.currentConversationID = -1;
      $scope.currentConversation = null;

      $scope.reloadConversations = function() {
        $http.get("/messaging/get/conversations/").success(function(data) {
          $scope.conversations = data.conversations;

          if ($scope.conversations.length) {
            for (var i = 0; i < $scope.conversations.length; ++i) {
              var conv = $scope.conversations[i];
              var c = $scope.calculateNewMessageCount(conv);
              conv.newMessageCount = c;

              if (conv.messages.length) {
                conv.highestMessageID =
                  conv.messages[conv.messages.length - 1].id;
              } else {
                conv.highestMessageID = 0;
              }
            }

            $scope.calculateUnreadCount();

            if ($scope.mainTab == "messages") {
              if ($scope.currentConversationID != -1) {
                $scope.selectConversation($scope.currentConversationID);
              } else {
                $scope.mobileBottom = "conversations";
              }
            }
          }
        });
      };

      if (userID) {
        // Load our existing conversations
        $scope.reloadConversations();
      }

      var uniqueConversationID = -1;

      $scope.beginMessage = function(userID, locID) {
        var i;

        for (i = 0; i < $scope.conversations.length; ++i) {
          if ($scope.conversations[i].userID == userID) {
            if ($scope.mobile) {
              $scope.showProfile = 0;
            }

            $scope.selectConversation($scope.conversations[i].id);
            $scope.setMainTab("messages");
            return;
          }
        }

        // We need to create a new conversation. Find the user details
        $http.get("/user/" + userID).success(function(data) {
          var username = data.display_name;
          var avatarURL = data.avatar_url;

          --uniqueConversationID;

          var c = {
            userID: userID,
            avatar_url: avatarURL,
            name: username,

            remoteReadID: 0,
            remoteReadTime: null,
            localReadID: 0,
            localReadTime: null,

            highestMessageID: 0,

            last_message_read_id: 0,
            id: uniqueConversationID,

            messages: []
          };

          if ($scope.mobile) {
            $scope.showProfile = 0;
          }
          $scope.conversations.push(c);

          $scope.selectConversation(uniqueConversationID);
          $scope.setMainTab("messages");
        });
      };

      $scope.calculateUnreadCount = function() {
        var unreadCount = 0;

        for (var i = 0; i < $scope.conversations.length; ++i) {
          unreadCount += $scope.conversations[i].newMessageCount;
        }

        $scope.unreadMessageCount = unreadCount;
      };

      $scope.calculateNewMessageCount = function(conversation) {
        if (conversation.messages.length === 0) return 0;

        if (conversation.last_message_read_id === 0)
          return conversation.messages.length;

        // Find the message
        var lb = lowerBound(
          conversation.messages,
          "id",
          conversation.last_message_read_id
        );

        var newMessageCount;

        if (lb < conversation.messages.length) {
          if (
            conversation.last_message_read_id == conversation.messages[lb].id
          ) {
            newMessageCount = conversation.messages.length - lb - 1;
          } else {
            // There is one additional message which is unread. And where did ours go?
            newMessageCount = conversation.messages.length - lb;
          }
        } else {
          newMessageCount = 0;
        }

        return newMessageCount;
      };

      $scope.activateMessagesTab = function() {
        if ($scope.conversations.length && $scope.currentConversationID == -1) {
          $scope.selectConversation($scope.conversations[0].id);
        }
      };

      $scope.selectConversation = function(id) {
        if (isNaN(id)) {
          $scope.mobileBottom = "conversations";
          return;
        }

        var index = -1;

        for (var i = 0; i < $scope.conversations.length; ++i) {
          if ($scope.conversations[i].id === id) {
            index = i;
            break;
          }
        }

        if (index != -1 && index < $scope.conversations.length) {
          $scope.currentConversationID = id;
          $scope.currentConversation = $scope.conversations[index];

          if ($scope.currentConversation.messages.length) {
            $scope.currentConversation.localReadID =
              $scope.currentConversation.messages[
                $scope.currentConversation.messages.length - 1
              ].id;
            $scope.currentConversation.localReadTime = new Date();
          }

          $scope.readLastMessage($scope.currentConversation);

          if (!$scope.changingRoute) {
            $location.path(buildUrl());
          }
        } else {
          $scope.currentConversationID = id;
        }

        if ($scope.showProfile) {
          // Update this
          $scope.showProfileByUser($scope.currentConversation.userID);
        }

        //if ( $scope.mobileBottom == 'conversations' )
        //{
        $scope.mobileBottom = "messages";
        //}
      };

      $scope.readLastMessage = function(conversation) {
        // Find the last message
        if (conversation.messages.length) {
          var message = conversation.messages[conversation.messages.length - 1];

          if (conversation.last_message_read_id < message.id) {
            conversation.last_message_read_id = message.id;
            $scope.currentConversation.newMessageCount = 0;
            $scope.calculateUnreadCount();
            $http.post("/messaging/read/", { id: message.id });
          }
        }
      };

      $scope.insertMessage = function(conversation, message) {
        // This should always be true, but, eh, just in case.
        if (message.id > conversation.highestMessageID) {
          conversation.highestMessageID = message.id;
          conversation.messages.push(message);
        } else {
          // Need to insert elsewhere

          if (message.id == conversation.highestMessageID) {
            // Already there!
            return;
          } else {
            var v = lowerBound(conversation.messages, "id", message.id);

            // Already there
            if (conversation.messages[v].id == message.id) return;

            conversation.messages.splice(v, 0, message);
          }
        }

        if (document.hasFocus() && conversation == $scope.currentConversation) {
          $scope.readLastMessage(conversation);
        } else {
          ++conversation.newMessageCount;
          $scope.calculateUnreadCount();
        }
      };

      $window.onfocus = function() {
        if (isMessagesActive()) {
          if ($scope.currentConversation) {
            $scope.readLastMessage($scope.currentConversation);
          }
        }
      };

      $window.onblur = function() {
        // $log.log("Window focus lost")
      };

      function ChatConnection(url) {
        if ("WebSocket" in window) {
          this.ws = null;
          this.isOpen = false;
          this.reopenDelay = 10; // Reopen delay of 10 ms if it closes
          this.open(url);
        } else {
          // Websockets not supported. TODO: Handle this situation much
          // better. Could even fall back to long polling, which would be
          // relatively simple to do.
          throw "Websockets not supported.";
        }
      }

      ChatConnection.prototype = {
        open: function(url) {
          var this_ = this;
          this.url = url;
          this.ws = new WebSocket(url);
          this.ws.onopen = function(event) {
            this_.onOpen(event);
          };
          this.ws.onerror = function(event) {
            this_.onError(event);
          };
          this.ws.onclose = function(event) {
            this_.onClose(event);
          };
          this.ws.onmessage = function(event) {
            this_.onReceive(event);
          };
        },

        close: function() {
          this.ws.close();
        },

        sendPing: function() {
          var str = JSON.stringify({ ping: "pong" });

          if (this.isOpen) {
            this.ws.send(str);
          }
        },

        onOpen: function(event) {
          // $log.log("WebSocket opened.");
          this.isOpen = true;
          var this_ = this;

          // Authenticate
          $http
            .get("/messaging/get/auth_token/")
            .success(function(data) {
              if (this_.isOpen) {
                var str = JSON.stringify({ auth: data.authToken });
                this_.ws.send(str);
                this_.reopenDelay = 10; // Reset our reopen delay.
              }
            })
            .error(function(data) {
              // We couldn't get our authentication token; the websocket is useless. Try again.
              this_.ws.close();
            });

          this.sendPing();
        },

        onError: function(event) {
          // $log.log("onError");
          // $log.log(event);
        },

        onClose: function(event) {
          this.isOpen = false;

          // Attempt to re-establish a connection, after a suitable delay.
          var this_ = this;
          window.setTimeout(function() {
            this_.open(this_.url);
          }, this.reopenDelay);

          if (this.reopenDelay < 1800000) this.reopenDelay *= 2; // Then double it, but wait no more than a half hour between
        },

        onReceive: function(event) {
          rootScope.safeApply(function() {
            var message = JSON.parse(event.data);

            // These are just to keep things alive
            if ("pong" in message) {
              return;
            }

            if ("connect" in message) {
              $scope.handleConnection(message);
              return;
            }

            if ("avatarChange" in message) {
              $scope.setProfilePicture(message.avatarURL);
              if ("locationUUID" in message) {
                $scope.setPlacePicture(
                  message.locationUUID,
                  message.avatarURL,
                  message.mapURL
                );
              }

              var images = $scope.activePlace().images;

              var image = {
                id: message.imageID,
                location_id: message.locationID,
                url_large: message.largeURL,
                url_avatar: message.avatarURL
              };

              images.push(image);

              return;
            }

            for (var i = 0; i < $scope.conversations.length; ++i) {
              var c = $scope.conversations[i];
              if (
                c.id == message.conversation ||
                c.userID == message.recipient
              ) {
                $scope.insertMessage(c, message);

                if (
                  $scope.currentConversationID == c.id &&
                  c.id != message.conversation
                ) {
                  $scope.currentConversationID = message.conversation;
                  $scope.selectConversation($scope.currentConversationID);
                  $location.path(buildUrl());
                  $location.replace();
                }

                c.id = message.conversation;
                return;
              }
            }

            // Did not find a matching convesation. Just reload all our conversations for now.
            $scope.reloadConversations();
          });
        }
      };

      if (userID) {
        $scope.startChatConnection();
      }

      $("html").removeClass("no-js");
    }
  ]);

  seApp.directive("ngEnter", function() {
    return function(scope, element, attrs) {
      element.bind("keydown keypress", function(event) {
        if (event.which === 13 && !event.shiftKey) {
          scope.$apply(function() {
            scope.$eval(attrs.ngEnter);
          });

          event.preventDefault();
        }
      });
    };
  });

  seApp.filter("startFrom", function() {
    return function(input, start) {
      if (input) return input.slice(start);
      else return null;
    };
  });

  seApp.directive("singleMessage", function() {
    return {
      restrict: "A",
      templateUrl: "/se-single-message.html"
    };
  });

  seApp.controller("NewMessageController", [
    "$scope",
    "$log",
    "$http",
    function($scope, $log, $http) {
      $scope.messageText = "";

      $scope.sendMessage = function() {
        // TODO: Fix this. It is disabled due to a bug on mobile, where the
        // messages start at the top rather than the bottom. A big disaster,
        // since as a result you can't see them when the keyboard is up.
        // $('#message-textarea').focus();

        if ($scope.messageText.length) {
          $scope.sendInProgress = true;

          var destConversation = $scope.currentConversation;

          var message = {
            to: $scope.currentConversation.userID,
            text: $scope.messageText
          };

          $http
            .post("/messaging/send/", message)
            .success(function(response) {
              if (destConversation.id === 0) {
                destConversation.id = response.conversation;
              }

              // If the WebSocket connection is gone, insert this ourselves
              if (!$scope.$parent.chat.isOpen) {
                message.sender = $scope.userID;
                message.id = response.id;

                $scope.$parent.insertMessage(
                  $scope.currentConversation,
                  message
                );
              }

              $scope.messageText = "";
              $scope.sendInProgress = false;
            })
            .error(function() {
              $scope.sendInProgress = false;
            });
        }
      };
    }
  ]);

  seApp.controller("ConversationController", [
    "$scope",
    function($scope) {
      $scope.longGap = function(conversation, message) {
        /*var messages = conversation.messages;
      var index = message.index;
      
      if ( index === 0 )
        return true;
      
      var t1 = messages[index-1].timeSent;
      var t2 = messages[index].timeSent;
      
      var delta = (t2 - t1) / 1000; // Difference in seconds
      
      return delta > 600;*/
        return false;
      };

      $scope.showTimeRead = function(conversation, message) {
        return message.id === conversation.remoteReadID;
      };
    }
  ]);
})();
