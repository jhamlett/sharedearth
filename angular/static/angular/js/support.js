var clusterIcons;

function getClusterIcon ( count )
{
  if ( clusterIcons === undefined )
  {
    clusterIcons = initializeClusterIcons();
  }

  if ( count < 5 )
    return clusterIcons[count-1];
  if ( count < 10 )
    return clusterIcons[4];
  if ( count < 25 )
    return clusterIcons[5];
  if ( count < 50 )
    return clusterIcons[6];

  return clusterIcons[7];    
}

function initializeClusterIcons ( )
{
  var i = 0;
  var icons = [];
  
  for ( var y = 0; y < 2; ++y )
  {
    for ( var x = 0; x < 4; ++x )
    {
      var icon = {
        anchor: new google.maps.Point(16,16),
        origin: new google.maps.Point(x*32,y*32),
        size: new google.maps.Size(32,32),
        scaledSize: new google.maps.Size(128,64),
        url: staticBase + 'map/img/number-sprites.png'
      };
      
      icons[i++] = icon;
    }
  }
  
  return icons;
}

var openInfoWindow;
function showClusterInfoWindow ( window, map, location )
{
  if ( openInfoWindow )
    openInfoWindow.close();
    
  window.open(map, location);
  
  openInfoWindow = window;
}

var activeCluster = null;

function deactivateCluster ( )
{
  if ( activeCluster )
  {
    activeCluster.circle.setOptions({strokeColor:"#008000", fillColor:"#008000"});
  }
}

function UpdateClusters ( clusterer, map, items, overlay )
{
  if ( !overlay || !overlay.getProjection() )
  {
    // Not ready yet.
    return null; 
  } 
  
  if ( clusterer )
  {
    clusters = clusterer.clusters;
  
    var fadeCircles = [];

    // Delete the old circles
    cnt = clusters.length;
    for ( i = 0; i < cnt; ++i )
    {
      if ( clusters[i].circle )
      {
        fadeCircles.push(clusters[i].circle);
        //clusters[i].circle.setMap(null);
        clusters[i].marker.setMap(null);
      }
    }
    
    var uniqueSpan = jQuery("<span/>").appendTo('#animph');
    
    uniqueSpan.css({opacity: 0.25});
    uniqueSpan.animate({opacity:0.0}, {duration:240, progress:function(animation,progress,remaining) {
      var options = {fillOpacity:uniqueSpan.css("opacity")};
      
      var cnt = fadeCircles.length;
      for ( var i = 0; i < cnt; ++i )
      {
        fadeCircles[i].setOptions(options);
      }
    },complete:function() {
      var cnt = fadeCircles.length;
      for ( var i = 0; i < cnt; ++i )
      {
        fadeCircles[i].setMap(null);
      }
      uniqueSpan.remove();
    }
    } );
  }

  // A lot of calculations just to get this distance
  
  var sw = overlay.getProjection().fromContainerPixelToLatLng(new google.maps.Point(100,160));
  var ne = overlay.getProjection().fromContainerPixelToLatLng(new google.maps.Point(160,100));
  var left = sw.lng();
  var right = ne.lng();
  var top = sw.lat();
  var bottom = ne.lat();
  var distance = deltaToKM((top+bottom)/2,left,(top+bottom)/2,right);
  var minRadius = distance * 1000 / 2;
  var clusters;
  var i, cnt;
      
  clusterer = new Clusterer(distance);
  clusterer.createClusters(items);
  clusters = clusterer.clusters;
  
  var newCircles = [];
  var newMarkers = [];
  cnt = clusters.length;
  
  for ( i = 0; i < cnt; ++i )
  {
    var thisCluster = clusters[i];
    var radius = Math.max(minRadius, clusters[i].mapRadius());
    var location;
    
    var icon;
    
    if ( clusters[i].circle === null )
    {
      location = new google.maps.LatLng(clusters[i].lat(), clusters[i].lng());
      
      var circle = new google.maps.Circle({
        center: location,
        radius:radius,
        strokeColor:"#008000",    // #F75800   #008000
        strokeOpacity:0.0,
        strokeWeight:0,
        fillColor:"#008000",      // #F75800   #008000
        fillOpacity:0,
        zIndex: mapClusterCircleZ
      });
      
      icon = getClusterIcon(clusters[i].points.length);
      var singleLoc = (clusters[i].points.length == 1);
      
      if ( singleLoc )
      {
        icon = {
          url: clusters[i].points[0].url,
          size: new google.maps.Size(96,96),
          origin: new google.maps.Point(0,0),
          scaledSize: new google.maps.Size(48,48),
          anchor: new google.maps.Point(24,24)
        };
      }

      var marker = new google.maps.Marker({
        position: location,
        icon: icon,
        map: map,
        opacity: 0,
        anchorPoint: new google.maps.Point(0, -8),
        zIndex: mapClusterMarkerZ
      });
      
      var closure = function (thisCluster, marker) {
      
        var infoWindow = new google.maps.InfoWindow({
          content: "Location details go here",
          zIndex: 200
        });
        
      
        var func = function() {
          deactivateCluster();
          thisCluster.circle.setOptions({strokeColor:"#F75800", fillColor:"#F75800"});
          activeCluster = thisCluster;
          
          if ( thisCluster.points.length > 1 )
          {
            map.panTo(marker.getPosition());
            var zoom = map.getZoom();
            if ( zoom < maxZoom )
            {
              map.setZoom(maxZoom);
            }
            else
            {
              var idList = [];
              for ( var i = 0; i < thisCluster.points.length; ++i )
              {
                idList.push(thisCluster.points[i].id);
              }
              
              $.get('/locations/card_list/', {loc_uuid: getSelectedLocationID(), get_id:idList}, function(response)
              {
                map.panTo(marker.getPosition());
                infoWindow.setContent(response.content);
                showClusterInfoWindow(infoWindow, map, marker);
              }).fail(function() {
                requireAccount();
              });
            }
          }
          else
          {
          
            map.panTo(marker.getPosition());
            viewLocationProfile(thisCluster.points[0].id);
            
            /*$.get('/locations/card/', {loc_uuid: getSelectedLocationID(), get_id:thisCluster.points[0].id}, function(response)
            {
              map.panTo(marker.getPosition());
              infoWindow.setContent(response.content);
              showClusterInfoWindow(infoWindow, map, marker);
            }).fail(function() {
              requireAccount();
            });*/
          }
        };
        google.maps.event.addListener(circle, "click", func);      
        google.maps.event.addListener(marker, "click", func);      
      };
      
      closure(thisCluster, marker);
      
      clusters[i].marker = marker;
      clusters[i].circle = circle;
      circle.setMap(map);
      newCircles.push(circle);
      newMarkers.push(marker);
    }
    else
    {
      if ( clusters[i].circle.getCenter().lat() != clusters[i].lat() || clusters[i].circle.getCenter().lng() != clusters[i].lng() )
      {
        location = new google.maps.LatLng(clusters[i].lat(), clusters[i].lng())
        clusters[i].circle.setCenter(location);
        clusters[i].marker.setPosition(location);
      }
      
      icon = getClusterIcon(clusters[i].points.length);
      
      if ( clusters[i].points.length == 1 )
      {
        icon = {
          url: clusters[i].points[0].url,
          size: new google.maps.Size(96,96),
          origin: new google.maps.Point(0,0),
          scaledSize: new google.maps.Size(48,48),
          anchor: new google.maps.Point(24,24)
        };
      }
            
      clusters[i].marker.icon = icon;
      
      if ( radius != clusters[i].circle.getRadius() )
      {
        clusters[i].circle.setRadius(radius);
      }
    }
  }
  
  if ( newCircles.length )
  {
    fadeIn(newCircles);
    fadeInMarkers(newMarkers);
  }      
      
  return clusterer;
}

function whenMapReady ( func )
{
  if ( mainScope.map.ready )
  {
    func();
  }
  else
  {
    rootScope.$on("mapReady", function (event, args)
    {
      func();
    });
  }
}

// Returns the index of the first element which does not compare less
// than val.
function lowerBound ( array, property, value )
{
  var first = 0;
  var count = array.length;

  while ( count > 0 )
  {
    var it = first;
    var step = count >> 1;
    it += step;

    if ( array[it][property] < value )
    {
      first = ++it;
      count -= step + 1;
    }
    else
    {
      count = step;
    }
  }

  return first;
}

function beginMessage ( userID, locationID )
{
  mainScope.beginMessage(userID, locationID);
}