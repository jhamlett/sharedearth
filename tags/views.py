from django.http.response import JsonResponse
from django.views.decorators.csrf import ensure_csrf_cookie
from tags.utilities import get_tag_field
from sharedearth.decorators import ensure_user_authorized
from sharedearth.custom_responses import HttpResponseNoContent
from profiles.tasks import on_new_tool


@ensure_user_authorized
@ensure_csrf_cookie
def add(request):
    d = request.POST
    tag_field = get_tag_field(request.user, d['location'], d['field'])
    tag_field.add(d['tag']) 
    
    if d['field'] == 'tools_i_have':
        on_new_tool(d['location'], d['tag'])

    return HttpResponseNoContent()


@ensure_user_authorized
@ensure_csrf_cookie
def get_list(request):
    d = request.GET
    tag_field = get_tag_field(request.user, d['location'], d['field'])
    
    return JsonResponse({'tags': list(tag_field.names())})


@ensure_user_authorized
@ensure_csrf_cookie
def delete(request):
    d = request.POST
    tag_field = get_tag_field(request.user, d['location'], d['field'])
    tag_field.remove(d['tag']) 
    
    return HttpResponseNoContent()

# EOF
