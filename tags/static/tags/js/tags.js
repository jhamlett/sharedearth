
function activateTags( elementID, dataSource )
{
  var tagsReady = false;
  
  $('#' + elementID).tokenInput(dataSource, { 
    theme: "sharedearth", 
    hintText: "",
    preventDuplicates: false,   // This is broken; inserts next item in a weird place 
    propertyToUse: 'v',
    onAdd: function( hidden_input, item )
    {
      if ( tagsReady )
      {
        item = hidden_input.v;
        $.post("/tags/add/", 
        {
          field:elementID,
          location:getSelectedLocationID(),
          tag:item
        }, function( data ) {
          // TODO: Detect failed tags here and kill them.
        });
      }    
    },
    onDelete: function( hidden_input, item )
    {
      item = hidden_input.v;
      $.post("/tags/delete/", 
      {
        field:elementID,
        location:getSelectedLocationID(),
        tag:item
      }, function( data ) {
        // TODO: Detect failed tags here and ... not delete them.
      });    
    },
    onReady: function ()
    {
      $.get("/tags/get/",
      {
        field:elementID,
        location:getSelectedLocationID()
      }, function (data ) {
        var tags = data.tags;
        
        for ( var i = 0; i < tags.length; ++i )
        {
          $('#' + elementID).tokenInput("add", {v: tags[i], id: tags[i]});
        }
        tagsReady = true;
      });
    }
  });
}

var searchTags = {}

function activateSearchTags( elementID, dataSource )
{
  $('#' + elementID).tokenInput(dataSource, { 
    theme: "sharedearth", 
    hintText: "",
    preventDuplicates: false,   // This is broken; inserts next item in a weird place 
    propertyToUse: 'v',
    onAdd: function( hidden_input, item )
    {
      item = hidden_input.v;
      searchTags[item] = true;
      reloadLocations();
    },
    onDelete: function( hidden_input, item )
    {
      item = hidden_input.v;
      delete searchTags[item];
      reloadLocations();
    },
    onReady: function ()
    {
    }
  });
}

$(function(){
  activateSearchTags('tool-search', tools);  
});


function activateAllTags ( )
{
  activateTags('what-i-like-to-grow', vegetables);
  activateTags('tools-i-need', tools);
  activateTags('tools-i-have', tools);
  activateTags('interests', interests);
}


