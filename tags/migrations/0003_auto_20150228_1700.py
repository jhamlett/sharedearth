# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('locations', '0004_auto_20150228_1700'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('tags', '0002_auto_20150224_1418'),
    ]

    operations = [
        migrations.CreateModel(
            name='PerLocationTag',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('type', models.CharField(max_length=8)),
                ('name', models.CharField(max_length=32)),
                ('location', models.ForeignKey(to='locations.Location')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PerUserTag',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('type', models.CharField(max_length=8)),
                ('name', models.CharField(max_length=32)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='have',
            name='location',
        ),
        migrations.DeleteModel(
            name='Have',
        ),
        migrations.RemoveField(
            model_name='interest',
            name='user',
        ),
        migrations.DeleteModel(
            name='Interest',
        ),
        migrations.RemoveField(
            model_name='skill',
            name='user',
        ),
        migrations.DeleteModel(
            name='Skill',
        ),
        migrations.RemoveField(
            model_name='want',
            name='location',
        ),
        migrations.DeleteModel(
            name='Want',
        ),
        migrations.RemoveField(
            model_name='whatiliketogrow',
            name='user',
        ),
        migrations.DeleteModel(
            name='WhatILikeToGrow',
        ),
        migrations.AlterIndexTogether(
            name='perusertag',
            index_together=set([('type', 'name'), ('user', 'type')]),
        ),
        migrations.AlterIndexTogether(
            name='perlocationtag',
            index_together=set([('type', 'name'), ('location', 'type')]),
        ),
    ]
