# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tags', '0007_auto_20150301_1432'),
    ]

    operations = [
        migrations.RunSQL("CREATE UNIQUE INDEX tags_tag_manual_1_uniq ON tags_tag (user_id, field, name, location_id) WHERE location_id IS NOT NULL;"),
        migrations.RunSQL("CREATE UNIQUE INDEX tags_tag_manual_2_uniq ON tags_tag (user_id, field, name) WHERE location_id IS NULL;"),
    ]
