# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tags', '0008_auto_20150301_1432'),
    ]

    operations = [
        migrations.AlterIndexTogether(
            name='tag',
            index_together=None,
        ),
        migrations.RemoveField(
            model_name='tag',
            name='location',
        ),
        migrations.RemoveField(
            model_name='tag',
            name='user',
        ),
        migrations.DeleteModel(
            name='Tag',
        ),
    ]
