# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('locations', '0005_auto_20150228_2113'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('tags', '0004_auto_20150301_1254'),
    ]

    operations = [
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('field', models.IntegerField(choices=[(0, 'What I like to grow'), (1, 'What I am interested in'), (2, 'Tools I need'), (3, 'Tools I have')])),
                ('name', models.CharField(max_length=32)),
                ('location', models.ForeignKey(null=True, blank=True, to='locations.Location')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='perlocationtag',
            unique_together=None,
        ),
        migrations.AlterIndexTogether(
            name='perlocationtag',
            index_together=None,
        ),
        migrations.RemoveField(
            model_name='perlocationtag',
            name='location',
        ),
        migrations.DeleteModel(
            name='PerLocationTag',
        ),
        migrations.AlterUniqueTogether(
            name='perusertag',
            unique_together=None,
        ),
        migrations.AlterIndexTogether(
            name='perusertag',
            index_together=None,
        ),
        migrations.RemoveField(
            model_name='perusertag',
            name='user',
        ),
        migrations.DeleteModel(
            name='PerUserTag',
        ),
        migrations.AlterIndexTogether(
            name='tag',
            index_together=set([('field', 'name'), ('location', 'field'), ('user', 'field')]),
        ),
    ]
