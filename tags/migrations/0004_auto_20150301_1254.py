# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tags', '0003_auto_20150228_1700'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='perlocationtag',
            unique_together=set([('location', 'type', 'name')]),
        ),
        migrations.AlterUniqueTogether(
            name='perusertag',
            unique_together=set([('user', 'type', 'name')]),
        ),
    ]
