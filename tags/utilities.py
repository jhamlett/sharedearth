from django.core.exceptions import ObjectDoesNotExist, PermissionDenied
from profiles.models import UserProfile
from locations.models import Location


what_i_like_to_grow = 'what-i-like-to-grow'
tools_i_need = 'tools-i-need'
tools_i_have = 'tools-i-have'
interests = 'interests'


def append_tag_field(base, field):
    if field == what_i_like_to_grow:
        return base.what_i_grow
    elif field == interests:
        return base.interests
    elif field == tools_i_need:
        return base.tools_i_need
    elif field == tools_i_have:
        return base.tools_i_have
    raise ObjectDoesNotExist
    
    
def get_tag_base(user, location, field):
    if field == what_i_like_to_grow or field == interests:
        # Per-user tags
        return UserProfile.objects.get(user=user)
    elif field == tools_i_need or field == tools_i_have:
        # Per-location tags
        location = Location.objects.get(uuid=location)
        if location.user != user:
            raise PermissionDenied
        return location        
    raise ObjectDoesNotExist
    

def get_tag_field(user, location, field):
    field = field.replace('_', '-')

    base = get_tag_base(user, location, field)
    return append_tag_field(base, field)

# EOF
