from django.conf.urls import patterns, url
from tags import views

urlpatterns = patterns('',
    # Examples:
    url(r'^add/', views.add, name='add_tag'),
    url(r'^get/', views.get_list, name='get_tag_list'),
    url(r'^delete/', views.delete, name='delete_tag')
)
