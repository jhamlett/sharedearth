// /*jshint globalstrict: true*/
// /*globals google,locations,location_bounds,Quadtrie,toRadians,deltaToKM,haversineDistance,Clusterer, excludeUser*/
// 'use strict';

// Some global varialbles. TODO: Turn this whole thing into a class. Later.
var autocomplete;
var debug = true;
var circles = [];
var maxResults = 100;
var minResults = 1;
var clusterer;
var oldZoom = 0;
var rawLocations;
var numberIcons = [];
var currentResultsIndex = 0;
var readyForBoundsChanged = false;

/*var autocompleteOptions = {
    types: ['(regions)'],
    componentRestrictions: {country: "us"}
};*/

function fadeIn ( newCircles )
{
  var uniqueSpan = jQuery("<span/>").appendTo('#animph');
  
  uniqueSpan.css({opacity: 0.0});
  uniqueSpan.animate({opacity:0.25}, {duration:240, progress:function(animation,progress,remaining)
  {
    var options = {fillOpacity:uniqueSpan.css("opacity")};
    
    var cnt = newCircles.length;
    for ( var i = 0; i < cnt; ++i )
    {
      newCircles[i].setOptions(options);
    }
  },complete:function() {
    uniqueSpan.remove();
  }
  });    
}

function fadeInMarkers ( newMarkers )
{
  var uniqueSpan = jQuery("<span/>").appendTo('#animph');
  
  uniqueSpan.css({opacity: 0.0});
  uniqueSpan.animate({opacity:1.0}, {duration:240, progress:function(animation,progress,remaining)
  {
    var options = {opacity:parseFloat(uniqueSpan.css("opacity"))};
    
    var cnt = newMarkers.length;
    for ( var i = 0; i < cnt; ++i )
    {
      newMarkers[i].setOptions(options);
    }
  },complete:function() {
    uniqueSpan.remove();
  }
  });    
}


function setLocation ( map, place )
{
  if (!place.geometry)
  {
    return;
  }

  $('#pac-input').val(place.formatted_address);

  // If the place has a geometry, then present it on a map, otherwise, choose a default zoom level that looks OK.
  if (place.geometry.viewport)
  {
    map.fitBounds(place.geometry.viewport);
  }
  else
  {
    map.setCenter(place.geometry.location);
    map.setZoom(13); 
  }   
}

/*
function onAutocompleteChanged ( )
{
  var result = autocomplete.getPlace();
  if(typeof result.address_components == 'undefined') {
    // The user pressed enter in the input 
    // without selecting a result from the list
    // Let's get the list from the Google API so that
    // we can retrieve the details about the first result
    // and use it (just as if the user had actually selected it)
    var autocompleteService = new google.maps.places.AutocompleteService();

    var placeOptions = {
        'input': result.name,
        'offset': result.name.length
    };
    jQuery.extend(placeOptions, autocompleteOptions);

    autocompleteService.getPlacePredictions(
        placeOptions,
        function listentoresult(list, status) {
          if(list === null || list.length === 0) {
            // There are no suggestions available.
            // The user saw an empty list and hit enter.
          } else {
            // Here's the first result that the user saw
            // in the list. We can use it and it'll be just
            // as if the user actually selected it
            // themselves. But first we need to get its details
            // to receive the result on the same format as we
            // do in the AutoComplete.
            var placesService = new google.maps.places.PlacesService(document.getElementById('pac-input'));
            placesService.getDetails(
                {'reference': list[0].reference},
                function detailsresult(detailsResult, placesServiceStatus) {
                  setLocation(map, detailsResult);
                }
            );
          }
        }
    );
  } else {
    setLocation(map, result);
  }   
}
*/


function initializeLocations ( locations )
{
  var count = locations.locations.length;
  
  var bounds = {
      left:-180, //locations.location_bounds.minLng,
      right:0, //locations.location_bounds.maxLng,
      top:0,  // locations.location_bounds.minLat,
      bottom: 90 // locations.location_bounds.maxLat
  };
  
  var maxObjects = 4;
  var maxLevels = 30;
  var quad = new Quadtrie(bounds, maxObjects, maxLevels);
  
  // x = Longitude
  // y = Latitude
  
  var locs = []

  for ( var i = 0; i < count; ++i )
  {
    if ( true ) // locations.locations[i][3] != excludeUser )
    {
    
      var y = locations.locations[i][0];
      var x = locations.locations[i][1];
      var id = locations.locations[i][2];
      var type = locations.locations[i][3];
      var url = locations.locations[i][4];
      quad.insert({x:x, y:y, id:id, iter:0, url: url, type:type, lngr:toRadians(x), latr:toRadians(y), latlng:new google.maps.LatLng(y,x)});
    }
  }
  
  return quad;
}


function initializeIcons ( )
{
  var i = 0;
  var icons = [];
  
  for ( var y = 0; y < 2; ++y )
  {
    for ( var x = 0; x < 4; ++x )
    {
      var icon = {
        anchor: new google.maps.Point(16,16),
        origin: new google.maps.Point(x*32,y*32),
        size: new google.maps.Size(32,32),
        scaledSize: new google.maps.Size(128,64),
        url: staticBase + 'map/img/number-sprites.png'
      };
      
      icons[i++] = icon;
    }
  }
  
  return icons;
}

function getIcon ( count )
{
  if ( count < 5 )
    return numberIcons[count-1];
  if ( count < 10 )
    return numberIcons[4];
  if ( count < 25 )
    return numberIcons[5];
  if ( count < 50 )
    return numberIcons[6];

  return numberIcons[7];    
}

var zoomChanged = false;

function onZoomChanged ( )
{
  zoomChanged = true;
}

function onBoundsChanged ( )
{
  if ( !readyForBoundsChanged )
    return;

  if ( !zoomChanged )
    return;
  zoomChanged = false;

  var center = map.getCenter();

  if ( debug )
  {
    $('#latitude').text(center.lat());
    $('#longitude').text(center.lng());
  }
  
  var bounds = map.getBounds();
  var sw = bounds.getSouthWest();
  var ne = bounds.getNorthEast();

  // Assume North America only; no wrapping at 0. Also, top and bottom are technically switched
  // but we're dealing with > and <, not true top and bottom.
  /*var left = sw.lng();
  var right = ne.lng();
  var top = sw.lat();
  var bottom = ne.lat();*/
  
  var left = -180, //locations.location_bounds.minLng,
      right = 0, //locations.location_bounds.maxLng,
      top = 0,  // locations.location_bounds.minLat,
      bottom = 90; // locations.location_bounds.maxLat
  var items = rawLocations.retrieve({left:left, right:right, top:top, bottom:bottom});
  
  left = sw.lng();
  right = ne.lng();
  top = sw.lat();
  bottom = ne.lat();
  
  var distance = deltaToKM((top+bottom)/2,left,(top+bottom)/2,right) / 10;
  var minRadius = distance * 1000 / 2;
  var clusters;
  var i, cnt;

  if ( true ) //map.getZoom() != oldZoom )
  {
    if ( clusterer )
    {
      clusters = clusterer.clusters;
    
      var fadeCircles = [];

      // Delete the old circles
      cnt = clusters.length;
      for ( i = 0; i < cnt; ++i )
      {
        if ( clusters[i].circle )
        {
          fadeCircles.push(clusters[i].circle);
          clusters[i].marker.setMap(null);
        }
      }
      
      var uniqueSpan = jQuery("<span/>").appendTo('#animph');
      
      uniqueSpan.css({opacity: 0.25});
      uniqueSpan.animate({opacity:0.0}, {duration:240, progress:function(animation,progress,remaining) {
        var options = {fillOpacity:uniqueSpan.css("opacity")};
        
        var cnt = fadeCircles.length;
        for ( var i = 0; i < cnt; ++i )
        {
          fadeCircles[i].setOptions(options);
        }
      },complete:function() {
        var cnt = fadeCircles.length;
        for ( var i = 0; i < cnt; ++i )
        {
          fadeCircles[i].setMap(null);
        }
        uniqueSpan.remove();
      }
      } );
    }
    
    clusterer = new Clusterer(distance);
    oldZoom = map.getZoom();
  }
  
  clusterer.createClusters(items);
  
  clusters = clusterer.clusters;
  
  var newCircles = [];
  var newMarkers = [];
  cnt = clusters.length;
  
  for ( i = 0; i < cnt; ++i )
  {
    var thisCluster = clusters[i];
    var radius = Math.max(minRadius, clusters[i].mapRadius());
    var location;
    
    var icon;
    
    if ( clusters[i].circle === null )
    {
      location = new google.maps.LatLng(clusters[i].lat(), clusters[i].lng());
      
      var circle = new google.maps.Circle({
        center: location,
        radius:radius,
        strokeColor:"#F75800",
        strokeOpacity:0.0,
        strokeWeight:0,
        fillColor:"#F75800", // #F75800   #008000
        fillOpacity:0,
        zIndex: mapClusterCircleZ
      });
      
      icon = getIcon(clusters[i].points.length);
      var singleLoc = (clusters[i].points.length == 1);
      
      if ( singleLoc )
      {
        icon = {
          url: clusters[i].points[0].url,
          size: new google.maps.Size(96,96),
          origin: new google.maps.Point(0,0),
          scaledSize: new google.maps.Size(48,48),
          anchor: new google.maps.Point(24,24)
        };
      }

      var marker = new google.maps.Marker({
        position: location,
        icon: icon,
        map: map,
        opacity: 0,
        anchorPoint: new google.maps.Point(0, -8),
        zIndex: mapClusterMarkerZ
      });
      
      var closure = function (thisCluster, marker) {
      
        var infoWindow = new google.maps.InfoWindow({
          content: "Location details go here"
        });
      
        var func = function() {
          if ( thisCluster.points.length > 1 )
          {
            map.panTo(marker.getPosition());
            var zoom = map.getZoom();
            if ( zoom < maxZoom )
            {
              map.setZoom(zoom+1);
            }
            else
            {
              var idList = [];
              for ( var i = 0; i < thisCluster.points.length; ++i )
              {
                idList.push(thisCluster.points[i].id);
              }
              $.get('/locations/card_list/', {loc_uuid: getSelectedLocationID(), get_id:idList}, function(response)
              {
                map.panTo(marker.getPosition());
                infoWindow.setContent(response.content);
                showInfoWindow(infoWindow, marker);
              }).fail(function() {
                requireAccount();
              });
            }
          }
          else
          {
            $.get('/locations/card/', {loc_uuid: getSelectedLocationID(), get_id:thisCluster.points[0].id}, function(response)
            {
              map.panTo(marker.getPosition());
              infoWindow.setContent(response.content);
              showInfoWindow(infoWindow, marker);
            }).fail(function() {
              requireAccount();
            });
          }
        };
        google.maps.event.addListener(circle, "click", func);      
        google.maps.event.addListener(marker, "click", func);      
      };
      
      closure(thisCluster, marker);
      
      clusters[i].marker = marker;
      clusters[i].circle = circle;
      circle.setMap(map);
      newCircles.push(circle);
      newMarkers.push(marker);
    }
    else
    {
      if ( clusters[i].circle.getCenter().lat() != clusters[i].lat() || clusters[i].circle.getCenter().lng() != clusters[i].lng() )
      {
        location = new google.maps.LatLng(clusters[i].lat(), clusters[i].lng())
        clusters[i].circle.setCenter(location);
        clusters[i].marker.setPosition(location);
      }
      
      icon = getIcon(clusters[i].points.length);
      
      if ( clusters[i].points.length == 1 )
      {
        icon = {
          url: clusters[i].points[0].url,
          size: new google.maps.Size(96,96),
          origin: new google.maps.Point(0,0),
          scaledSize: new google.maps.Size(48,48),
          anchor: new google.maps.Point(24,24)
        };
      }
            
      clusters[i].marker.icon = icon;
      
      if ( radius != clusters[i].circle.getRadius() )
      {
        clusters[i].circle.setRadius(radius);
      }
    }
  }
  
  if ( newCircles.length )
  {
    fadeIn(newCircles);
    fadeInMarkers(newMarkers);
  }

}

var rawLocations;
var numberIcons = [];

function getLocations ( bounds )
{
  var lat1 = bounds.getSouthWest().lat();
  var lat2 = bounds.getNorthEast().lat();
  var lng1 = bounds.getSouthWest().lng();
  var lng2 = bounds.getNorthEast().lng();
  
  var searchCheck = $('#map-type input:checked').attr('id');
  var type;
  
  var searchOptions =
  {
    min_lat: Math.min(lat1, lat2),
    max_lat: Math.max(lat1, lat2),
    min_lng: Math.min(lng1, lng2),
    max_lng: Math.max(lng1, lng2),
    selected_location: getSelectedLocationID()
  };
  
  if ( searchCheck == 'search-gardens' )
    searchOptions.type = 'have';
  else if ( searchCheck == 'search-gardeners' )
    searchOptions.type = 'need';
  else if ( searchCheck == 'search-tools')
  {
    var s = '';
    for ( tag in searchTags )
      s += ',' + tag;
    
    searchOptions.have_tools = s;
  }
    
    
  $.get('/locations/get/', searchOptions, function ( data )
  {
     rawLocations = initializeLocations(data);
     readyForBoundsChanged = true;
     zoomChanged = true;
     onBoundsChanged();
  });
  
  dynamicGet('/locations/matches_list/', searchOptions, function(){
    // Nothing to do, really.    
  });
}

var safeBounds;
var mapReady = false;

function resetMapBounds ( )
{
  if ( !mapReady )
    return;
    
  var center = map.getCenter();
  
  // TODO - Use actual distance calculations
  safeBounds = new google.maps.LatLngBounds(
    new google.maps.LatLng(center.lat() - 0.4, center.lng() - 0.4), 
    new google.maps.LatLng(center.lat() + 0.4, center.lng() + 0.4)
  );
  
    
  /*safeBounds = new google.maps.LatLngBounds(
    new google.maps.LatLng(map.getBounds().getSouthWest().lat(), map.getBounds().getSouthWest().lng()), 
    new google.maps.LatLng(map.getBounds().getNorthEast().lat(), map.getBounds().getNorthEast().lng())
  );*/
  
  getLocations(safeBounds);
}


function advancedMapInitialize ( )
{
  // Load these guys
  numberIcons = initializeIcons();
  
  // Stop from scrolling all the way out.
  
  google.maps.event.addListenerOnce(map, 'idle', function() {
    // Do all of this only the first time the map is loaded
    mapReady = true;
    
    var tc = map.getCenter();
    resetMapBounds();
    
    var lastValidCenter = tc;

    // Prevent scrolling too far
    google.maps.event.addListener(map, 'center_changed', function() {
      if ( safeBounds == null )
        return;
        
      var tc = map.getCenter();
      if (safeBounds.contains(tc)) {
        // Still within valid bounds, so save the last valid position
        lastValidCenter = tc;
        return; 
      }

      // Not valid anymore; return to last valid position
      map.panTo(lastValidCenter);
    });
  });

  google.maps.event.addListener(map, 'zoom_changed', onZoomChanged); 
  google.maps.event.addListener(map, 'idle', onBoundsChanged); 

}


function reloadLocations ( )
{
  getLocations(safeBounds);
}

function searchOnChangeType ( )
{
  if ( $('#search-tools').is(':checked') )
  {
    reloadLocations();
  }
  else
  {
    reloadLocations();  
  }
}

function showCard ( location )
{
  $.get('/locations/card/', {loc_uuid: getSelectedLocationID(), get_id:location, back:true}, function(response)
  {
    swapInfoWindow(response.content);
  }).fail(function() {
    requireAccount();
  });  
}

function swapInfoWindow ( newContent )
{
  openInfoWindow.savedContent = openInfoWindow.getContent();
  openInfoWindow.setContent(newContent);
}

function infoWindowBack ( )
{
  openInfoWindow.setContent(openInfoWindow.savedContent);
  delete openInfoWindow.savedContent;
}