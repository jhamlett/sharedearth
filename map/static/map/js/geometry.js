
//Requires coordinates in radians
function haversineDistance( lat1, lon1, lat2, lon2 )
{
    var latd = lat2 - lat1;
    var lond = lon2 - lon1;
    var a = Math.sin(latd / 2) * Math.sin(latd / 2) +
    Math.cos(lat1) * Math.cos(lat2) *
         Math.sin(lond / 2) * Math.sin(lond / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    return 6371.0 * c;
}

function toRadians (angle)
{
	return angle * (Math.PI / 180);
}

function toDegrees (angle)
{
	return angle * (180 / Math.PI);
}

function deltaToKM(lat1, lon1, lat2, lon2){  // generally used geo measurement function
    var R = 6378.137; // Radius of earth in KM
    var dLat = (lat2 - lat1) * Math.PI / 180;
    var dLon = (lon2 - lon1) * Math.PI / 180;
    var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
    Math.sin(dLon/2) * Math.sin(dLon/2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = R * c;
    return d; // meters
}