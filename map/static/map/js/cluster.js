/*jshint globalstrict: false*/
/*globals google,locations,location_bounds,Quadtrie,toDegrees,toRadians,deltaToKM,haversineDistance,Clusterer*/

// 'use strict';

(function(){

var globalIter = 0;

function Cluster ( point, radius )
{
  this.latr = point.latr;
  this.lngr = point.lngr;
  this.radius = radius;
  this.trueRadius = 0;
  this.points = [ point ];
  
  this.latSum = this.latr;
  this.lngSum = this.lngr;
  this.ptCnt = 1;
  this.circle = null;
}

Cluster.prototype = 
{
  inCluster : function ( point )
  {
    return haversineDistance(this.latr, this.lngr, point.latr, point.lngr) < this.radius;
  },

  recalculateLatLng : function ( )
  {
    var ptCnt = this.points.length;
    this.latr = this.latSum / ptCnt;
    this.lngr = this.lngSum / ptCnt;
  },

  addPoint : function ( point )
  {
    this.points.push(point);
    this.latSum += point.latr;
    this.lngSum += point.lngr;
    this.recalculateLatLng();
    
    var cnt = this.points.length;
    
    var distance;
    // TODO: Don't hard-code this threshold here and either make it dynamic or choose a number that makes sense 
    if ( cnt < 10 )
    {
      for ( var i = 0; i < cnt; ++i )
      {
        distance = haversineDistance(this.latr, this.lngr, this.points[i].latr, this.points[i].lngr); 
        if ( distance > this.trueRadius )
        this.trueRadius = distance;
      }
    }
    else
    {
       distance = haversineDistance(this.latr, this.lngr, point.latr, point.lngr);
       if ( distance > this.trueRadius )
        this.trueRadius = distance;
    }
  },
  
  lat : function ( )
  {
    return toDegrees(this.latr);
  },
  
  lng : function ( )
  {
    return toDegrees(this.lngr);
  },
  
  mapRadius : function ( )
  {
    if ( this.trueRadius === 0 )
    {
      return this.radius * 1000 / 4; 
    }
    else
    {
      return this.trueRadius * 1000;
    }
  }
};


function Clusterer ( clusterRadius, iter )
{
  this.clusterRadius = clusterRadius;
  this.iter = ++globalIter;
  this.clusters = [];
}

Clusterer.prototype =
{
  addToClosestCluster : function  ( marker )
  {
    // This point is already in a cluster for this round
    if ( marker.iter == this.iter )
      return;
    
    marker.iter = this.iter;
    
    // Add to an existing cluster if we can
    var cnt = this.clusters.length;
    for ( var i = 0; i < cnt; ++i )
    {
      if ( this.clusters[i].inCluster(marker) )
      {
        this.clusters[i].addPoint(marker);
        return;
      }
    }
    
    // Otherwise, create a new cluster
    var cluster = new Cluster(marker, this.clusterRadius);
    this.clusters.push(cluster);
  },

  // Generate clusters for an entire array of points
  createClusters : function ( points)
  {
    var cnt = points.length;
    for ( var i = 0; i < cnt; ++i )
    {
      this.addToClosestCluster(points[i]);
    }
  }
};

window.Clusterer = Clusterer; 

})();