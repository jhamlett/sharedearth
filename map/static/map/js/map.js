var map;
var globalMap;
var openInfoWindow;

var minZoom = 11; 
var maxZoom = 14;


// On map load
function oml()
{
  if ( $("#locations-list li").length == 0 )
    guessInitialLocation();
    
  installAutocomplete(); 
  createMap();
}

function createMap ( )
{
  var mapDelay = 2000;
  
  hideMap();
  if ( guessingLocation )
  {
    window.setTimeout(showMap, mapDelay);
  }
  else
  {
    showMap();
  }

  createMapInternal();
}

function hideMap ( )
{
  $('#map-canvas').css({ opacity: 0.0 });
}

var mapShown = false;

function showMap ( )
{
  if ( mapShown )
    return;
    
  mapShown = true;
  $('#map-canvas').fadeTo(1000, 1.0);
}

var mapIsReady = false;
var funcsToRun = [];

function runWhenMapIsReady ( func )
{
  if ( mapIsReady )
  {
    func();
  }
  else
  {
    funcsToRun.push(func);
  }
}

function createMapInternal ( )
{
  if ( globalMap )
    return;
    
  var activeLocation = $('ul.address-list li.location.active');
  
  var lat, lng, selId;
  
  if ( activeLocation.length )
  {
    lat = activeLocation.data('lat');
    lng = activeLocation.data('lng');
    selId = activeLocation.data('id');
  }
  else
  {
    //lat = 40.7903;
    //lng = -73.9597;
    lat = 43.0344755;
    lng = -89.4218245000000;
    selId = null;
  }

  var c = new google.maps.LatLng(lat, lng);

  var mapOptions = {
    center: c,
    zoom: 11,
    panControl: true,
    streetViewControl: false,
    zoomControlOptions: {
        style: google.maps.ZoomControlStyle.LARGE
    },            
    mapTypeControl: false,
    minZoom: minZoom, 
    maxZoom: maxZoom,
    mapTypeId: google.maps.MapTypeId.ROADMAP  
  };
  
  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
  globalMap = map; 
  
    var styles = [
//    {"stylers": [{ "saturation": 0 }]},
//    {"featureType":"landscape","stylers":[{"saturation":-10},{"lightness":65},{"visibility":"on"}]},
//    {"featureType":"poi","stylers":[{"saturation":-10},{"lightness":51},{"visibility":"simplified"}]},
    {"featureType":"poi","stylers":[{"saturation":-10},{"lightness":10},{"visibility":"simplified"}]},
//    {"featureType":"road.highway","stylers":[{"saturation":-10},{"visibility":"simplified"}]},
    {"featureType": "poi","elementType": "labels","stylers": [{ "visibility": "off" }]},
//    {"featureType":"road.arterial","stylers":[{"saturation":-35},{"lightness":30},{"visibility":"on"}]},
//    {"featureType":"road.local","stylers":[{"saturation":-10},{"lightness":40},{"visibility":"on"}]},
    {"featureType":"transit","stylers":[{"visibility":"off"}]},
    {"featureType":"administrative.province","stylers":[{"visibility":"off"}]}
//    {"featureType":"water","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":-25},{"saturation":-100}]},
//    {"featureType":"water","elementType":"geometry","stylers":[{"hue":"#ffff00"},{"lightness":-25},{"saturation":-97}]},
//    {"featureType": "water","elementType": "geometry.fill","stylers": [{ "color": "#40C4F2" }]},
  ];

    map.setOptions({styles: styles}); 
 
  google.maps.event.addListenerOnce(map, 'idle', function(){
    mapIsReady = true;
    
    for ( var i = 0; i < funcsToRun.length; ++i )
    {
      var func = funcsToRun[i];
      func();
    }
    
    funcsToRun = [];
  }); 
    

  var map_type = document.getElementById('map-type');
  map.controls[google.maps.ControlPosition.TOP_RIGHT].push(map_type);
  
  google.maps.event.addListenerOnce(map, 'idle', function(){
    $('#map-type').show();
  });
  
  createLocationIcons();
  
  if ( selId )
  {
    selectMapLocation(selId);
  }


  advancedMapInitialize();  

      
  /*  
  google.maps.event.addListener(marker, "rightclick", function() {
    marker.setIcon(image2); // set image path here...
  });
  */ 
  
}
 

function moveMapToLocation ( lat, lng )
{
  showMap();
  var c = new google.maps.LatLng(lat, lng);
  map.panTo(c);
}

var mapClusterCircleZ = 50;
var mapClusterMarkerZ = 51;

var mapLowHighlightZ = 100;
var mapIconZ = 101;
var mapHighlightZ = 102;
var mapSelectedZ = 103;
var locations = {};
var selOpacity = 0.4;


function destroyLocationIcon ( i )
{
  locations[i].marker.setMap(null);
  locations[i].selMarker.setMap(null);
}


function changeLocationIcon ( i, newUrl )
{
  var icon = locations[i].marker.getIcon();
  icon.url = newUrl;
  locations[i].marker.setIcon(icon);  
}


function destroyLocationIcons ( )
{
  for ( var i in locations )
    destroyLocationIcon(i);
  
  locations = {};
}


function createLocationIcons ( )
{
  destroyLocationIcons();

  $('ul.address-list li.location').each(function ( index )
  {
    var location = $(this);

    var loc = {
      id : location.data("id"),
      lat : location.data("lat"),
      lng : location.data("lng"),
      name : location.data("name"),
      type : location.data("type"),
      mapIcon : location.data("mapIcon"),
      mapSelIcon :  location.data("mapSelIcon")
    };

    loc.latlng = new google.maps.LatLng(loc.lat, loc.lng);

    loc.icon = {
      url: loc.mapIcon,
      size: new google.maps.Size(96,96),
      origin: new google.maps.Point(0,0),
      scaledSize: new google.maps.Size(48,48),
      anchor: new google.maps.Point(24,24)    
    };
    
    loc.selIcon = {
      url: loc.mapSelIcon,
      size: new google.maps.Size(120,120),
      origin: new google.maps.Point(0,0),
      scaledSize: new google.maps.Size(60,60),
      anchor: new google.maps.Point(30,30)
    };
    
    loc.marker = new google.maps.Marker({
        position: loc.latlng,
        map: globalMap,
        icon: loc.icon,
        title: loc.name,
        zIndex: mapIconZ,
        anchorPoint: new google.maps.Point(0, -8)
    });      

    loc.selMarker = new google.maps.Marker({
        position: loc.latlng,
        map: globalMap,
        icon: loc.selIcon,
        zIndex: mapLowHighlightZ,
        opacity: selOpacity
    });
    
    var infoWindowOptions = {
      content: loc.type == 'have' ? '<div style="overflow: hidden">This is your garden.</div>' : '<div style="overflow: hidden">This is you. You need a garden.</div>',
      anchorPoint: new google.maps.Point(0, 0)
    };
    
    loc.infoWindow = new google.maps.InfoWindow(infoWindowOptions);
        
    var locationId = loc.id;
    google.maps.event.addListener(loc.marker, "click", function() {
      selectLocation(locationId);
      showInfoWindow(loc.infoWindow, loc.marker);
    });

    locations[loc.id] = loc;
  });
}

function showInfoWindow ( window, location )
{
  if ( openInfoWindow )
    openInfoWindow.close();
    
  window.open(map, location);
  
  openInfoWindow = window;
}

var mapSelectedId = '';

function selectMapLocation ( id )
{
  if ( mapSelectedId in locations )
  { 
    locations[mapSelectedId].selMarker.setZIndex(mapLowHighlightZ);
    locations[mapSelectedId].selMarker.setOpacity(selOpacity);
    locations[mapSelectedId].marker.setZIndex(mapIconZ);
  }
  
  if ( id in locations )
  {
    locations[id].marker.setZIndex(mapSelectedZ);
    locations[id].selMarker.setZIndex(mapHighlightZ);
    locations[id].selMarker.setOpacity(1.0);
    showInfoWindow(locations[id].infoWindow, locations[id].marker);
    safeBounds = null;
    moveMapToLocation(locations[id].lat, locations[id].lng);
    resetMapBounds();
  }
  
  mapSelectedId = id;
}

var searchOnOptionChanged = true;


function selectSearchOption ( type )
{
  searchOnOptionChanged = false;

  if ( type == "have" )
    $('#search-gardeners').prop("checked", true);
  else if ( type == "need" )
    $('#search-gardens').prop("checked", true);
    
  searchOnOptionChanged = true;
}






function makeTinyMap ( )
{
  whenMapReady(function() {
    var lat = $('#tiny-map').data("lat");
    var lng = $('#tiny-map').data("lng");
    var center = new google.maps.LatLng(lat, lng);
  
    var icon = $('#tiny-map').data("icon");
  
    var mapOptions = {
      center: center,
      zoom: maxZoom,
      panControl: false,
      streetViewControl: false,
      zoomControlOptions: {
          style: google.maps.ZoomControlStyle.SMALL
      },            
      mapTypeControl: false,
      minZoom: minZoom, 
      maxZoom: maxZoom,
      mapTypeId: google.maps.MapTypeId.ROADMAP  
    };
    
    var tinyMap = new google.maps.Map(document.getElementById('tiny-map'), mapOptions);
    
    var styles = [
      {"featureType":"poi","stylers":[{"saturation":-10},{"lightness":10},{"visibility":"simplified"}]},
      {"featureType": "poi","elementType": "labels","stylers": [{ "visibility": "off" }]},
      {"featureType":"transit","stylers":[{"visibility":"off"}]},
      {"featureType":"administrative.province","stylers":[{"visibility":"off"}]}
    ];
  
    tinyMap.setOptions({styles: styles});
  
    var mapIcon = {
      url: icon,
      size: new google.maps.Size(96,96),
      origin: new google.maps.Point(0,0),
      scaledSize: new google.maps.Size(48,48),
      anchor: new google.maps.Point(24,24)    
    };
    
    var marker = new google.maps.Marker({
      position: center,
      map: tinyMap,
      icon: mapIcon
    });
  });
}
