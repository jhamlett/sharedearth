import math


def toRadians(angle):
    return angle * (math.pi / 180)


def haversine_distance(lat1, lon1, lat2, lon2):
    lat1 = toRadians(lat1)
    lon1 = toRadians(lon1)
    lat2 = toRadians(lat2)
    lon2 = toRadians(lon2)
    
    latd = lat2 - lat1
    lond = lon2 - lon1
    a = math.sin(latd / 2) * math.sin(latd / 2) + math.cos(lat1) * math.cos(lat2) * \
        math.sin(lond / 2) * math.sin(lond / 2)
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
    return 6371.0 * c
