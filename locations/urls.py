from django.conf.urls import patterns, url
from locations import views

urlpatterns = patterns('',
    url(r'^add/', views.add, name='add'),
    url(r'^delete/', views.delete_location, name='delete_location'),
    url(r'^get/', views.get_locations, name='get_locations'),
    url(r'^card/', views.get_card),
    url(r'^card_list/', views.get_card_list),
    url(r'^profile/', views.get_profile),
    url(r'^change_field/', views.change_field),
    url(r'^json_list/', views.json_list),
    url(r'^json_matches_list/', views.json_matches_list),
)
