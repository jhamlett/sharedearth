from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from taggit.managers import TaggableManager
from taggit.models import TaggedItemBase


class TaggedToolsIHave(TaggedItemBase):
    content_object = models.ForeignKey("Location")


class TaggedToolsINeed(TaggedItemBase):
    content_object = models.ForeignKey("Location")


class Location(models.Model):
    user = models.ForeignKey(User)

    # These fields are required in order to create the location
    formatted_address = models.CharField(max_length=256)
    
    street_number = models.CharField(max_length=64, blank=True, null=True)
    street_name = models.CharField(max_length=64, blank=True, null=True)
    city = models.CharField(max_length=64, blank=True, null=True)
    state = models.CharField(max_length=32, blank=True, null=True)
    county = models.CharField(max_length=32, blank=True, null=True)
    zip = models.CharField(max_length=5, blank=True, null=True)
    zip_plus_four = models.CharField(max_length=4, blank=True, null=True)
    country = models.CharField(max_length=32, blank=True, null=True) 
    
    latitude = models.FloatField()
    longitude = models.FloatField()
    type = models.CharField(max_length=32)              # For example, "have" or "need"

    uuid = models.CharField(max_length=36, unique=True)
    date_added = models.DateTimeField(default=timezone.now)
    
    avatar_url = models.CharField(max_length=256)
    map_icon_url = models.CharField(max_length=256)
    
    search_distance = models.FloatField(default=5.0)    # Default radius of 5 miles
    email_matches = models.BooleanField(default=True)
    
    # These can be filled in later, or not. No big deal.
    name = models.CharField(max_length=128, default=None, blank=True, null=True)
    about = models.TextField(default=None, blank=True, null=True)
    
    tools_i_have = TaggableManager(related_name='tools_i_have', through=TaggedToolsIHave)
    tools_i_need = TaggableManager(related_name='tools_i_need', through=TaggedToolsINeed)

    DISTANCE_CHOICES = (
        (0, "0.5 miles"),
        (1, "1 mile"),
        (2, "2 miles"),
        (5, "5 miles"),
        (10, "10 miles"),
        (20, "20 miles"),
        (50, "50 miles"),
    )
    distance = models.IntegerField(choices=DISTANCE_CHOICES, default=20)
    
    GARDEN_SIZE_CHOICES = (
        (0, "< 50 sq ft"),
        (1, "50 - 250 sq ft"),
        (2, "250 - 500 sq ft"),
        (3, "500 - 1000 sq ft"),
        (4, "1000 - 1500 sq ft"),
        (5, "1 acre"),
        (6, "5 acres"),
        (7, "10 acres"),
        (8, "20 acres"),
        (9, "50 acres"),
        (10, "100+ acres"),
    )
    garden_size = models.IntegerField(choices=GARDEN_SIZE_CHOICES, default=2)
    
    COMPENSATION_CHOICES = (
        (0, "Free to use"),
        (1, "Rent for money"),
        (2, "Share your harvest"),
        (3, "Other arrangement"),                         
    )
    compensation = models.IntegerField(choices=COMPENSATION_CHOICES, default=3)
    
    GARDEN_STATUS_CHOICES = (
        (0, "Ready to plant"),
        (1, "Needs some work"),
        (2, "Isn&rsquo;t a garden yet"),
    )
    garden_status = models.IntegerField(choices=GARDEN_STATUS_CHOICES, default=1) 
    
    haversine_distance = 0
    
    class Meta:
        index_together = [
            ["latitude", "longitude"],
            ["latitude", "longitude", "type"],
        ]

# EOF
    