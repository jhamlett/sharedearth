from django.contrib.staticfiles.templatetags import staticfiles
from operator import itemgetter
from locations.models import Location
from datetime import datetime
import pytz
import time
from profiles.models import UserProfile
from tags.utilities import get_tag_field
from images.interfaces import get_image_list_by_location_as_list
import random
from django.db.models.query_utils import Q
from profiles.tasks import on_new_location


def state_to_abbreviation(state):
    mapping = {
        'Alaska': 'AK',
        'Alabama': 'AL',
        'Arkansas': 'AR',
        'American Samoa': 'AS',
        'Arizona': 'AZ',
        'California': 'CA',
        'Colorado': 'CO',
        'Connecticut': 'CT',
        'District of Columbia': 'DC',
        'Delaware': 'DE',
        'Florida': 'FL',
        'Georgia': 'GA',
        'Guam': 'GU',
        'Hawaii': 'HI',
        'Iowa': 'IA',
        'Idaho': 'ID',
        'Illinois': 'IL',
        'Indiana': 'IN',
        'Kansas': 'KS',
        'Kentucky': 'KY',
        'Louisiana': 'LA',
        'Massachusetts': 'MA',
        'Maryland': 'MD',
        'Maine': 'ME',
        'Michigan': 'MI',
        'Minnesota': 'MN',
        'Missouri': 'MO',
        'Northern Mariana Islands': 'MP',
        'Mississippi': 'MS',
        'Montana': 'MT',
        'National': 'NA',
        'North Carolina': 'NC',
        'North Dakota': 'ND',
        'Nebraska': 'NE',
        'New Hampshire': 'NH',
        'New Jersey': 'NJ',
        'New Mexico': 'NM',
        'Nevada': 'NV',
        'New York': 'NY',
        'Ohio': 'OH',
        'Oklahoma': 'OK',
        'Oregon': 'OR',
        'Pennsylvania': 'PA',
        'Puerto Rico': 'PR',
        'Rhode Island': 'RI',
        'South Carolina': 'SC',
        'South Dakota': 'SD',
        'Tennessee': 'TN',
        'Texas': 'TX',
        'Utah': 'UT',
        'Virginia': 'VA',
        'Virgin Islands': 'VI',
        'Vermont': 'VT',
        'Washington': 'WA',
        'Wisconsin': 'WI',
        'West Virginia': 'WV',
        'Wyoming': 'WY',    
    }
    
    return mapping[state] if state in mapping else state


def obfuscate_lat(lat):
    return round(lat, 3) + 2 * (random.random() - 0.5) / 1000

    
def obfuscate_lng(lng):
    return round(lng, 3) + 2 * (random.random() - 0.5) / 1000


def get_session_locations(s, selected):
    savedLocations = s.get("locations")
    gardenAvatar = staticfiles.static('profiles/img/avi-garden-square.png')
    gardenerAvatar = staticfiles.static('profiles/img/avi-gardener-square.png')

    gardenMapIcon = staticfiles.static('profiles/img/have-land-96.png')
    gardenerMapIcon = staticfiles.static('profiles/img/need-land-96.png')
    
    selIcon = staticfiles.static('locations/img/icon-sel.png')
    
    locations = []
    
    if savedLocations:
        for key in savedLocations:
            loc = savedLocations[key]
            
            line1 = [loc.get('street_number', ''), loc.get('street_name', '')]
            line2 = [loc.get('city', ''), state_to_abbreviation(loc.get('state', ''))]
            
            line1 = [x for x in line1 if (x != '' and x is not None)]
            line2 = [x for x in line2 if (x != '' and x is not None)]

            line1 = " ".join(line1)
            line2 = ", ".join(line2)
            
            if line1 is None or len(line1) == 0:
                line1 = loc.get('city', '')
                line2 = loc.get('state', '')
                if line1 is None:
                    line1 = ''
                if line2 is None:
                    line2 = ''
            
            location = {}
            if len(line1):
                location['address1'] = line1
            location['address2'] = line2
            location['avatar_url'] = gardenAvatar if loc['type'] == 'have' else gardenerAvatar
            location['lat'] = loc['latitude']
            location['lng'] = loc['longitude']
            location['id'] = key
            location['type'] = loc['type']
            location['date'] = loc['date_added']
            location['active'] = (selected == key)
            location['name'] = ''  # "This is you"  # location['address1'] if 'address1' in location else location['address2']
            location['formatted_address'] = loc['formatted_address'] 
            location['map_icon_url'] = gardenMapIcon if loc['type'] == 'have' else gardenerMapIcon
            location['map_sel_icon_url'] = selIcon
            
            locations.append(location)

    locations = sorted(locations, key=itemgetter('date'), reverse=True)
    return locations    


# Copypasta due to 2/27 deadline - fix later
def get_db_locations(request, selected):
    locs = Location.objects.filter(user_id=request.user.id).order_by('-date_added')

    selIcon = staticfiles.static('locations/img/icon-sel.png')

    locations = []

    for loc in locs:
            
        line1 = [loc.street_number, loc.street_name]
        line2 = [loc.city, state_to_abbreviation(loc.state)]
        
        line1 = [x for x in line1 if (x != '' and x is not None)]
        line2 = [x for x in line2 if (x != '' and x is not None)]

        line1 = " ".join(line1)
        line2 = ", ".join(line2)
        
        if line1 is None or len(line1) == 0:
            line1 = loc.city
            line2 = loc.state
            if line1 is None:
                line1 = ''
            if line2 is None:
                line2 = ''
        
        location = {}
        if len(line1):
            location['address1'] = line1
        location['address2'] = line2
        location['avatar_url'] = loc.avatar_url
        location['lat'] = loc.latitude
        location['lng'] = loc.longitude
        location['id'] = loc.uuid
        location['type'] = loc.type
        location['date'] = int(time.mktime(loc.date_added.timetuple()) * 1000)
        location['active'] = (selected == loc.uuid)
        location['name'] = loc.name
        location['formatted_address'] = loc.formatted_address.replace(', USA', '')
        location['map_icon_url'] = loc.map_icon_url
        location['map_sel_icon_url'] = selIcon
        location['about'] = loc.about
        location['num_id'] = loc.id
        location['user_about'] = UserProfile.objects.get(user=loc.user).about
        location['distance'] = str(loc.distance)
        location['distance_miles'] = 0.5 if str(loc.distance) == "0" else int(str(loc.distance))
        location['garden_status'] = str(loc.garden_status)
        location['garden_size'] = str(loc.garden_size)
        location['compensation'] = str(loc.compensation)

        # Perform a few additional queries here         
        location['tools_i_have'] = list(get_tag_field(request.user, loc.uuid, 'tools-i-have').names())
        location['tools_i_need'] = list(get_tag_field(request.user, loc.uuid, 'tools-i-need').names())
        location["images"] = get_image_list_by_location_as_list(loc.uuid)
                
        locations.append(location)

    return locations


def get_location_list(request, selected=None):
    
    if request.user.is_authenticated():
        session_locations = get_session_locations(request.session, selected)
        
        # We need to merge these into their database locations, because they've
        # probably just created a new user
        if len(session_locations):
            merge_locations(request)

        locations = get_db_locations(request, selected)
    else:
        locations = get_session_locations(request.session, selected)
        
    return locations

    
def session_to_db_location(request, loc):
    gardenAvatar = staticfiles.static('profiles/img/avi-garden-square.png')
    gardenerAvatar = staticfiles.static('profiles/img/avi-gardener-square.png')
    gardenMapIcon = staticfiles.static('profiles/img/have-land-96.png')
    gardenerMapIcon = staticfiles.static('profiles/img/need-land-96.png')

    location = Location()
    location.user = request.user   
                                       
    # These fields are required in orde
    location.formatted_address = loc['formatted_address']
                                       
    location.street_number = loc['street_number']
    location.street_name = loc['street_name']
    location.city = loc['city']
    location.state = loc['state']
    location.county = loc['county']
    location.zip = loc['zip']
    location.zip_plus_four = loc['zip_plus_four']
    location.country = loc['country']
                          
    location.latitude = loc['latitude']
    location.longitude = loc['longitude'] 
    location.type = loc['type'] 
                          
    location.uuid = loc['id'] 
    
    # See https://developers.google.com/maps/documentation/timezone/
    
    utc_dt = datetime.utcfromtimestamp(loc['date_added']).replace(tzinfo=pytz.utc) 
    location.date_added = utc_dt
                          
    location.avatar_url = gardenAvatar if loc['type'] == 'have' else gardenerAvatar 
    location.map_icon_url = gardenMapIcon if loc['type'] == 'have' else gardenerMapIcon 

    return location

        
def merge_locations(request):
    session_locations = request.session.get('locations')

    if isinstance(session_locations, dict) and len(session_locations):
        
        locations = []
        
        for key in session_locations:
            loc = session_locations[key]
            locations.append(session_to_db_location(request, loc))                   
        
        # Now save all these locations
        
        for location in locations:
            location.save()
            on_new_location(location)
        
        del request.session['locations']


def filter_locations(d):
    min_lat = float(d['min_lat'])
    max_lat = float(d['max_lat'])
    min_lng = float(d['min_lng'])
    max_lng = float(d['max_lng'])
    
    locations = Location.objects.filter(latitude__gte=min_lat).filter(latitude__lte=max_lat).filter(longitude__gte=min_lng).filter(longitude__lte=max_lng)

    query = None
    
    if "type" in d:
        query = Q(type=d["type"])
    if "have_tools[]" in d:
        tag_list = d.getlist('have_tools[]')
        nq = Q(tools_i_have__name__in=tag_list)
        
        if query is None:
            query = nq
        else:
            query |= nq
            
    if query is not None:
        locations = locations.filter(query).distinct()

    return locations

# EOF
