function editLocation ( id )
{
  selectLocation(id);
}

function deleteLocation ( id )
{
  selectLocation(id);
  var loc = $('#location-' + id);

  // Confirmation dialog first
  doModal("delete-confirm", { id: id, location: loc.data('formatted-address')});
}

function confirmDeleteLocation ( id )
{
  $.post('/locations/delete/', {id:id}, function(r)
  {
    closeModal();
    
    if ( r.success )
    {
      // The location has been deleted successfully. We need to delete it from
      // our list, remove its markers from our map, and make the first
      // element in the list the active one.
      
      destroyLocationIcon(id);
      $('#location-' + id).remove();
          
      var locs = $('li.location');
      
      if ( locs.length )
      {
        selectLocation(locs.first().data('id'));
      }
      else
      {
        $('#locations-list').addClass('hidden');
      }
    }
  });
}

function selectLocation ( id )
{
  var oldId = getSelectedLocationID();

  var loc = $('#location-' + id);
  selectSearchOption(loc.data("type"));

  $('li.location').removeClass('active');
  loc.addClass('active');
  
  selectMapLocation(id);

  changeProfile(id);
  
  // The active tab needs to be Map, List, or About My Place.
  if ( $('#tabs-right .selected').data("about-my-location") != 1 )
  {
    $('#map-tab').trigger('click');
  }  

  
  //history.replaceState(history.state, "", "/places/" + id);
  updateUrl();
}

function editProfile ( )
{
  $('#tab-about').trigger('click');
}

function getSelectedLocationID ( )
{
  //return $('li.location.active').first().data('id');
  return mainScope.activePlaceId;
}

function setSelectedLocationAvatar ( url )
{
  $('li.location.active>div>img').attr('src', url);
}

function setSelectedLocationMapIcon ( url )
{
  changeLocationIcon(getSelectedLocationID(), url);
}