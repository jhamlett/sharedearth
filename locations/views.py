from django.http.response import JsonResponse, HttpResponseForbidden
import shortuuid
import time
from django.template.loader import render_to_string
from django.template.context import RequestContext
from locations.interface import get_location_list, session_to_db_location,\
    filter_locations, state_to_abbreviation, obfuscate_lat, obfuscate_lng
from locations.models import Location
from django.shortcuts import get_object_or_404
import math
from profiles.models import UserProfile
from sharedearth.decorators import ensure_user_authorized
from images.interfaces import get_image_list_by_location
from django.forms.models import model_to_dict
import json
from django.views.decorators.csrf import ensure_csrf_cookie
from django.views.decorators.cache import never_cache
from django.views.decorators.http import require_POST
from profiles.operations import get_display_name
import sys
from sharedearth.custom_responses import HttpResponseNoContent
from profiles.tasks import on_new_location
from locations.utils import haversine_distance


# Add a location
@require_POST
def add(request):

    # Create a new location

    try:
        p = json.loads(request.body.decode())
    except:
        p = request.POST
    
    location = {}
    
    # Guaranteed components
    locid = shortuuid.uuid()
    location['id'] = locid
    location['formatted_address'] = p['formatted_address']
    location["latitude"] = p["latitude"]
    location["longitude"] = p["longitude"]
    location["date_added"] = int(time.time())
    location["type"] = p["type"]
    
    # Optional components
    location['street_number'] = p.get('street_number')
    location['street_name'] = p.get('route')
    if location['street_name'] is None:
        location['street_name'] = p.get('intersection')
    location['city'] = p.get('locality')
    location['state'] = p.get('administrative_area_level_1')
    location['county'] = p.get('administrative_area_level_2')
    location['zip'] = p.get('postal_code')
    location['zip_plus_four'] = p.get('postal_code_suffix')
    location['country'] = p.get('country')

    if request.user.is_authenticated():
        location = session_to_db_location(request, location)
        location.save()
        on_new_location(location)
    else:
        s = request.session
        locations = s.get("locations")
        
        # Create a new locations list if necessary
        if not (type(locations) is dict):
            locations = {}

        # Add it to the list and save it ack to the cookie 
        locations[locid] = location 
        s["locations"] = locations
        
    return JsonResponse({'id': locid, 'places': get_location_list(request)})


# Get the sidebar of all the locations as JSON
def json_list(request):
    data = get_location_list(request)
    return JsonResponse(data, safe=False)
    
        
@ensure_user_authorized
def json_matches_list(request):
    locations = filter_locations(request.GET)
    locations = locations.exclude(user_id=request.user.id)
    
    selected_location = Location.objects.get(uuid=request.GET['selected_location'])
    
    locs = []
    lat2 = selected_location.latitude
    lon2 = selected_location.longitude
    
    for location in locations:
        lat1 = location.latitude
        lon1 = location.longitude
        
        loc = model_to_dict(location)
        
        loc['haversine_distance'] = haversine_distance(lat1, lon1, lat2, lon2) * 0.621371
        loc['hdfmt'] = ("%.1f miles" if loc['haversine_distance'] >= 1 else "%.2f miles") % loc['haversine_distance'] if loc['haversine_distance'] >= 0.1 else "< 0.1 miles"   
        loc['user_id'] = location.user.id
        loc['user_display_name'] = get_display_name(request.user, location.user)
        loc['user_avatar_url'] = location.user.userprofile.avatar_url
        
        # Probably should have a whitelist here instead.
        del loc['tools_i_have']
        del loc['tools_i_need']
        del loc['date_added']
        del loc['street_number']
        del loc['zip_plus_four']
        del loc['email_matches']
        del loc['street_name']
        del loc['formatted_address']
        del loc['country']
        del loc['zip']
        del loc['county']
        
        if loc['name'] and len(loc['name']):
            loc['display_name'] = loc['name']
        else:
            loc['display_name'] = loc['user_display_name']
             
        locs.append(loc)

    locs = sorted(locs, key=lambda k: k['haversine_distance'])
    # locs = locs[:200]

    response = {}
    response["locations"] = locs
    response["type"] = request.GET.get('type')
    response['tools'] = "have_tools" in request.GET
    
    # print(response)
    
    return JsonResponse(response)
    
   
def delete_location(request):
    if request.method != 'POST':
        return HttpResponseForbidden()
    
    p = json.loads(request.body.decode())
    locationID = p['id']
         
    if request.user.is_authenticated():
        get_object_or_404(Location, user_id=request.user.id, uuid=locationID).delete()
        return JsonResponse({'places': get_location_list(request)})
    else:
        s = request.session
        locations = s.get("locations")
        
        # Create a new locations list if necessary
        if not (type(locations) is dict):
            locations = {}
            return HttpResponseForbidden()
            
        if locationID in locations:
            del locations[locationID]
            s["locations"] = locations
            return JsonResponse({'places': get_location_list(request)})
        else:
            return HttpResponseForbidden()


def get_locations(request):
    
    print(request.GET)
    locations = filter_locations(request.GET)

    # Exclude current user
    if request.user.is_authenticated():
        locations = locations.exclude(user_id=request.user.id)
    
    response = []
    for location in locations: 
        response.append([
            obfuscate_lat(location.latitude),
            obfuscate_lng(location.longitude),
            location.id,
            location.type,
            location.map_icon_url
        ])

    return JsonResponse({"locations": response}) 


def get_card(request):
    d = request.GET
    
    # TODO - return an error for users not logged in 
    if not request.user.is_authenticated():
        return HttpResponseForbidden()
    
    context = RequestContext(request)
    userLocation = Location.objects.get(uuid=d['loc_uuid'])
    lat1 = userLocation.latitude
    lng1 = userLocation.longitude
    otherLocation = Location.objects.get(id=d['get_id'])   
    lat2 = otherLocation.latitude
    lng2 = otherLocation.longitude
    
    context['back'] = d.get('back')
    
    context['location'] = otherLocation
    distance = haversine_distance(lat1, lng1, lat2, lng2) * 0.621371
    distance = "%.2f miles" % distance
    context['distance'] = distance
    
    # Tags per location
    context['tools_i_have'] = otherLocation.tools_i_have.names()
    context['tools_i_need'] = otherLocation.tools_i_need.names()
    
    # Tags per user
    profile = UserProfile.objects.get(user=otherLocation.user)
    context['what_i_like_to_grow'] = profile.what_i_grow.names()
    context['interests'] = profile.interests.names()
    
    context['user_display_name'] = get_display_name(request.user, otherLocation.user)

    return JsonResponse({
        'success': 1,
        'content': render_to_string('locations/location_card.html', context)
    })
    
    
def get_card_list(request):
    d = request.GET
    
    # TODO - return an error for users not logged in 
    if not request.user.is_authenticated():
        return HttpResponseForbidden()
    
    context = RequestContext(request)
    context['locations'] = Location.objects.filter(id__in=d.getlist('get_id[]'))
    
    for location in context['locations']:
        location.user_display_name = get_display_name(request.user, location.user) 

    return JsonResponse({
        'success': 1,
        'content': render_to_string('locations/location_card_list.html', context)
    })
    
    
def find_closest_location(this_user, other_user):
    closest_distance = sys.float_info.max
    closest_location = None
    
    # If this user has no locations, just return the first one
    if len(this_user) == 0:
        return other_user[0]
    
    for l1 in this_user:
        for l2 in other_user:
            distance = haversine_distance(l1.latitude, l1.longitude, l2.latitude, l2.longitude)
            
            if distance < closest_distance:
                closest_distance = distance
                closest_location = l2
    
    return closest_location
    

def get_profile(request):
    d = request.GET
    
    # TODO - return an error for users not logged in 
    if not request.user.is_authenticated():
        return HttpResponseForbidden()
    
    context = RequestContext(request)
    
    loc_id = int(d['id'])
    
    if loc_id > 0:
        location = Location.objects.get(id=loc_id)
    else:
        # Find the closest location to the current user
        current_user_locations = Location.objects.filter(user=request.user)
        other_user_locations = Location.objects.filter(user__id=-loc_id)
        location = find_closest_location(current_user_locations, other_user_locations)
        
        # TODO: Better response if the other user HAS no locations....
        if location is None:
            return HttpResponseNoContent()
    
    loc_id = location.id
    location.latitude = obfuscate_lat(location.latitude)
    location.longitude = obfuscate_lng(location.longitude)  
    context['location'] = location
    context['uuid'] = location.uuid
    
    if location.city is not None:
        context['city_state'] = location.city + ", " + state_to_abbreviation(location.state)
    else:
        context['city_state'] = location.state
        
    context["images"] = get_image_list_by_location(location.uuid)
        
    # Tags per location
    context['tools_i_have'] = location.tools_i_have.names()
    context['tools_i_need'] = location.tools_i_need.names()
    
    # Tags per user
    profile = UserProfile.objects.get(user=location.user)
    context['self'] = location.user == request.user
    context['profile'] = profile
    context['what_i_like_to_grow'] = profile.what_i_grow.names()
    context['interests'] = profile.interests.names()   
    
    context['other_gardens'] = Location.objects.filter(user_id=location.user_id, type='have').exclude(id=loc_id)
    context['other_gardeners'] = Location.objects.filter(user_id=location.user_id, type='need').exclude(id=loc_id)
    
    context['user_display_name'] = get_display_name(request.user, location.user)
         
    return JsonResponse({'response': [{
        'what': '#profile',
        'action': 'html',
        'content': render_to_string('locations/location_profile.html', context)
    }]})


@ensure_csrf_cookie
@never_cache
def change_field(request):
    # May want to verify in a different way.
    if not request.user.is_authenticated():
        return HttpResponseForbidden()
    
    d = json.loads(request.body.decode())
    loc_id = d['location_id']
    field_name = d['field_name']
    if d['value'] == None:
        d['value'] = ''
    value = d['value'].strip()  # Remove whitespace

    location = Location.objects.get(id=loc_id, user_id=request.user.id)
    
    # It's farily important to have a whitelist of fields that can be updated.
    if location and field_name in ['name', 'about', 'distance', 'garden_size', 'compensation', 'status']:
        setattr(location, field_name, value)
        location.save(update_fields=[field_name])
        return JsonResponse({'success': 1})
    else:
        return JsonResponse({'success': 0})
    
# EOF
