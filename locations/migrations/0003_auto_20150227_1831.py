# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('locations', '0002_auto_20150227_1333'),
    ]

    operations = [
        migrations.AlterField(
            model_name='location',
            name='date_added',
            field=models.DateField(default=django.utils.timezone.now),
            preserve_default=True,
        ),
    ]
