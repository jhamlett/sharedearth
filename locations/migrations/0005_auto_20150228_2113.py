# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('locations', '0004_auto_20150228_1700'),
    ]

    operations = [
        migrations.AlterField(
            model_name='location',
            name='date_added',
            field=models.DateTimeField(default=django.utils.timezone.now),
            preserve_default=True,
        ),
    ]
