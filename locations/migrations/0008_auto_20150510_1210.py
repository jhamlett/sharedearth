# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('locations', '0007_location_distance'),
    ]

    operations = [
        migrations.AddField(
            model_name='location',
            name='compensation',
            field=models.IntegerField(default=2, choices=[(0, 'Free to use'), (1, 'Rent for money'), (2, 'Share your harvest'), (3, 'Other arrangement')]),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='location',
            name='garden_size',
            field=models.IntegerField(default=2, choices=[(0, '< 50 sq ft'), (1, '50 - 250 sq ft'), (2, '250 - 500 sq ft'), (3, '500 - 1000 sq ft'), (4, '1000 - 1500 sq ft'), (5, '1 acre'), (6, '5 acres'), (7, '10 acres'), (8, '20 acres'), (9, '50 acres'), (10, '100+ acres')]),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='location',
            name='garden_status',
            field=models.IntegerField(default=1, choices=[(0, 'Ready to plant'), (1, 'Needs some work'), (2, 'Isn&rsquo;t a garden yet')]),
            preserve_default=True,
        ),
    ]
