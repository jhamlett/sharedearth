# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.utils.timezone import utc
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('locations', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='location',
            name='address',
        ),
        migrations.RemoveField(
            model_name='location',
            name='experience',
        ),
        migrations.AddField(
            model_name='location',
            name='avatar_url',
            field=models.CharField(max_length=256, default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='location',
            name='city',
            field=models.CharField(max_length=64, default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='location',
            name='country',
            field=models.CharField(max_length=32, default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='location',
            name='county',
            field=models.CharField(max_length=32, default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='location',
            name='date_added',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 19, 33, 27, 562879, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='location',
            name='formatted_address',
            field=models.CharField(max_length=256, default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='location',
            name='map_icon_url',
            field=models.CharField(max_length=256, default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='location',
            name='state',
            field=models.CharField(max_length=32, default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='location',
            name='street_name',
            field=models.CharField(max_length=64, default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='location',
            name='street_number',
            field=models.CharField(max_length=64, default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='location',
            name='uuid',
            field=models.CharField(max_length=36, default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='location',
            name='zip',
            field=models.CharField(max_length=5, default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='location',
            name='zip_plus_four',
            field=models.CharField(max_length=4, default=''),
            preserve_default=False,
        ),
    ]
