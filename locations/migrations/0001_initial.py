# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Location',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('address', models.CharField(max_length=128)),
                ('latitude', models.FloatField()),
                ('longitude', models.FloatField()),
                ('type', models.CharField(max_length=32)),
                ('search_distance', models.FloatField(default=5.0)),
                ('email_matches', models.BooleanField(default=True)),
                ('name', models.CharField(max_length=128, default=None, blank=True, null=True)),
                ('about', models.TextField(default=None, blank=True, null=True)),
                ('experience', models.TextField(default=None, blank=True, null=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterIndexTogether(
            name='location',
            index_together=set([('latitude', 'longitude'), ('latitude', 'longitude', 'type')]),
        ),
    ]
