# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('locations', '0010_auto_20150522_1339'),
    ]

    operations = [
        migrations.AlterField(
            model_name='location',
            name='uuid',
            field=models.CharField(unique=True, max_length=36),
            preserve_default=True,
        ),
    ]
