# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('locations', '0008_auto_20150510_1210'),
    ]

    operations = [
        migrations.AlterField(
            model_name='location',
            name='compensation',
            field=models.IntegerField(default=3, choices=[(0, 'Free to use'), (1, 'Rent for money'), (2, 'Share your harvest'), (3, 'Other arrangement')]),
            preserve_default=True,
        ),
    ]
