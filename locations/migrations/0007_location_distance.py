# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('locations', '0006_auto_20150304_1253'),
    ]

    operations = [
        migrations.AddField(
            model_name='location',
            name='distance',
            field=models.IntegerField(choices=[(0, '0.5 miles'), (1, '1 mile'), (2, '2 miles'), (5, '5 miles'), (10, '10 miles'), (20, '20 miles'), (50, '50 miles')], default=5),
            preserve_default=True,
        ),
    ]
