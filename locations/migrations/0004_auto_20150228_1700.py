# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('locations', '0003_auto_20150227_1831'),
    ]

    operations = [
        migrations.AlterField(
            model_name='location',
            name='city',
            field=models.CharField(blank=True, null=True, max_length=64),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='location',
            name='country',
            field=models.CharField(blank=True, null=True, max_length=32),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='location',
            name='county',
            field=models.CharField(blank=True, null=True, max_length=32),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='location',
            name='state',
            field=models.CharField(blank=True, null=True, max_length=32),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='location',
            name='street_name',
            field=models.CharField(blank=True, null=True, max_length=64),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='location',
            name='street_number',
            field=models.CharField(blank=True, null=True, max_length=64),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='location',
            name='zip',
            field=models.CharField(blank=True, null=True, max_length=5),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='location',
            name='zip_plus_four',
            field=models.CharField(blank=True, null=True, max_length=4),
            preserve_default=True,
        ),
    ]
