# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import taggit.managers


class Migration(migrations.Migration):

    dependencies = [
        ('taggit', '0001_initial'),
        ('locations', '0005_auto_20150228_2113'),
    ]

    operations = [
        migrations.CreateModel(
            name='TaggedToolsIHave',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('content_object', models.ForeignKey(to='locations.Location')),
                ('tag', models.ForeignKey(related_name='locations_taggedtoolsihave_items', to='taggit.Tag')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TaggedToolsINeed',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('content_object', models.ForeignKey(to='locations.Location')),
                ('tag', models.ForeignKey(related_name='locations_taggedtoolsineed_items', to='taggit.Tag')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='location',
            name='tools_i_have',
            field=taggit.managers.TaggableManager(verbose_name='Tags', through='locations.TaggedToolsIHave', help_text='A comma-separated list of tags.', to='taggit.Tag'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='location',
            name='tools_i_need',
            field=taggit.managers.TaggableManager(verbose_name='Tags', through='locations.TaggedToolsINeed', help_text='A comma-separated list of tags.', to='taggit.Tag'),
            preserve_default=True,
        ),
    ]
