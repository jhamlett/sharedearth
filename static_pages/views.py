from django.template.response import TemplateResponse


# Create your views here.
def privacy_policy(request):
    return TemplateResponse(request, 'static_pages/privacy_policy.html')
