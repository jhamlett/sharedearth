call venv\Scripts\activate.bat
set DEBUG_SHAREDEARTH=

call heroku maintenance:on --app sharedearth-staging
call heroku ps:scale --app sharedearth-staging web=0

git add --all .
@if %errorlevel% neq 0 exit /b %errorlevel%

git commit -m "Automatic commit before heroku deployment"
@if %errorlevel% neq 0 exit /b %errorlevel%

git push bitbucket master
@if %errorlevel% neq 0 exit /b %errorlevel%

git push heroku-staging master
@if %errorlevel% neq 0 exit /b %errorlevel%

call heroku run --app sharedearth-staging python manage.py migrate

call setenv.bat

call heroku ps:scale --app sharedearth-staging web=1
call heroku maintenance:off --app sharedearth-staging