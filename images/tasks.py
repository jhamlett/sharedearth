from sharedearth import settings, celery_app
from PIL import Image as PilImage, ImageOps
import requests
import boto
from io import BytesIO
import os
from images.models import Image
import shortuuid


master_uuid = ('sharedearth.com')


def get_user_uuid_from_uid(user_id):
    return str(shortuuid.uuid(name=master_uuid + str(user_id)))


def create_map_image_internal(user_id, image_url, uuid):
    # Find our reference circle
    circleFilename = os.path.join(os.path.dirname(os.path.realpath(__file__)), "reference_images", "circle.png")
    circle = PilImage.open(circleFilename).convert("L")
    
    # Retrieve our source image from a URL
    r = requests.get(image_url)
    im = PilImage.open(BytesIO(r.content))
    
    (width, height) = im.size
    (cwidth, cheight) = circle.size
    
    # Resize the image
    if cwidth != width or cheight != height:
        circle = circle.resize((width, height), PilImage.ANTIALIAS)
    
    output = ImageOps.fit(im, circle.size, centering=(0.5, 0.5))
    output.putalpha(circle)
    
    # Resize to 96. Make it flawless.
    output = output.resize((96, 96), PilImage.ANTIALIAS)
    
    # Save the image into a BytesIO() object to avoid writing to disk
    out_im2 = BytesIO()
    # Specify the file type since there is no file name to discern it from
    output.save(out_im2, 'PNG')
    
    # Connect to our s3 bucket and upload from memory
    conn = boto.connect_s3(settings.UPLOADER_AWS_ACCESS_KEY, settings.UPLOADER_AWS_SECRET_KEY)
    
    # Connect to bucket and create key
    b = conn.get_bucket(settings.UPLOADER_S3_BUCKET)
    
    user_uuid = get_user_uuid_from_uid(user_id)
    fuid = uuid
    suffix = "map"
    extension = "png"
    
    object_name = 'm/' + user_uuid + "/" + fuid + "." + suffix + "." + extension

    k = b.new_key(object_name)
    
    # Note we're setting contents from the in-memory string provided by cStringIO
    k.set_contents_from_string(out_im2.getvalue(), headers={"Content-Type": "image/png"})
    
    return 'https://%s.s3.amazonaws.com/%s' % (settings.UPLOADER_S3_BUCKET, object_name)


@celery_app.task()
def create_map_image_async(user_id, image_id, uuid):
    image = Image.objects.get(id=image_id)
    image_url = image.url_avatar
    image.url_map = create_map_image_internal(user_id, image_url, uuid)
    image.save(update_fields=['url_map'])
    

def create_map_image(user_id, image_id, uuid):
    create_map_image_async.apply_async(args=[user_id, image_id, uuid])

# EOF
