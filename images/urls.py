from django.conf.urls import patterns, url
from images import views

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'sharedearth.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^request/', views.request),
    url(r'^sign/', views.sign),
    url(r'^complete/', views.complete),
    url(r'^delete/', views.delete),
    url(r'^avatar/', views.set_avatar),
    url(r'^render_list/', views.render_list),
)
