from images.models import Image
from django.forms.models import model_to_dict


# Extend the attributes appropriately
def force_all_image_sizes(image_list):
    for image in image_list:
        if len(image.url_medium) == 0:
            image.url_medium = image.url_avatar
        if len(image.url_large) == 0:
            image.url_large = image.url_medium 
    return image_list


# Retrieve all images for a user
def get_image_list_by_user(user_id, ensure_all_image_sizes=True, max_results=50, start_at=0):
    image_list = Image.objects.filter(location__user=user_id).order_by('-created_at')[start_at:(max_results + start_at)]
    if ensure_all_image_sizes:
        image_list = force_all_image_sizes(image_list)
    return image_list

    
# Retrieve all images for a location
def get_image_list_by_location(location_uuid, ensure_all_image_sizes=True, max_results=50, start_at=0):
    image_list = Image.objects.filter(location__uuid=location_uuid).order_by('-created_at')[start_at:(max_results + start_at)]
    if ensure_all_image_sizes:
        image_list = force_all_image_sizes(image_list)
    return image_list


# Retrieve all images for a location
def get_image_list_by_location_as_list(location_uuid, ensure_all_image_sizes=True, max_results=50, start_at=0):
    image_list = Image.objects.filter(location__uuid=location_uuid).order_by('-created_at')[start_at:(max_results + start_at)]
    if ensure_all_image_sizes:
        image_list = force_all_image_sizes(image_list)
        
    true_list = []
    
    for image in image_list:
        img = model_to_dict(image)
        true_list.append(img)
        
    return true_list
