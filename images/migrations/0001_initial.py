# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('locations', '0002_auto_20150227_1333'),
    ]

    operations = [
        migrations.CreateModel(
            name='Image',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('uuid', models.CharField(max_length=36)),
                ('url_large', models.CharField(max_length=256, default=None)),
                ('url_medium', models.CharField(max_length=256, default=None)),
                ('url_avatar', models.CharField(max_length=256, default=None)),
                ('url_map', models.CharField(max_length=256, default=None)),
                ('location', models.ForeignKey(to='locations.Location')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
