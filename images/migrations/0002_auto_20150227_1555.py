# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('images', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='image',
            name='url_avatar',
            field=models.CharField(blank=True, max_length=256),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='image',
            name='url_large',
            field=models.CharField(blank=True, max_length=256),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='image',
            name='url_map',
            field=models.CharField(blank=True, max_length=256),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='image',
            name='url_medium',
            field=models.CharField(blank=True, max_length=256),
            preserve_default=True,
        ),
    ]
