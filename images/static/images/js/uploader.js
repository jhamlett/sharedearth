// toBlob shim. See #61 at https://code.google.com/p/chromium/issues/detail?id=67587
var is_chrome = !! window.chrome
if (is_chrome) {
    // This does not work in Internet Explorer; it fails with an "access denied" error when opening the data URL
    HTMLCanvasElement.prototype.toBlob || Object.defineProperty(HTMLCanvasElement.prototype, 'toBlob', {
            value: function(callback, type, quality) {
                var xhr = new XMLHttpRequest;
                xhr.open('GET', this.toDataURL(type, quality));
                xhr.responseType = 'arraybuffer';
                xhr.onload = function(e) {
                    callback(new Blob([this.response], {
                                type: type || 'image/png'
                            }));
                }
                xhr.send();
            }
        });
} else if (!HTMLCanvasElement.prototype.toBlob) {
    Object.defineProperty(HTMLCanvasElement.prototype, 'toBlob', {
            value: function(callback, type, quality) {
                var bin = atob(this.toDataURL(type, quality).split(',')[1]),
                    len = bin.length,
                    len32 = len >> 2,
                    a8 = new Uint8Array(len),
                    a32 = new Uint32Array(a8.buffer, 0, len32);

                for (var i = 0, j = 0; i < len32; i++) {
                    a32[i] = bin.charCodeAt(j++) |
                        bin.charCodeAt(j++) << 8 |
                        bin.charCodeAt(j++) << 16 |
                        bin.charCodeAt(j++) << 24;
                }

                var tailLength = len & 3;

                while (tailLength--) {
                    a8[j] = bin.charCodeAt(j++);
                }

                callback(new Blob([a8], {
                            'type': type || 'image/png'
                        }));
            }
        });
}

function clip(value, min, max) {
  if (value < min) value = min;
  if (value > max) value = max;
  return value;
}

function getRotatedImageWidth(orientation, width, height) {
  return (orientation > 4) ? height : width;
}

function getRotatedImageHeight(orientation, width, height) {
  return (orientation > 4) ? width : height;
}

function rotateCanvas(canvas, context, orientation) {
  var angle = 0;

  switch (orientation) {
  // 1,2 -> 0
  case 3:
  case 4:
    angle = 180;
    break;
  case 5:
  case 6:
    angle = 90;
    break;
  case 7:
  case 8:
    angle = -90;
    break;
  }

  angle = angle * (Math.PI / 180);

  // Move registration point to the center of the canvas
  context.translate(canvas.width / 2, canvas.height / 2);

  // Rotate canvas
  context.rotate(angle);

    /* 
  // And mirror, if necessary. NOT TESTED!
  switch ( orientation )
  {
    case 2: context.scale(-1, 1); break;  // Flip horizontally
    case 4: context.scale(-1, 1); break;  // Flip horizontally
    case 5: context.scale(1, -1); break;  // Flip vertically
    case 7: context.scale(1, -1); break;  // Flip vertically
  }*/
}

function drawRotatedAndCentered(canvas, orientation, img, width, height) {
    var ctx = canvas.getContext("2d");

    rotateCanvas(canvas, ctx, orientation);
    ctx.drawImage(img, -width / 2, -height / 2, width, height);
}

function resizeImage(canvas, img, orientation, maxWidth, maxHeight) {
    var width = img.width;
    var height = img.height;

    if (width > height) {
        if (width > maxWidth) {
            height *= maxWidth / width;
            width = maxWidth;
        }
    } else if (height > maxHeight) {
        width *= maxHeight / height;
        height = maxHeight;
    }
    canvas.width = getRotatedImageWidth(orientation, width, height);
    canvas.height = getRotatedImageHeight(orientation, width, height);
    drawRotatedAndCentered(canvas, orientation, img, width, height);
}

function cropImage(canvas, img, orientation, maxWidth, maxHeight) {
    var width = img.width;
    var height = img.height;

    if (width < height) {
        height *= maxWidth / width;
        width = maxWidth;
    } else {
        width *= maxHeight / height;
        height = maxHeight;
    }

    canvas.width = getRotatedImageWidth(orientation, maxWidth, maxHeight);
    canvas.height = getRotatedImageHeight(orientation, maxWidth, maxHeight);
    drawRotatedAndCentered(canvas, orientation, img, width, height);
}


function dataURItoBlob(dataURI) {
    // convert base64 to raw binary data held in a string
    // doesn'saved_this handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
    var byteString = atob(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to an ArrayBuffer
    var ab = new ArrayBuffer(byteString.length);
    var dw = new DataView(ab);
    var len = byteString.length;
    for (var i = 0; i < len; ++i) {
        dw.setUint8(i, byteString.charCodeAt(i));
    }

    // write the ArrayBuffer to a blob, and you're done
    return new Blob([ab], {
            type: mimeString
        });
}

// Quality ranges from 0.0 to 1.0

function canvasToJpeg(canvas, quality) {
    quality = (typeof quality !== 'undefined') ? quality : 0.7;
    return canvas.toDataURL("image/jpeg", quality);
}

function resizeJpegImmediateToData(image, orientation, maxWidth, maxHeight, crop, quality) {
    var canvas = document.createElement('canvas');

    if (crop)
        cropImage(canvas, image, orientation, maxWidth, maxHeight);
    else
        resizeImage(canvas, image, orientation, maxWidth, maxHeight);

    return canvasToJpeg(canvas, quality);
}

function resizeJpegAsyncToBlob(callback, image, orientation, maxWidth, maxHeight, crop, quality) {
    var canvas = document.createElement('canvas');

    if (crop)
        cropImage(canvas, image, orientation, maxWidth, maxHeight);
    else
        resizeImage(canvas, image, orientation, maxWidth, maxHeight);

    canvas.toBlob(function(blob) {
            callback(blob);
        }, "image/jpeg", quality);
}

var globalImageId = 0;

function ImageUploader(imageList, file)
{
  this.id = globalImageId++;
  this.file = file;
  this.originalImage = new Image();
  this.images = [];
  this.bytesSent = [];
  this.totalBytes = 0;
  this.numImages = 0;
  this.finishCount = 0;
  this.thumbQuality = 0.7;
  this.imageQuality = 0.5;
  this.error = false;

  var saved_this = this;

  this.originalImage.onload = function()
  {
    EXIF.getData(saved_this.originalImage, function()
    {
      var orientation = EXIF.getTag(saved_this.originalImage, "Orientation");
      if (orientation === undefined) orientation = 1;
      saved_this.orientation = orientation;

      // Make a thumbnail immediately for displaying
      saved_this.img240 = resizeJpegImmediateToData(saved_this.originalImage, orientation, 240, 240, true, saved_this.thumbQuality);

      saved_this.li = $('<li>').attr('class', 'uploading');
      saved_this.a = $('<a>').attr('href', 'javascript:void(0);');
      saved_this.img = $('<img>').attr('src', saved_this.img240);
      saved_this.canvas = $('<canvas>').attr('width', '120').attr('height', '120');
      saved_this.div = $('<div>').attr('class', 'percent');
      saved_this.span = $('<span>');

      saved_this.a.prepend(saved_this.img);

      imageList.after(saved_this.li.append(saved_this.a).append(saved_this.canvas).append(saved_this.div.append(saved_this.span)));
      saved_this.updateProgress(0);

      setTimeout(function() {
        saved_this.uploadImages();
      }, 0);
    });
  };

  this.originalImage.src = window.URL.createObjectURL(file);
};

// This assumes a square canvas
ImageUploader.prototype = 
{
  clockProgress: function(canvas, percent) {
    percent = clip(percent, 0, 100);
    var c = canvas.getContext("2d");

    // Clear the canvas
    canvas.width = canvas.width;

    if (percent == 100) {
      // Nothing to draw
      return;
    }

    if (percent == 0) {
      // Fill entire rectangle
      c.fillRect(0, 0, canvas.width, canvas.height);
      return;
    }

    var radius = canvas.width / Math.sqrt(2);
    var cx = canvas.width / 2;

    c.translate(cx, cx);
    c.rotate(-Math.PI / 2);
    c.beginPath();
    c.moveTo(0, 0);
    c.arc(0, 0, radius, 2 * Math.PI * (percent / 100), 0);
    c.lineTo(radius, 0);
    c.lineTo(0, 0);
    c.fill();
  },

  textProgress: function(percentSpan, percent) {
      percent = clip(percent, 0, 100);
      percentSpan.html(percent + "%");
  },

  updateProgress: function(percent, message) {
      if (this.error) {
          this.clockProgress(this.canvas.get(0), 0);
          this.span.html("Upload failed");
          return;
      }

      this.clockProgress(this.canvas.get(0), percent);

      if (typeof message !== 'undefined')
          this.span.html(message);
      else
          this.textProgress(this.span, percent);
  },

  getPercent: function() {
    var totalBytes = this.totalBytes;
    var bytesSent = 0;
  
    for (var i = 0; i < this.bytesSent.length; ++i) {
      bytesSent += this.bytesSent[i];
    }
  
    return Math.round(100 * bytesSent / totalBytes);
  },

  uploadImage: function(maxDimension, crop, largest, quality) {
      var saved_this = this;
      var isLargest = largest;
      var saved_imageSize = maxDimension;
      ++saved_this.numImages;

      resizeJpegAsyncToBlob(function(blob) 
      {
        saved_this.images.push(blob);
        saved_this.totalBytes += blob.size;

        var blobSize = blob.size;
        var bytesSentIndex = saved_this.bytesSent.length;
        saved_this.bytesSent.push(0);

        var s3 = new S3Upload({
          s3_sign_put_url: '/images/sign/',
          s3_object_name: saved_this.filenameBase,
          s3_suffix: "" + maxDimension,
          onProgress: function(bytesSent, bytesTotal, percent, message) {
            if (bytesSent == -1) {
              bytesSent = blobSize;
            }

            if (bytesTotal == -1) {
              bytesTotal = blobSize;
            }

            if (saved_this.bytesSent.length == saved_this.numImages) {
              saved_this.bytesSent[bytesSentIndex] = bytesSent;
              saved_this.updateProgress(saved_this.getPercent());
            } else {
              saved_this.updateProgress(0, "Preparing");
            }
          },
          onFinishS3Put: function(url) {
            ++saved_this.finishCount;
            saved_this.bytesSent[bytesSentIndex] = blobSize;

            var id = saved_this.filenameBase;

            $.post('/images/complete/', {
              id: id,
              size: saved_imageSize,
              url: url
            }, function(r) {
              if (isLargest) {
                /*saved_this.a.attr('href', url);
                saved_this.a.data('id', r.id);
                saved_this.li.attr('id', 'image-' + r.id);
                saved_this.li.removeClass('uploading');
                makeFancy(saved_this.a, true);*/
                saved_this.li.remove();
                mainScope.uploadFinished(saved_this, r, url);
              }

              if (saved_this.finishCount == saved_this.numImages) {
                if (!saved_this.error) {
                  saved_this.updateProgress(100, "");
                  saved_this.canvas.remove();
                  saved_this.div.remove();
                }
              } else {
                saved_this.updateProgress(saved_this.getPercent());
              }
            }).fail(function() {
              saved_this.error = true;
              saved_this.updateProgress();
            });
          },
          onError: function(status) {
            saved_this.error = true;
            saved_this.updateProgress();
          }
      });

  s3.uploadBlob(blob);

    }, this.originalImage, this.orientation, maxDimension, maxDimension, crop, quality);
  },

  uploadImages: function() {
    var saved_this = this;
    saved_this.locationId = getSelectedLocationID();

    $.post('/images/request/', {
      location: saved_this.locationId
    }).done(function(data) {
      var largest = true;

      saved_this.filenameBase = data.filenameBase;
      if (saved_this.originalImage.width > 1200 || saved_this.originalImage.height > 1200) {
        saved_this.uploadImage(2400, false, true, saved_this.imageQuality);
        largest = false;
      }

      saved_this.uploadImage(1200, false, largest, saved_this.imageQuality);
      saved_this.uploadImage(240, true, false, saved_this.thumbQuality);
    }).fail(function(data) {
      saved_this.error = true;
      saved_this.updateProgress();
    });
  }
};

//
// Code to handle the initial upload events
//

$(document).on('dragover', function(e) {
        e.preventDefault();
        e = e.originalEvent;
        e.dataTransfer.dropEffect = 'copy';
    });

$(document).on('drop', function(e) {
        e.preventDefault();
        e = e.originalEvent;
        var target = e.dataTransfer || e.target;
        var files = target && target.files;

        if (files) {
            beginUploadImages(files);
        }
    });

function beginUploadImages(files) {
    for (var i = 0; i < files.length; ++i) {
        var file = files[i];
        if (file.type.match(/image.*/)) {
            new ImageUploader($('#imagelist li:eq(0)'), file);
        }
    }
}

function globalOnUpload() {
    var input = document.getElementById('file-input');
    beginUploadImages(input.files);
}