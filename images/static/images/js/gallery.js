
// Call with "what" = a jQuery node for an "a" tag.

function makeFancy ( what, editable )
{
	what.fancybox({
    'transitionIn'        : 'elastic',
    'transitionOut'	      :	'elastic',
    'titlePosition'       : 'inside',
    'easingIn'            : 'easeOutBack',
    'easingOut'           : 'easeInBack',
    'hideOnContentClick'  : true,
    'hideOnOverlayClick'  : true,
    'overlayColor'        :'#000000',
    'overlayOpacity'      : 0.85,
    'overlayShow'	        :	true
	});

  if ( editable )
  {
    what.each(function()
    {	
      storeTemplateInData('photo-menu', $(this), 'controls',
      {
        id:$(this).data('id')
      });
    });
  }
}

$(function() {
  $.extend($.easing,
  {
    easeInBack : function (x,t,b,c,d,s)
    {
      if ( s === undefined )
        s = 1.70158;
      return c*(t/=d)*t*((s+1)*t-s)+b;
    },
      
    easeOutBack : function (x,t,b,c,d,s)
    {
      if ( s === undefined )
        s = 1.70158;
      return c*((t=t/d-1)*t*((s+1)*t+s)+1)+b;
    }
  });
});


function changePhotosLocation ( id )
{
  dynamicGet('/images/render_list/', {'location_id':id});
}

function setProfilePicture ( id )
{
  $.post('/images/avatar/', {type: "profile", id: id}, function(response)
  {
    mainScope.setProfilePicture(response.url);
    //$('#user_avatar').attr('src', response.url);
    //$('#tiny_avatar').attr('src', response.url);
  });
  $.fancybox.close();
}

function setPlacePicture ( location_id, id )
{
  $.post('/images/avatar/', {type: "place", id: id, location: location_id}, function(response)
  {
    //setSelectedLocationAvatar(response.url);
    //setSelectedLocationMapIcon(response.map_url);
    mainScope.setPlacePicture(location_id, response.url, response.map_url);
  });
  $.fancybox.close();
}

function deletePicture ( location_id, id )
{
  $.post('/images/delete/', {id: id}, function(response)
  {
    //$('li#image-' + id).remove();
    mainScope.deletePicture(location_id, id);
  });
  $.fancybox.close();
}

