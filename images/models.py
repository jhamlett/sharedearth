from django.db import models
from locations.models import Location
from django.db.models.signals import pre_delete
from sharedearth import settings
import boto
from urllib.parse import urlparse
from django.utils import timezone
from django.contrib.auth.models import User


# An image associated with a particular location 
class Image(models.Model):
    user = models.ForeignKey(User)
    location = models.ForeignKey(Location, blank=True, null=True)
    uuid = models.CharField(max_length=36)
    created_at = models.DateTimeField(default=timezone.now)
    url_large = models.CharField(max_length=256, blank=True)
    url_medium = models.CharField(max_length=256, blank=True)
    url_avatar = models.CharField(max_length=256, blank=True)
    url_map = models.CharField(max_length=256, blank=True)


# An image the site no longer uses and which could be removed from S3
class ToDeleteFromS3(models.Model):
    bucket = models.CharField(max_length=256)
    key = models.CharField(max_length=256)

    
# Delete an image from Amazon S3. We defer these until later because there might
# be a huge number of them to delete when a user removes a location or deletes
# their account.
def delete_from_s3(urls):
    for url in urls:
        if len(url): 
            u = urlparse(url)
            toDelete = ToDeleteFromS3()
            toDelete.bucket = u.netloc
            toDelete.key = u.path
            toDelete.save()


# To actually delete, we would do this later:    
def image_cleanup_purge(sender, instance, *args, **kwargs):
    to_delete = instance
    conn = boto.connect_s3(settings.UPLOADER_AWS_ACCESS_KEY, settings.UPLOADER_AWS_SECRET_KEY)
    bucket = conn.get_bucket(to_delete.bucket)
    key = bucket.new_key(to_delete.key)
    key.delete()


# When deleting an image, also delete the files from S3
def image_cleanup(sender, instance, *args, **kwargs):
    image = instance
    delete_from_s3([image.url_large, image.url_medium, image.url_avatar, image.url_map])


# Connect our cleanup
pre_delete.connect(image_cleanup, sender=Image)
pre_delete.connect(image_cleanup_purge, sender=ToDeleteFromS3)

# EOF
