from django.http.response import HttpResponseForbidden, JsonResponse,\
    HttpResponseBadRequest
import shortuuid
import time
import base64
import hmac
from sharedearth import settings
from _sha1 import sha1
import urllib
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import ensure_csrf_cookie
from locations.models import Location
from django.core.exceptions import ObjectDoesNotExist
from images.models import Image
from images.interfaces import get_image_list_by_location
from django.template.loader import render_to_string
from django.template.context import RequestContext
from profiles.models import UserProfile
from images.tasks import create_map_image, get_user_uuid_from_uid


def get_user_uuid_from_request(request):
    return get_user_uuid_from_uid(request.user.id)


@ensure_csrf_cookie
@never_cache
def sign(request):
    # Validate that they're sending a JPEG 
    if request.user.is_authenticated() and request.GET['s3_object_type'] == 'image/jpeg':
        extension = 'jpeg'
    else:
        return HttpResponseForbidden()
        
    user_id = get_user_uuid_from_request(request)
    fuid = request.GET['s3_object_name']
    suffix = request.GET['s3_suffix']
    
    object_name = 'm/' + user_id + "/" + fuid + "." + suffix + "." + extension  # m/test-image.jpg' #request.GET['s3_object_name']
    mime_type = request.GET['s3_object_type']

    expires = int(time.time() + 3600)
    amz_headers = "x-amz-acl:public-read"

    put_request = "PUT\n\n%s\n%d\n%s\n/%s/%s" % (mime_type, expires, amz_headers, settings.UPLOADER_S3_BUCKET, object_name)

    signature = base64.encodestring(hmac.new(settings.UPLOADER_AWS_SECRET_KEY.encode(), put_request.encode(), sha1).digest())
    # signature = urllib.parse.quote_plus(urllib.parse.quote_plus(signature.strip()))
    signature = urllib.parse.quote_plus(signature.strip())

    url = 'https://%s.s3.amazonaws.com/%s' % (settings.UPLOADER_S3_BUCKET, object_name)

    response = JsonResponse({
        'signed_request': '%s?AWSAccessKeyId=%s&Expires=%d&Signature=%s' % (url, settings.UPLOADER_AWS_ACCESS_KEY, expires, signature),
        'url': url
    })
      
    if True:  # settings.DEBUG:
        response['Access-Control-Allow-Origin'] = '*'
      
    return response


@ensure_csrf_cookie
@never_cache
def request(request):
    
    if not request.user.is_authenticated():
        return HttpResponseForbidden()
    
    p = request.POST
    
    if 'location' in p:
        loc_uuid = p['location']
        
        try:
            location = Location.objects.get(user__id=request.user.id, uuid=loc_uuid)
        except ObjectDoesNotExist:
            return HttpResponseBadRequest("Error")
    else:
        location = None

    # Start a new image    
    image = Image()
    image.user = request.user
    image.location = location
    image.uuid = str(shortuuid.uuid()) 
    image.save()
    
    response = JsonResponse({'filenameBase': image.uuid})
    
    if True:  # settings.DEBUG:
        response['Access-Control-Allow-Origin'] = '*'
    
    return response


@ensure_csrf_cookie
@never_cache
def complete(request):
    d = request.POST
    
    if not request.user.is_authenticated():
        return HttpResponseForbidden()

    # Find the image in the database
    image = Image.objects.get(location__user__id=request.user.id, uuid=d['id'])
    
    if d['size'] == '2400':
        image.url_large = d['url']
        image.save(update_fields=['url_large'])
    elif d['size'] == '1200':
        image.url_medium = d['url']
        image.save(update_fields=['url_medium'])
    elif d['size'] == '240':
        image.url_avatar = d['url']
        image.save(update_fields=['url_avatar'])

        # Make a round version for their avatar in a background process
        create_map_image(request.user.id, image.id, d['id'])        
    else:
        return HttpResponseBadRequest("Error")
    
    imageId = image.id
    return JsonResponse({'id': imageId})


@ensure_csrf_cookie
@never_cache
def render_list(request):
    d = request.GET
    context = RequestContext(request)    
    
    context["images"] = get_image_list_by_location(d['location_id'])

    data = {'response': [
        {
            'action': 'html',
            'what': '#images-list',
            'content': render_to_string('images/image_list.html', context)
        }
    ]}                   
    
    return JsonResponse(data)    
    
    
@ensure_csrf_cookie
@never_cache
def set_avatar(request):
    if not request.user.is_authenticated():
        return HttpResponseForbidden()

    d = request.POST
    
    imageId = d['id']
    avatarType = d['type']
    
    if avatarType == 'profile':
        image = Image.objects.get(id=imageId, user=request.user)
        url = image.url_avatar
        UserProfile.objects.filter(user=request.user).update(avatar_url=url)
        return JsonResponse({'url': url})    
    elif avatarType == 'place':
        locationUuid = d['location']
        location = Location.objects.get(user=request.user, uuid=locationUuid)
        image = Image.objects.get(id=imageId, user=request.user, location=location)
        location.avatar_url = image.url_avatar
        location.map_icon_url = image.url_map
        location.save(update_fields=['avatar_url', 'map_icon_url'])
        return JsonResponse({'url': location.avatar_url, 'map_url': location.map_icon_url})    


@ensure_csrf_cookie
@never_cache
def delete(request):
    if not request.user.is_authenticated():
        return HttpResponseForbidden()

    imageId = request.POST['id']
    Image.objects.filter(id=imageId, user=request.user).delete()
    
    return JsonResponse({'success': True})


# EOF
